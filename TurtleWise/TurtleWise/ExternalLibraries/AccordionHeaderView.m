//
//  AccordionHeaderView.m
//  FZAccordionTableViewExample
//
//  Created by Krisjanis Gaidis on 6/7/15.
//  Copyright (c) 2015 Fuzz Productions, LLC. All rights reserved.
//

#import "AccordionHeaderView.h"
#import "BrandedPageManager.h"

@implementation AccordionHeaderView


- (void)set:(NSString *)headerTextName
{
    [_headerName setText:headerTextName];
    if (![[_headerName text] isEqualToString:@"Summary"]) {
        [_editButton setTitle:@"Add" forState:UIControlStateNormal];
        [_editButton setImage:[UIImage imageNamed:@"add_icon"] forState:UIControlStateNormal];
    } else {
        [_editButton setTitle:@"Edit" forState:UIControlStateNormal];
        [_editButton setImage:[UIImage imageNamed:@"tw-edit"] forState:UIControlStateNormal];
    }
    
    if ([[BrandedPageManager sharedManager] isOwn]) {
        _editButton.hidden = NO;
    } else {
        _editButton.hidden = YES;
    }
}
@end
