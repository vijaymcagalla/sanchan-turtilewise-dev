//
//  ChatMessageModel.m
//  TurtleWise
//
//  Created by Waaleed Khan on 3/30/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "ChatMessage.h"
#import "ChatMessageModel.h"

@interface ChatMessageModel ()

@property (nonatomic, strong) NSMutableDictionary *mapTitleToMessages;
@property (nonatomic, strong) NSArray *orderedTitles;
@property (nonatomic, assign) NSInteger numberOfSections;
@property (nonatomic, assign) NSInteger numberOfMessages;
@property (nonatomic, strong) NSDateFormatter *formatter;
@property (nonatomic) BOOL shouldSort;

@end

@implementation ChatMessageModel

#pragma mark - Life Cycle Methods

- (id)init
{
    self = [super init];
    
    if (self)
    {
        _shouldSort = NO;
        _orderedTitles = [[NSArray alloc] init];
        _mapTitleToMessages = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

#pragma mark - Model Interpolation Methods

- (void)addObject:(ChatMessage *)message
{
    return [self addMessage:message];
}

- (void)addObjectsFromArray:(NSArray *)messages
{
    _shouldSort = YES;
    
    for (ChatMessage *message in messages)
    {
        [self addMessage:message];
    }
    
    _shouldSort = NO;
}

#pragma mark - Getter Methods

- (NSInteger)numberOfMessages
{
    return _numberOfMessages;
}

- (NSInteger)numberOfSections
{
    return _numberOfSections;
}

#pragma mark - For Table View User Only

- (NSInteger)numberOfMessagesInSection:(NSInteger)section
{
    if ([_orderedTitles count] == 0)
    {
        return 0;
    }
    
    NSString *key = [_orderedTitles objectAtIndex:section];
    NSArray *array = [_mapTitleToMessages valueForKey:key];
    
    return [array count];
}

- (NSString *)titleForSection:(NSInteger)section
{
    NSDateFormatter *formatter = [_formatter copy];
    NSString *key = [_orderedTitles objectAtIndex:section];
    NSDate *date = [formatter dateFromString:key];
    
    [formatter setDoesRelativeDateFormatting:YES];
    
    return [formatter stringFromDate:date];
}

- (ChatMessage *)objectAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [_orderedTitles objectAtIndex:[indexPath section]];
    NSArray *array = [_mapTitleToMessages valueForKey:key];
    
    return [array objectAtIndex:[indexPath row]];
}

- (ChatMessage *)lastObject
{
    NSIndexPath *indexPath = [self indexPathForLastMessage];
    
    return [self objectAtIndexPath:indexPath];
}

- (NSIndexPath *)indexPathForLastMessage
{
    NSInteger lastSection = _numberOfSections - 1;
    NSInteger numberOfMessages = [self numberOfMessagesInSection:lastSection];
    
    return [NSIndexPath indexPathForRow:numberOfMessages - 1 inSection:lastSection];
}

- (NSIndexPath *)indexPathForMessage:(ChatMessage *)message
{
    NSString *key = [self keyForMessage:message];
    NSInteger section = [_orderedTitles indexOfObject:key];
    NSInteger row = [[_mapTitleToMessages valueForKey:key] indexOfObject:message];
    
    return [NSIndexPath indexPathForRow:row inSection:section];
}

- (NSIndexPath *)indexPathForSentMessage:(ChatMessage *)sentMessage
{
    NSString *key = [self keyForMessage:sentMessage];
    NSInteger section = [_orderedTitles indexOfObject:key];

    __block NSIndexPath *indexPath;
    
    [[_mapTitleToMessages valueForKey:key] enumerateObjectsUsingBlock:^(ChatMessage *chatMessage, NSUInteger idx, BOOL *stop)
     {
         if ([[sentMessage messageIdBeforeSending] isEqualToString:[chatMessage messageIdBeforeSending]])
         {
             indexPath = [NSIndexPath indexPathForRow:idx inSection:section];
         }
    }];
    
    return indexPath;
}

#pragma mark - Helpers

- (void)addMessage:(ChatMessage *)message
{
    NSString *key = [self keyForMessage:message];
    NSMutableArray *array = [_mapTitleToMessages valueForKey:key];
   
    if (!array)
    {
        _numberOfSections += 1;
        array = [[NSMutableArray alloc] init];
    }
    
    [array addObject:message];
    
    NSMutableArray *result;
    
    if (_shouldSort)
    {
        NSArray *sortedArray = [array sortedArrayUsingComparator:^NSComparisonResult(ChatMessage *message1, ChatMessage *message2)
                                {
                                    return [[message1 messageDate] compare: [message2 messageDate]];
                                }];
        result = [NSMutableArray arrayWithArray:sortedArray];
    }
    else
    {
        result = [NSMutableArray arrayWithArray:array];
    }
    
    [_mapTitleToMessages setValue:result forKey:key];
    [self cacheTitles];
    
    _numberOfMessages += 1;
}

- (void)cacheTitles
{
    NSArray *array = [_mapTitleToMessages allKeys];
    NSArray *orderedArray = [array sortedArrayUsingComparator:^NSComparisonResult(NSString *dateString1, NSString *dateString2)
                             {
                                 NSDate *date1 = [_formatter dateFromString:dateString1];
                                 NSDate *date2 = [_formatter dateFromString:dateString2];
                                 
                                 return [date1 compare: date2];;
                             }];
    
    _orderedTitles  = [[NSArray alloc] initWithArray:orderedArray];
}

- (NSString *)keyForMessage:(ChatMessage *)message
{
    return [self.formatter stringFromDate:[message messageDate]];  //Without using Self, Setter will not be called
}

- (NSDateFormatter *)formatter
{
    if (!_formatter)
    {
        _formatter = [[NSDateFormatter alloc] init];
        _formatter.timeStyle = NSDateFormatterNoStyle;
        _formatter.dateStyle = NSDateFormatterShortStyle;
        _formatter.doesRelativeDateFormatting = NO;
        
    }
    
    return _formatter;
}

@end
