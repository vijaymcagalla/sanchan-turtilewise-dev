//
//  TWChatTableView.m
//  TurtleWise
//
//  Created by Waleed Khan on 3/24/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "TWChatTableView.h"
#import "ChatMessageCell.h"
#import "ChatMessage.h"
#import "ChatMessageModel.h"

#define MY_USERID @"1"
#define HEADER_HEIGHT 40.0

@interface TWChatTableView () < UITableViewDataSource, UITableViewDelegate >

- (void)initalize;
- (void)markMessageAsSent:(NSIndexPath *)messageIndexPath;

@property(nonatomic, strong) ChatMessageModel *chatMessages;
@property(nonatomic, strong) ChatMessage *messageThatTriggeredLoadMoreMessages;
@property(nonatomic) BOOL isLoadedFirstTime;

@end

@implementation TWChatTableView

#pragma mark - Life Cycle Methods

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initalize];
}

- (void)initalize
{
    [self setDelegate:self];
    [self setDataSource:self];
    
    [self registerNib:[UINib nibWithNibName:[ChatMessageCell myCellName] bundle:[NSBundle mainBundle]] forCellReuseIdentifier:[ChatMessageCell myCellName]];
    [self registerNib:[UINib nibWithNibName:[ChatMessageCell cellName] bundle:[NSBundle mainBundle]] forCellReuseIdentifier:[ChatMessageCell cellName]];
    [self setRowHeight:UITableViewAutomaticDimension];
    [self setEstimatedRowHeight:108];
    [self setContentInset:UIEdgeInsetsMake(0, 0, 10, 0)];
    
    _chatMessages = [ChatMessageModel new];
    
    _isLoadedFirstTime = YES;
}

- (void)prepareChatMessages:(NSArray *)chatMessages
{
    [_chatMessages addObjectsFromArray:chatMessages];

    [self reloadData];
    
    if (_isLoadedFirstTime)
    {
        [self scrollToBottomAnimated:NO];
    }
    
    else if(_messageThatTriggeredLoadMoreMessages)
    {
        [self scrollToRowAtIndexPath:[_chatMessages indexPathForMessage:_messageThatTriggeredLoadMoreMessages]
                    atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}

#pragma mark - UITableView Date Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_chatMessages numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_chatMessages numberOfMessagesInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatMessage *chatMessage = [_chatMessages objectAtIndexPath:indexPath];
    
    NSString *identifer = ([chatMessage isMyMessage]) ? [ChatMessageCell myCellName] : [ChatMessageCell cellName];
    
    ChatMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:identifer];
    [cell set:chatMessage];
    
    return cell;
}

#pragma UITableView Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HEADER_HEIGHT;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [_chatMessages titleForSection:section];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [tableView frame].size.width, HEADER_HEIGHT)];
    [view setBackgroundColor:[UIColor clearColor]];
    [view setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    UILabel *label = [UILabel new];
    [label setText:[self tableView:tableView titleForHeaderInSection:section]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont fontWithName:@"Helvetica" size:23.0]];
    [label sizeToFit];
    [label setCenter:[view center]];
    [label setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
    [label setBackgroundColor:[UIColor colorWithRed:207 / 255.0 green:220 / 255.0 blue:252.0 / 255.0 alpha:1]];
    [label setAutoresizingMask:UIViewAutoresizingNone];
    
    [[label layer] setCornerRadius:13.0];
    [[label layer] setMasksToBounds:YES];

    [view addSubview:label];
    
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isLoadedFirstTime)
    {
        _isLoadedFirstTime = NO;
        
        return;
    }
    
    if ([indexPath section] == 0 && [indexPath row] == 0)
    {
        if (_viewDelegate && [_viewDelegate respondsToSelector:@selector(loadMoreMessages)])
        {
            _messageThatTriggeredLoadMoreMessages = [_chatMessages objectAtIndexPath:indexPath];
            
            [_viewDelegate loadMoreMessages];
        }
    }
}

#pragma mark - Utility Methods

- (void)scrollToBottomAnimated:(BOOL)animated
{
    NSInteger numberOfSections = [_chatMessages numberOfSections];
    NSInteger numberOfRows = [_chatMessages numberOfMessagesInSection:numberOfSections - 1];
    
    if (numberOfRows)
    {
        [self scrollToRowAtIndexPath:[_chatMessages indexPathForLastMessage] atScrollPosition:UITableViewScrollPositionBottom animated:animated];
    }
}

- (void)addMessage:(ChatMessage *)message
{
    BOOL shouldScroll = [[self indexPathsForVisibleRows] containsObject:[_chatMessages indexPathForLastMessage]];

    [_chatMessages addObject:message];
    
    //Update Table View
    NSIndexPath *indexPath = [_chatMessages indexPathForMessage:message];
    
    [self beginUpdates];
    
    if ([_chatMessages numberOfMessagesInSection:[indexPath section]] == 1)
    {
        [self insertSections:[NSIndexSet indexSetWithIndex:[indexPath section]] withRowAnimation:UITableViewRowAnimationNone];
    }

    [self insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
    
    [self  endUpdates];
    
    if (shouldScroll)
    {
        [self scrollToBottomAnimated:NO];
    }
}

- (void)addActivityIndicator
{
    if ([_chatMessages numberOfMessages] == 0)
    {
        return;
    }
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    [activityIndicator setCenter:[headerView center]];
    [activityIndicator startAnimating];
    [activityIndicator setHidesWhenStopped:YES];
    
    [headerView addSubview:activityIndicator];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    
    [self setTableHeaderView:headerView];
}

- (void)updateMessage:(ChatMessage *)message
{
    NSIndexPath *indexPath = [_chatMessages indexPathForSentMessage:message];
    
    if (indexPath)
    {
        [self markMessageAsSent:indexPath];
        
        return;
    }
    
    [self addMessage:message];
}

- (void)markMessageAsSent:(NSIndexPath *)messageIndexPath
{
    ChatMessageCell *cellToUpdate = [self cellForRowAtIndexPath:messageIndexPath];
    
    if (cellToUpdate)
    {
        [cellToUpdate markMessageSent];
    }
}

@end
