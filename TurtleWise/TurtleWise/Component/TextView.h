//
//  TextView.h
//
//  Created by Usman Asif on 3/4/16.
//  Copyright © 2016 10Pearls. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface TextView : UITextView

@property (nonatomic) IBInspectable NSString * placeholder;
@property (nonatomic) IBInspectable UIColor * placeholderColor;

-(void)textChanged:(NSNotification*)notification;

@end
