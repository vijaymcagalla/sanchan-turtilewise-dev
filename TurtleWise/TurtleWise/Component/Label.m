//
//  Label.m
//  Guardian
//
//  Created by mohsin on 10/18/14.
//  Copyright (c) 2014 10Pearls. All rights reserved.
//

#import "Label.h"
#import "Constant.h"

@implementation Label

#pragma mark - Life cycle Methods

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self sizeToFit];
}

#pragma mark - Customization

- (void)addLineSpacing:(CGFloat)lineSpacing
{
    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc]initWithString:[self text]];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    
    [style setLineSpacing:lineSpacing];
    [attrString addAttribute:NSParagraphStyleAttributeName
                       value:style
                       range:NSMakeRange(0, [[self text] length])];
    
    [self setAttributedText:attrString];
}

@end
