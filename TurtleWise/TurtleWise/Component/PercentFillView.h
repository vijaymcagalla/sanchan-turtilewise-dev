//
//  PercentFillView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/14/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PercentFillView : UIView

@property(nonatomic) IBInspectable float fillPercentage;

@end
