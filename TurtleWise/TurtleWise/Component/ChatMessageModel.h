//
//  ChatMessageModel.m
//  TurtleWise
//
//  Created by Waaleed Khan on 3/30/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//


#import <Foundation/Foundation.h>

@class ChatMessage;

@interface ChatMessageModel : NSObject

- (void)addObject:(ChatMessage *)message;
- (void)addObjectsFromArray:(NSArray *)messages;

- (NSInteger)numberOfMessages;
- (NSInteger)numberOfSections;
- (NSInteger)numberOfMessagesInSection:(NSInteger)section;

- (NSString *)titleForSection:(NSInteger)section;

- (ChatMessage *)objectAtIndexPath:(NSIndexPath *)indexPath;
- (ChatMessage *)lastObject;

- (NSIndexPath *)indexPathForLastMessage;
- (NSIndexPath *)indexPathForMessage:(ChatMessage *)message;
- (NSIndexPath *)indexPathForSentMessage:(ChatMessage *)message;

@end
