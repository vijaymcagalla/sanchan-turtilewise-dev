//
//  IBView.m
//  GCS
//
//  Created by Ali Zohair on 27/07/2015.
//  Copyright (c) 2015 tenpearls. All rights reserved.
//

#import "IBView.h"

@implementation IBView


//Assign xib to class and add constraints
- (id)awakeAfterUsingCoder:(NSCoder *)aDecoder
{
    if (![self.subviews count])
    {
        NSBundle *mainBundle = [NSBundle mainBundle];
        NSString *className = NSStringFromClass([self class]);
        NSArray *loadedViews = [mainBundle loadNibNamed:className owner:nil options:nil];
        IBView *loadedView = [loadedViews firstObject];
        
        loadedView.frame = self.frame;
        loadedView.autoresizingMask = self.autoresizingMask;
        loadedView.translatesAutoresizingMaskIntoConstraints =
        self.translatesAutoresizingMaskIntoConstraints;
        
        for (NSLayoutConstraint *constraint in self.constraints)
        {
            id firstItem = constraint.firstItem;
            if (firstItem == self)
            {
                firstItem = loadedView;
            }
            id secondItem = constraint.secondItem;
            if (secondItem == self)
            {
                secondItem = loadedView;
            }
            [loadedView addConstraint:
             [NSLayoutConstraint constraintWithItem:firstItem
                                          attribute:constraint.firstAttribute
                                          relatedBy:constraint.relation
                                             toItem:secondItem
                                          attribute:constraint.secondAttribute
                                         multiplier:constraint.multiplier
                                           constant:constraint.constant]];
        }
        return loadedView;
    }
    return self;
}

@end
