//
//  TextField.m
//  Guardian
//
//  Created by mohsin on 10/18/14.
//  Copyright (c) 2014 10Pearls. All rights reserved.
//

#import "TextField.h"
#import "Constant.h"
#import "Color.h"
#import "Font.h"
#import "StringUtils.h"
#import "UITextField+Padding.h"


#define TEXTFIELD_BORDER_WIDTH  1.2
#define TEXTFIELD_TEXT_PADDING_X 5
#define TEXTFIELD_TEXT_PADDING_Y 0

#define MAX_VALUE 20
#define MIN_VALUE 0

@implementation TextField

-(void)awakeFromNib
{
    [super awakeFromNib];
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , TEXTFIELD_TEXT_PADDING_X , TEXTFIELD_TEXT_PADDING_Y);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , TEXTFIELD_TEXT_PADDING_X , TEXTFIELD_TEXT_PADDING_Y);
}



-(void) customizePlaceHoder{

    if (self.placeholder == NULL)
    {
        return;
    }
    
    UIColor *placeHolderTextColor = [UIColor lightGrayColor];

    [self setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:self.placeholder
                                                                   attributes:@{
                                                                                NSFontAttributeName :[UIFont fontWithName:REGULAR_FONT size:self.font.pointSize],
                                                                                NSForegroundColorAttributeName:placeHolderTextColor
                                                                                }]];

}

-(void) setBorderColorRed {

    UIColor *borderColor = [UIColor redColor];
    self.layer.borderColor= borderColor.CGColor;

}

-(void) setBorderColorGray{


     UIColor *borderColor = [UIColor lightGrayColor];
        self.layer.borderColor= borderColor.CGColor;
}

-(BOOL) isValid {

    if([StringUtils isEmptyOrNull:self.text]) {
        //[self setBorderColorRed];
        return NO;
    }
    return YES;

}

- (BOOL)isValueInRangeOf:(int)minValue and:(int)maxValue
{
    int intValue = [[self text] intValue];
    
    if (intValue >= minValue && intValue <= maxValue)
    {
        return YES;
    }
    
    return NO;
}

- (BOOL)isLenghtInRangeOf:(int)minValue and:(int)maxValue
{
    NSUInteger length = [[self text] length];
    
    if (length >= minValue && length <= maxValue)
    {
        return YES;
    }
    
    return NO;
}

-(BOOL)isValueGreaterThen:(int)minValue
{
    int intValue = [[self text] intValue];
    
    return intValue <= minValue;
}

@end
