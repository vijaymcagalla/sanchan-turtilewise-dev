//
//  FacebookManager.m
//  FlikShop
//
//  Created by Waaleed Khan on 4/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "FacebookManager.h"
#import "BaseController.h"
#import "Constant.h"
#import "Alert.h"

@interface FacebookManager () 

+ (void)fetchUserInfo:(void (^)(id response))success failure:(void (^)(NSError *error))failure;

@end

@implementation FacebookManager

#pragma mark - Auth Methods

+ (void)loginFromController:(BaseController *)controller WithSuccess:(void (^)(id response))success failure:(void (^)(NSError *error))failure
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login logInWithReadPermissions:@[@"email"] fromViewController:controller handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
    {
        if (error || [result isCancelled])
        {
            if (failure)
            {
                failure(error);
            }
        }
        else
        {
            if (success)
            {
                success([[FBSDKAccessToken currentAccessToken] tokenString]);
            }
        }
    }];
}

#pragma mark - Fetch Methods

+ (void)fetchUserInfo:(void (^)(id response))success failure:(void (^)(NSError *error))failure
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
        {
            if (error)
            {
                if (failure)
                {
                    failure(error);
                }
            }
            
            else
            {
                if (success)
                {
                    success(result);
                }
            }
         }];
    }
}

@end
