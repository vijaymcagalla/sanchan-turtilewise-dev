//
//  AnalyticsHelper.m
//  SnapAndStyle
//
//  Created by Waleed Khan on 9/29/14.
//  Copyright (c) 2014 sana. All rights reserved.
//

#import "GAIDictionaryBuilder.h"
#import "EnvironmentConstants.h"
#import "AnalyticsHelper.h"
#import "StringUtils.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "Constant.h"
#import "GAI.h"

@implementation AnalyticsHelper


#pragma mark - AnalyticsHelper Methods

+ (void) startAnalyticsSession
{
    [[GAI sharedInstance] setTrackUncaughtExceptions:YES];
    [[GAI sharedInstance] setDispatchInterval:1];
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    [[GAI sharedInstance] trackerWithTrackingId:GOOGLE_ANALYTICS_TRACKING_ID];
    
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createScreenView];
    [builder set:@"start" forKey:kGAISessionControl];
}

+ (void)endSession
{
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createScreenView];
    [builder set:@"end" forKey:kGAISessionControl];
}

+ (void) logScreeen:(GAITrackedViewController *)controller {
    [AnalyticsHelper logScreeen:controller AndTitle:[controller title]];
}

+ (void) logScreeen:(GAITrackedViewController *)controller AndTitle:(NSString *)title
{
    NSString *temp = title;
    
    if ([StringUtils isEmptyOrNull:temp])
    {
        temp = @"Home";
    }
    
    [controller setScreenName:temp];
}

+ (void) logEvent:(GAITrackedViewController *)controller category:(NSString *)category action:(NSString *)action {
    [AnalyticsHelper logEvent:controller category:category action:action label:nil value:nil];
}

+ (void) logEvent:(GAITrackedViewController *)controller category:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category
                                                          action:action
                                                           label:label
                                                           value:value] build]];
}
@end
