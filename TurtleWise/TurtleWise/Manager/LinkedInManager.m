//
//  LinkedInManager.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/2/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "EnvironmentConstants.h"
#import "LinkedInManager.h"
#import "LinkedInHelper.h"
#import "BaseController.h"
#import "ParserUtils.h"

#define KEY_LI_ACCESS_TOKEN @"access_token"

@interface LinkedInManager ()


@end

@implementation LinkedInManager

#pragma mark - REST API Implementation

+ (void)startLoginProcessWithSuccess:(BaseController *)controller WithSuccess:(successCallback)success andfailure:(failureCallback)failure
{
    LinkedInHelper *linkedIn = [LinkedInHelper sharedInstance];
    
    [linkedIn requestAccessTokenWithSenderViewController:controller
                                                clientId:LINKEDIN_CLIENT_ID
                                            clientSecret:LINKEDIN_CLIENT_SECRET
                                             redirectUrl:LINKEDIN_REDIRECT_URL
                                             permissions:@[@(BasicProfile),@(Share), @(EmailAddress)]
                                                   state:NULL
                                         successUserInfo:^(NSDictionary *userInfo){
        if (success){
            success([userInfo objectForKey:KEY_LI_ACCESS_TOKEN]);
        }
    }
    failUserInfoBlock:^(NSError *error)
    {
        if (failure)
        {
            failure(error);
        }
    }];
}

@end
