//
//  PushNotificationManager.h
//  TurtleWise
//
//  Created by Waleed Khan on 2/29/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ChatMessage;
@protocol PushNotificationManagerDelegate <NSObject>

@required

- (void)recievedQuestionWithId:(NSString *)questionId;
- (void)recievedAnswerForQuestion:(NSString *)questionId;
- (void)recievedNewMessage:(ChatMessage *)chatMessage isChatActive:(BOOL)isChatActive;
- (void)recievedNewChat:(NSString *)chatId;
- (void)recievedEndSubscription;

@end

@interface PushNotificationManager : NSObject

- (id)initWithPayload:(NSDictionary *)payload andDelegate:(id)delegate;
- (id)initWithUserActivity:(NSUserActivity *)userActivity andDelegate:(id)delegate;
- (void)handleRoutesForNotification;
- (void)handleRoutesForUserActivity;

//Static Helper Methods
+ (void)registerForPushNotification;
+ (void)storeDeviceToken:(NSData *)deviceToken;

@end
