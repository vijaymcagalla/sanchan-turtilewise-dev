//
//  LinkedInManager.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/2/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "Service.h"

@class BaseController;

@interface LinkedInManager : NSObject

+ (void)startLoginProcessWithSuccess:(BaseController *)controller WithSuccess:(successCallback)success andfailure:(failureCallback)failure;

@end