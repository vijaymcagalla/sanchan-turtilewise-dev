//
//  SocialShareManager.m
//  TurtleWise
//
//  Created by Ajdal on 3/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "SocialShareManager.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <SafariServices/SafariServices.h>
#import <Social/Social.h>

@implementation SocialShareManager
+(void)shareOnFB{
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:@"http://turtlewise.net/"];
    
    FBSDKShareDialog *shareDialog = [FBSDKShareDialog new];
    shareDialog.mode = FBSDKShareDialogModeFeedBrowser;
    shareDialog.shareContent = content;
    
    [shareDialog show];
}
+(void)shareOnLinkedin:(UIViewController*)presentingController{
    NSURL *shareURL = [NSURL URLWithString:@"https://www.linkedin.com/shareArticle?mini=true&url=http://turtlewise.net"];
    if([SFSafariViewController class] != nil) {
        SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:shareURL];
        [presentingController presentViewController:sfvc animated:YES completion:nil];
    } else {
        [[UIApplication sharedApplication] openURL:shareURL];
    }
}
+(void)shareOnGooglePlus:(UIViewController*)presentingController{
    NSURL *shareURL = [NSURL URLWithString:@"http://turtlewise.net/"];
    
    // Construct the Google+ share URL
    NSURLComponents* urlComponents = [[NSURLComponents alloc]
                                      initWithString:@"https://plus.google.com/share"];
    urlComponents.queryItems = @[[[NSURLQueryItem alloc]
                                  initWithName:@"url"
                                  value:[shareURL absoluteString]]];
    NSURL* url = [urlComponents URL];
    
    if ([SFSafariViewController class]) {
        // Open the URL in SFSafariViewController (iOS 9+)
        SFSafariViewController* controller = [[SFSafariViewController alloc] initWithURL:url];
        [presentingController presentViewController:controller animated:YES completion:nil];
    } else {
        // Open the URL in the device's browser
        [[UIApplication sharedApplication] openURL:url];
    }
}
+(void)shareOnTwitter:(UIViewController*)presentingController{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *slComposeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [slComposeViewController addURL:[NSURL URLWithString:@"http://turtlewise.net/"]];
        [presentingController presentViewController:slComposeViewController animated:YES completion:nil];
        return;
    }
    
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@""
                              message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup."
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
    [alertView show];
}
@end
