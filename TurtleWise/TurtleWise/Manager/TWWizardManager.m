//
//  ProfileWizardManager.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/15/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardController.h"
#import "TWWizardManager.h"
#import "BaseController.h"
#import "SeekingAdvisor.h"
#import "UserProfile.h"
#import "UserDefaults.h"

#define PROFILE_WIZARD_STEPS @[@"UserBasicInfoController", @"UserLocationController", @"SelectEthnicReligionController", @"SelectMaritalStatusController", @"SelectEducationLevelController", @"SelectSchoolsController", @"SelectProfessionController", @"SelectHobbiesController",@"SelectSignUpAsController",@"ThankYouController", @"UserTagsController"]

#define PROFILE_WIZARD_STEPS_WITHOUT_PAGE @[@"UserBasicInfoController", @"UserLocationController", @"SelectEthnicReligionController", @"SelectMaritalStatusController", @"SelectEducationLevelController", @"SelectSchoolsController", @"SelectProfessionController", @"SelectHobbiesController",@"ThankYouController", @"UserTagsController"]

#define ADVISOR_WIZARD_STEPS @[@"AdvisorBasicInfoController", @"UserLocationController", @"SelectEthnicReligionController", @"SelectMaritalStatusController", @"SelectEducationLevelController", @"SelectSchoolsController", @"SelectProfessionController"]

@interface TWWizardManager ()

@property(nonatomic, strong) NSArray *stepsOfWizard;
@property(nonatomic, assign) NSInteger startStep;

@property(nonatomic, strong) BaseController *presentingController;
@property(nonatomic, strong) UINavigationController *navigationController;

- (void)initalizeWith:(BaseController *)presentingController andStep:(NSInteger )startStep;
- (void)start;

@end

@implementation TWWizardManager

#pragma mark - Life Cycle Methods

- (id)initWithPresentingController:(BaseController *)presentingController fromStep:(NSInteger )startStep
{
    if (self = [super init])
    {
        [self initalizeWith:presentingController andStep:startStep];
    }
    
    return self;
}

- (id)initWithPresentingController:(BaseController *)presentingController
{
    return [self initWithPresentingController:presentingController fromStep:0];
}

- (void)initalizeWith:(BaseController *)presentingController andStep:(NSInteger )startStep
{
    _createBrandedPage = [[NSMutableDictionary alloc] init];
    _startStep = startStep;
    _currentStep = 0;
    _presentingController = presentingController;
    _navigationController = [_presentingController navigationController];
}

#pragma mark - Helper Methods

- (BOOL)isLastStep
{
    return _currentStep >= [_stepsOfWizard count];
}

- (BaseProfileWizardController *)getControllerForStep:(NSInteger)step
{
    return [[NSClassFromString([_stepsOfWizard objectAtIndex:step]) alloc] initWithProfileWizardManager:self];
}

#pragma mark - Wizard Navigation

- (void)startProfileWizard
{
    if (!_profile)
    {
        _profile = [UserProfile new];
    }
    
    if ([UserDefaults getPageID].length == 0) {
        _stepsOfWizard = PROFILE_WIZARD_STEPS;
    } else {
        _stepsOfWizard = PROFILE_WIZARD_STEPS_WITHOUT_PAGE;
    }
    
    [self start];
}

- (void)startAdvisorWizard
{
    if (!_profile)
    {
        _profile = [SeekingAdvisor new];
    }
    
    _stepsOfWizard = ADVISOR_WIZARD_STEPS;
    
    [self start];
}

- (void)start
{
    NSMutableArray *viewControllers = [[NSMutableArray alloc] initWithArray:[_navigationController viewControllers]];
  
    for (int i = 0; i <= _startStep; i++)
    {
        _currentStep += 1;
        
        [viewControllers addObject:[self getControllerForStep:i]];
    }
    
    [_navigationController setViewControllers:viewControllers animated:YES];
}

- (void)next
{
    
    if (_currentStep >= [_stepsOfWizard count])
    {
        return;
    }
    
    [_navigationController pushViewController:[self getControllerForStep:_currentStep] animated:YES];
    
    _currentStep += 1;
}

- (void)previous
{
    _currentStep -= 1;
    
    [_navigationController popViewControllerAnimated:YES];
}

- (void)finish
{
    _currentStep = 0;
    
    [_navigationController popToViewController:_presentingController animated:YES];
}

@end
