//
//  TurtleBuck.m
//  TurtleWise
//
//  Created by Ajdal on 6/7/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "UserLevel.h"

#define KEY_CHAT_PAYMENT @"chatPayment"
#define KEY_LEVEL        @"level"
#define KEY_MIN_BUCKS    @"minBucks"
#define KEY_MAX_BUCKS    @"maxBucks"
#define KEY_TITLE        @"title"

@implementation UserLevel

- (void)set:(NSDictionary *)input{
    self.chatReward = [ParserUtils intValue:input key:KEY_CHAT_PAYMENT];
    self.level = [ParserUtils intValue:input key:KEY_LEVEL];
    self.minBucks = [ParserUtils intValue:input key:KEY_MIN_BUCKS];
    self.maxBucks = [ParserUtils intValue:input key:KEY_MAX_BUCKS];
    self.title = [ParserUtils stringValue:input key:KEY_TITLE];
}

#pragma mark -- 
- (id)initWithCoder:(NSCoder *)coder{
    self = [super init];
    
    if (self != nil){
        _chatReward = [coder decodeIntegerForKey:KEY_CHAT_PAYMENT];
        _level = [coder decodeIntegerForKey:KEY_LEVEL];
        _minBucks = [coder decodeIntegerForKey:KEY_MIN_BUCKS];
        _maxBucks = [coder decodeIntegerForKey:KEY_MAX_BUCKS];
        _title= [coder decodeObjectForKey:KEY_TITLE];
        
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder{
    [coder encodeInteger:_chatReward forKey:KEY_CHAT_PAYMENT];
    [coder encodeInteger:_level forKey:KEY_LEVEL];
    [coder encodeInteger:_minBucks forKey:KEY_MIN_BUCKS];
    [coder encodeInteger:_maxBucks forKey:KEY_MAX_BUCKS];
    [coder encodeObject:_title forKey:KEY_TITLE];

}

@end
