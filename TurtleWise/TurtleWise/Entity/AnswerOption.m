//
//  AnswerOption.m
//  TurtleWise
//
//  Created by Ajdal on 1/15/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "AnswerOption.h"

@implementation AnswerOption
-(void)set:(NSDictionary*)input{
    self.name = [ParserUtils stringValue:input key:@"key"];
    self.count = (int)[ParserUtils intValue:input key:@"count"];
}
@end
