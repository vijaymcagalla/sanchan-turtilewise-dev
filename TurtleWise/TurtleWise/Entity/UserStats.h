//
//  UserStats.h
//  TurtleWise
//
//  Created by Waleed Khan on 2/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseEntity.h"

@interface UserStats : BaseEntity

@property(nonatomic, assign) long seekerQuestions;
@property(nonatomic, assign) long seekerAnswers;
@property(nonatomic, assign) long advisorQuestions;

@property(nonatomic, assign) long unratedChats;

- (void)markActiveQuestionInActive;
- (void)markUnReadQuestionRead;
- (void)decreaseUnAnsweredQuestionCount;
- (void)markChatReadForUserRole;

- (long)unreadChats;

@end
