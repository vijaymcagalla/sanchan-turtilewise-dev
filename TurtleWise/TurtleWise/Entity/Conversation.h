//
//  Conversation.h
//  TurtleWise
//
//  Created by Waleed Khan on 4/21/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseEntity.h"
#import "Enum.h"
@class Question;
@class UserProfile;

@interface Conversation : BaseEntity

@property(nonatomic, strong) NSString *conversationId;
@property(nonatomic, strong) NSString *lastMessage;

@property(nonatomic) BOOL isRead;
@property(nonatomic) BOOL isRated;

@property(nonatomic, strong) NSNumber *createdAt;
@property(nonatomic, strong) NSNumber *expiry;
@property(nonatomic, strong) NSNumber *timeDifferenceSystemAndServer;

@property(nonatomic) ConversationStatus status;

@property(nonatomic, strong) NSMutableArray *advisors;

@property(nonatomic, strong) Question *question;

@property(nonatomic) NSString * message;

-(NSTimeInterval)getRemainingTime;
-(NSArray*)getParticipantsForUserRole:(UserRole)userRole andUserId:(NSString*)userId;

@end
