//
//  Answer.h
//  TurtleWise
//
//  Created by Irfan Gul on 25/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseEntity.h"
#import "Advisor.h"

@interface Answer : BaseEntity

@property(nonatomic,strong) NSString *answerId;
@property(nonatomic,strong) NSString *answerText;
@property(nonatomic,strong) NSString *reason;
@property(nonatomic,strong) NSString *whyChooseMe;
@property(nonatomic,strong) NSString *seekersFeedback;
@property(nonatomic,strong) NSNumber *createdAt;
@property(nonatomic) BOOL advisorAvailableForChat;
@property(nonatomic,strong) Advisor *advisor;
@end
