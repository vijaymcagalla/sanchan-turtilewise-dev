//
//  Settings.h
//  TurtleWise
//
//  Created by Usman Asif on 2/12/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseEntity.h"

@interface Settings : BaseEntity

@property(nonatomic, assign) BOOL shouldNotifyOnQuestion;
@property(nonatomic, assign) BOOL shouldNotifyOnAnswer;
@property(nonatomic, assign) BOOL shouldSendEmailDigest;

@property(nonatomic) BOOL emailVerified;
@end
