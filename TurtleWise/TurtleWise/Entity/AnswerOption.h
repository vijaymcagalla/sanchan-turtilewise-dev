//
//  AnswerOption.h
//  TurtleWise
//
//  Created by Ajdal on 1/15/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseEntity.h"

@interface AnswerOption : BaseEntity
@property(nonatomic,strong) NSString *name;
@property(nonatomic) int count;
@end
