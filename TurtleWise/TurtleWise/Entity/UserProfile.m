//
//  Profile.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/8/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "UserDefaults.h"
#import "ProfileCell.h"
#import "StringUtils.h"
#import "DateUtils.h"
#import "UserProfile.h"
#import "Keys.h"
#import "Constant.h"
#import "EnvironmentConstants.h"

#define HEADER_PROFILE_PICTURE @"Profile Picture:"
#define HEADER_NAME @"Name:"
#define HEADER_ABOUT_ME @"About me:"
#define HEADER_LOCATION @"Location:"
#define HEADER_DOB @"Date of Birth:"
#define HEADER_MARITAL_STATUS @"Marital Status:"
#define HEADER_POLITICAL_AFFILIATION @"Political Affiliation:"
#define HEADER_SIBLINGS @"Sibling(s):"
#define HEADER_SCHOOL @"School(s):"
#define HEADER_EMPLOYER @"Employer(s):"
#define HEADER_HOBBIES @"Hobby(s):"
#define HEADER_KIDS @"Kid(s):"
#define HEADER_PROFESSION @"Profession(s):"
#define HEADER_EDUCATION @"Education:"
#define HEADER_GURU_TAGS @"Guru Tag(s):"
#define HEADER_EXPLORER_TAGS @"Explorer Tag(s):"

#define HEADER_SIGN_UP_AS @"signUpAs(s):"
#define HEADER_BRANDED_PAGE_NAME @"Branded Page Name(s):"
#define HEADER_SUMMARY_TAG @"Summary(s):"
#define HEADER_ARTICLE_ONE_URL @"Article One URL(s):"
#define HEADER_ARTICLE_TWO_URL @"Article Two URL(s):"
#define HEADER_ARTICLE_THREE_URL @"Article Three URL(s):"
#define HEADER_EVENT_ONE_URL @"Event One URL(s):"
#define HEADER_EVENT_TWO_URL @"Event Two URL(s):"
#define HEADER_EVENT_THREE_URL @"Event Three URL(s):"
#define HEADER_PUBLIC_PRIVATE @"Public Or Private(s):"

#define PROFILE_PRIVACY_PUBLIC @"public"
#define PROFILE_PRIVACY_PRIVATE @"private"

//keys
#define KEY_SUBSCRIPTION @"subscription"
#define KEY_SUBSCRIPTION_TYPE_FREE @"free"
#define KEY_SUBSCRIPTION_EXPIRAY @"expiryTime"

#define PHOTO_URL_STRING @"%@users/%@/avatarStream/%@"

#pragma mark - Class Extansion

@interface UserProfile ()

@property(nonatomic, assign) BOOL includeEmptyValues;
@property(nonatomic, strong) NSMutableArray *arrayForEntity;

- (void)setProfileCellEntityWithHeader:(NSString *)header content:(NSString *)content stepInWizard:(NSInteger)step andResponseKey:(NSString *)responseKey;
- (NSString *)getCommaSepartedFrom:(NSArray *)targetArray;

@end

#pragma mark - Implementation

@implementation UserProfile

#pragma mark - Entity Setter

- (void)set:(NSDictionary*)input
{
    NSDictionary *data = [ParserUtils object:input key:KEY_API_DATA];
    NSDictionary *attributes = [ParserUtils object:data key:KEY_ATTRIBUTES];
    NSDictionary *tags = [ParserUtils object:data key:KEY_TAGS];
    NSDictionary *rating = [ParserUtils object:data key:KEY_RATING];
    NSDictionary *turtle = [ParserUtils object:data key:KEY_TURTLE];
    NSDictionary *subscription = [ParserUtils object:data key:KEY_SUBSCRIPTION];
    
    if([ParserUtils array:data key:KEY_PAGES])
    {
        [self setPages:[ParserUtils array:data key:KEY_PAGES]];
        NSString *pages = [[ParserUtils array:data key:KEY_PAGES] firstObject];
        if(pages.length > 0)
        {
            [UserDefaults setPageID:pages];
        }
    } else {
        [UserDefaults setPageID:NULL];
    }
    
    self.emailVerified = [ParserUtils boolValue:data key:KEY_EMAIL_VERIFIED];
    
    _firstName = [StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_FIRST_NAME]];
    _lastName = [StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_LAST_NAME]];
    _about = [StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_ABOUT]];
    _dateOfBirth = [self parseDateOfBirth:[ParserUtils numberValue:attributes key:KEY_DOB]];
    _hobbies = [ParserUtils array:attributes key:KEY_HOBBIES];
    _avatar = [StringUtils decodeBase64ToImage:[ParserUtils stringValue:data key:KEY_AVATAR] withDefault:PHOTO_PLACEHOLDER_IMAGE];
    
    _profileImage = [StringUtils decodeBase64ToImage:[ParserUtils stringValue:data key:KEY_PROFILE] withDefault:PHOTO_BRAND_PAGE_PLACEHOLDER_IMAGE];
    _coverImage = [StringUtils decodeBase64ToImage:[ParserUtils stringValue:data key:KEY_COVER] withDefault:PHOTO_BRAND_PAGE_PLACEHOLDER_IMAGE];
    
    _privateAttributes = [NSMutableSet setWithArray:[ParserUtils array:data key:KEY_PRIVATE_ATTRIBUTES]];
    
    _profilePercentage = [NSString stringWithFormat:@"%@ %%", [[ParserUtils numberValue:data key:KEY_PERCENTAGE defaultValue:[NSNumber numberWithInt:0]] stringValue]];
    _profilePrivacy = [ParserUtils stringValue:data key:KEY_PROFILE_PRIVACY defaultValue:PROFILE_PRIVACY_PUBLIC];
    _subscriptionExpiray = [DateUtils getDateFromUnixTimeStamp:[ParserUtils numberValue:subscription key:KEY_SUBSCRIPTION_EXPIRAY]];
    
    _pending = [ParserUtils array:data key:KEY_PENDING];
    _following = [ParserUtils array:data key:KEY_FOLLOWING];
    
    [self setCity:[StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_CITY]]];
    [self setState:[StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_STATE]]];
    [self setCountry:[StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_COUNTRY]]];
    
    [self setGender:[StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_GENDER]]];
    
    [self setReligion:[StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_RELIGION]]];
    [self setEthnicity:[[ParserUtils stringValue:attributes key:KEY_ETHNICITY] capitalizedString]];
    [self setPoliticalAffiliation:[[ParserUtils stringValue:attributes key:KEY_POLITICAL_AFFILIATION] capitalizedString]];
    
    [self setOrientation:[StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_ORIENTATION]]];
    [self setMaritalStatus:[StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_MARITIAL_STATUS]]];
    [self setEducationLevel:[StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_EDUCATION]]];
    
    [self setKids:[ParserUtils numberValue:attributes key:KEY_KIDS]];
    [self setSiblings:[ParserUtils numberValue:attributes key:KEY_SIBLINGS]];
    
    [self setSchools:[ParserUtils array:attributes key:KEY_SCHOOL]];
    [self setEmployers:[ParserUtils array:attributes key:KEY_EMPLOYER]];
    [self setProfessions:[ParserUtils array:attributes key:KEY_PROFESSION]];
    [self setGuruTags:[ParserUtils array:tags key:KEY_GURU_TAGS]];
    
    [self setHobbies:[ParserUtils array:attributes key:KEY_HOBBIES]];
    _explorerTags = [ParserUtils array:tags key:KEY_EXPLORER_TAGS];
    
    [self setThumbsUps:[ParserUtils intValue:rating key:KEY_RATING_THUMBS_UP defaultValue:0]];
    [self setThumbsDown:[ParserUtils intValue:rating key:KEY_RATING_THUMBS_DOWN defaultValue:0]];
    [self setThanks:[ParserUtils intValue:rating key:KEY_RATING_THANKS defaultValue:0]];
    
    [self setChats:[ParserUtils intValue:data key:KEY_CHATS defaultValue:0]];
    [self setMemberSince:[ParserUtils numberValue:data key:KEY_MEMBER_SINCE]];
    [self setQuestionsAnswered:[ParserUtils intValue:data key:KEY_QUESTIONS_ANSWERED defaultValue:0]];
    
    [self setTurtleIdentifier:[ParserUtils stringValue:data key:KEY_TURTLE_IDENTIFIER]];
    
    [self setLevel:[ParserUtils intValue:data key:KEY_LEVEL]];
    
    [self setStage:[ParserUtils stringValue:data key:KEY_STAGE]];
    [self setPoints:[ParserUtils intValue:turtle key:KEY_TURTLE_POINTS]];
    [self setBucks:[ParserUtils intValue:turtle key:KEY_TURTLE_BUCKS]];
    [self setIsUserPremium:!([[ParserUtils stringValue:subscription key:KEY_TYPE] isEqualToString:KEY_SUBSCRIPTION_TYPE_FREE])];
}

-(void)setSeekerProfile:(NSDictionary*)seeker{
    _firstName = [StringUtils sentenceCapitalizedString:[ParserUtils stringValue:seeker key:KEY_FIRST_NAME]];
    _lastName = [StringUtils sentenceCapitalizedString:[ParserUtils stringValue:seeker key:KEY_LAST_NAME]];
    _userId = [ParserUtils stringValue:seeker key:@"id"];
    [self setTurtleIdentifier:[ParserUtils stringValue:seeker key:KEY_TURTLE_IDENTIFIER]];
}

-(void)setSeekerProfile2:(NSDictionary *)seeker{//Temporary method.
    _firstName = [StringUtils sentenceCapitalizedString:[ParserUtils stringValue:[ParserUtils object:[ParserUtils object:seeker key:KEY_PROFILE] key:KEY_ATTRIBUTES] key:KEY_FIRST_NAME]];
    _lastName = [StringUtils sentenceCapitalizedString:[ParserUtils stringValue:[ParserUtils object:[ParserUtils object:seeker key:KEY_PROFILE] key:KEY_ATTRIBUTES] key:KEY_LAST_NAME]];
    _userId = [ParserUtils stringValue:seeker key:@"id"];
    [self setTurtleIdentifier:[ParserUtils stringValue:seeker key:KEY_TURTLE_IDENTIFIER]];
    NSString *avatarId = [ParserUtils stringValue:[ParserUtils object:seeker key:KEY_PROFILE] key:KEY_AVATAR];
    _imageURL = (avatarId)?[NSString stringWithFormat:PHOTO_URL_STRING, SERVICE_URL, avatarId, [UserDefaults getAccessToken]]:nil;
}

#pragma mark - Entity to Array Conversion

- (NSArray *)getAsArrayWithEmptyValues:(BOOL)includeEmptyValues
{
    _includeEmptyValues = includeEmptyValues;
    _arrayForEntity = [NSMutableArray new];
    
    if (includeEmptyValues)
    {
        //[self setProfileCellEntityWithHeader:HEADER_PROFILE_PICTURE content:@"" stepInWizard:0 andResponseKey:@""];
        [self setProfileCellEntityWithHeader:HEADER_NAME content:[self fullName] stepInWizard:0 andResponseKey:KEY_FIRST_NAME];
    }
    
    [self setProfileCellEntityWithHeader:HEADER_ABOUT_ME content:_about stepInWizard:-1 andResponseKey:KEY_ABOUT];
    
    [self setProfileCellEntityWithHeader:HEADER_DOB content:[self getDateOfBirthFromDate] stepInWizard:0 andResponseKey:KEY_DOB];
    [self setProfileCellEntityWithHeader:HEADER_GENDER content:[self gender] stepInWizard:0 andResponseKey:KEY_GENDER];
    
    [self setProfileCellEntityWithHeader:HEADER_LOCATION content:[self location] stepInWizard:1 andResponseKey:KEY_COUNTRY];
    
    [self setProfileCellEntityWithHeader:HEADER_ORIENTATION content:[self orientation] stepInWizard:2 andResponseKey:KEY_ORIENTATION];
    [self setProfileCellEntityWithHeader:HEADER_RELIGION content:[self religion] stepInWizard:2 andResponseKey:KEY_RELIGION];
    [self setProfileCellEntityWithHeader:HEADER_ETHNICITY content:[self ethnicity] stepInWizard:2 andResponseKey:KEY_ETHNICITY];
    [self setProfileCellEntityWithHeader:HEADER_POLITICAL_AFFILIATION content:[self politicalAffiliation] stepInWizard:2 andResponseKey:KEY_POLITICAL_AFFILIATION];
    
    [self setProfileCellEntityWithHeader:HEADER_MARITAL_STATUS content:[self maritalStatus] stepInWizard:3 andResponseKey:KEY_MARITIAL_STATUS];
    
    [self setProfileCellEntityWithHeader:HEADER_KIDS content:[[self kids] stringValue] stepInWizard:3 andResponseKey:KEY_KIDS];
    [self setProfileCellEntityWithHeader:HEADER_SIBLINGS content:[[self siblings] stringValue] stepInWizard:3 andResponseKey:KEY_SIBLINGS];
    
    [self setProfileCellEntityWithHeader:HEADER_EDUCATION content:[self educationLevel] stepInWizard:4 andResponseKey:KEY_EDUCATION];
    
    [self setProfileCellEntityWithHeader:HEADER_SCHOOL content:[self getCommaSepartedFrom:[self schools]] stepInWizard:5 andResponseKey:KEY_SCHOOL];
    [self setProfileCellEntityWithHeader:HEADER_PROFESSION content:[self getCommaSepartedFrom:[self professions]] stepInWizard:6 andResponseKey:KEY_PROFESSION];
    [self setProfileCellEntityWithHeader:HEADER_EMPLOYER content:[self getCommaSepartedFrom:[self employers]] stepInWizard:6 andResponseKey:KEY_EMPLOYER];
    [self setProfileCellEntityWithHeader:HEADER_HOBBIES content:[self getCommaSepartedFrom:_hobbies] stepInWizard:7 andResponseKey:KEY_HOBBIES];
    
//    [self setProfileCellEntityWithHeader:HEADER_GURU_TAGS content:[self getCommaSepartedFrom:[self guruTags]] stepInWizard:8 andResponseKey:KEY_GURU_TAGS];
//    [self setProfileCellEntityWithHeader:HEADER_EXPLORER_TAGS content:[self getCommaSepartedFrom:[self explorerTags]] stepInWizard:8 andResponseKey:KEY_EXPLORER_TAGS];

    
//     [self setProfileCellEntityWithHeader:HEADER_SIGN_UP_AS content:[self getCommaSepartedFrom:[self explorerTags]] stepInWizard:9 andResponseKey:KEY_SIGNUP_AS];
    
    if ([[UserDefaults getPageID] length] == 0) {
        [self setProfileCellEntityWithHeader:HEADER_BRANDED_PAGE_NAME content:[self getCommaSepartedFrom:[self explorerTags]] stepInWizard:8 andResponseKey:KEY_NAME];
    }
    
//     [self setProfileCellEntityWithHeader:HEADER_SUMMARY_TAG content:[self getCommaSepartedFrom:[self explorerTags]] stepInWizard:11 andResponseKey:KEY_SUMMARY];
//    
//     [self setProfileCellEntityWithHeader:HEADER_ARTICLE_ONE_URL content:[self getCommaSepartedFrom:[self explorerTags]] stepInWizard:12 andResponseKey:KEY_ARTICLE_ONE_URL];
//    
//     [self setProfileCellEntityWithHeader:HEADER_ARTICLE_TWO_URL content:[self getCommaSepartedFrom:[self explorerTags]] stepInWizard:13 andResponseKey:KEY_ARTICLE_TWO_URL];
//    
//     [self setProfileCellEntityWithHeader:HEADER_ARTICLE_THREE_URL content:[self getCommaSepartedFrom:[self explorerTags]] stepInWizard:14 andResponseKey:KEY_ARTICLE_THREE_URL];
//    
//     [self setProfileCellEntityWithHeader:HEADER_EVENT_ONE_URL content:[self getCommaSepartedFrom:[self explorerTags]] stepInWizard:15 andResponseKey:KEY_EVENT_ONE_URL];
//    
//     [self setProfileCellEntityWithHeader:HEADER_EVENT_TWO_URL content:[self getCommaSepartedFrom:[self explorerTags]] stepInWizard:16 andResponseKey:KEY_EVENT_TWO_URL];
//    
//     [self setProfileCellEntityWithHeader:HEADER_EVENT_THREE_URL content:[self getCommaSepartedFrom:[self explorerTags]] stepInWizard:17 andResponseKey:KEY_EVENT_THREE_URL];
//    
//     [self setProfileCellEntityWithHeader:HEADER_PUBLIC_PRIVATE content:[self getCommaSepartedFrom:[self explorerTags]] stepInWizard:18 andResponseKey:KEY_PUBLIC_OR_PRIVATE];

    
    return _arrayForEntity;
}

//TODO: Need Refactor (Too Long Method)

- (void)setProfileCellEntityWithHeader:(NSString *)header content:(NSString *)content stepInWizard:(NSInteger)step andResponseKey:(NSString *)responseKey
{
    if (_includeEmptyValues)
    {
        [_arrayForEntity addObject:[[ProfileCell alloc] initWithHeader:header
                                                               content:content
                                                          stepInWizard:step
                                                           responseKey:responseKey
                                                            andPrivacy:[self getPrivacyForAttribute:responseKey]]];
        
        return;
    }
    
    if (![StringUtils isEmptyOrNull:content])
    {
        [_arrayForEntity addObject:[[ProfileCell alloc] initWithHeader:header
                                                               content:content
                                                          stepInWizard:step
                                                           responseKey:responseKey
                                                            andPrivacy:[self getPrivacyForAttribute:responseKey]]];
    }
}

- (NSString *)getCommaSepartedFrom:(NSArray *)targetArray
{
    return [targetArray componentsJoinedByString:@", "];
}

#pragma mark - Entity to Dictionary Conversion

- (NSDictionary *)getAsDictionary
{
    NSMutableDictionary *jsonEntity = [NSMutableDictionary new];
    NSMutableDictionary *tags = [NSMutableDictionary new];
    

    if (self.name == nil) {
    
        NSMutableDictionary *profileAttributes = [[NSMutableDictionary alloc] initWithDictionary:[super getAsDictionary]];
        [self setString:_firstName inDictionary:profileAttributes forKey:KEY_FIRST_NAME];
        [self setString:_lastName inDictionary:profileAttributes forKey:KEY_LAST_NAME];
        
        [self setString:_about inDictionary:profileAttributes forKey:KEY_ABOUT];
        
        [self setArray:_hobbies inDictionary:profileAttributes forKey:KEY_HOBBIES];
        [self setArray:[self getAllPrivateAttributes] inDictionary:jsonEntity forKey:KEY_PRIVATE_ATTRIBUTES];
        
        [self setArray:_explorerTags inDictionary:tags forKey:KEY_EXPLORER_TAGS];
        [self setArray:[self guruTags] inDictionary:tags forKey:KEY_GURU_TAGS];
        
        [self setNumber:[DateUtils getUnixTimeStamp:[self dateOfBirth]] inDictionary:profileAttributes forKey:KEY_DOB];
        
        [self setArray:_pending inDictionary:profileAttributes forKey:KEY_PENDING];
        [self setArray:_following inDictionary:profileAttributes forKey:KEY_FOLLOWING];
        
        if (_pages.count > 0) {
            [jsonEntity setObject:_pages forKey:KEY_PAGES];
        }
        
        [jsonEntity setObject:[UserDefaults getUserID] forKey:KEY_USER_ID];
        [jsonEntity setObject:profileAttributes forKey:KEY_ATTRIBUTES];
        
        if (!_profilePrivacy)
        {
            _profilePrivacy = PROFILE_PRIVACY_PUBLIC;
        }
        
        [jsonEntity setObject:_profilePrivacy forKey:KEY_PROFILE_PRIVACY];
        [jsonEntity setObject:tags forKey:KEY_TAGS];
 
    } else {
    
//        NSMutableDictionary *brandedPageAttributes = [[NSMutableDictionary alloc] initWithDictionary:[super getBrandedDataAsDictionary]];
//        
//        [self setString:[self name] inDictionary:brandedPageAttributes forKey:KEY_NAME];
//        [self setString:[UserDefaults getUserID] inDictionary:brandedPageAttributes forKey:KEY_OWNER];
//        [self setString:[self signUpAs] inDictionary:brandedPageAttributes forKey:KEY_SIGNUP_AS];
//        [jsonEntity setObject:_profilePrivacy forKey:KEY_PROFILE_PRIVACY];
//        [jsonEntity addEntriesFromDictionary:brandedPageAttributes];
  
    }
    
    
    return jsonEntity;
}


#pragma mark - Helper Methods

- (NSString *)fullName
{
    if (_firstName.length > 0 && _lastName.length > 0) {
        return [_firstName stringByAppendingString:[NSString stringWithFormat:@" %@", _lastName]];
    }
    if (_lastName.length > 0) {
        return _lastName;
    }
    
    if (_lastName.length == 0 && _firstName.length == 0)
    {
        return [self getDisplayNameOrTurtleIdentifier];
    }
    
    return _firstName;
}

- (NSString *)displayName
{
    if (self.lastName.length==0) {
        return self.firstName;
    }
    if (self.firstName.length==0) {
        return self.lastName;
    }
    return [NSString stringWithFormat:@"%@ %@.",self.firstName ? self.firstName : @"",[[self.lastName ? self.lastName : @"" substringWithRange:NSMakeRange(0, 1)] uppercaseString]];
}

-(NSString*)getDisplayNameOrTurtleIdentifier
{
    if(!self.firstName || !self.lastName || (self.firstName.length==0 && self.lastName.length==0))
        return self.turtleIdentifier;
    
    return [self displayName];
}

- (NSString *)location
{
    NSString *location = @"";
    
    NSString *holdString = [self city];
    
    if (![StringUtils isEmptyOrNull:holdString])
    {
        location = [location stringByAppendingString:[NSString stringWithFormat:@"%@, ", holdString]];
    }
    
    holdString = [self state];
    if (![StringUtils isEmptyOrNull:holdString])
    {
        location = [location stringByAppendingString:[NSString stringWithFormat:@"%@, ", holdString]];
    }
    
    holdString = [self country];
    if (![StringUtils isEmptyOrNull:holdString])
    {
        location = [location stringByAppendingString:[NSString stringWithFormat:@"%@ ", holdString]];
    }
    
    return [StringUtils removeSuffix:@", " from:location];
}

#pragma mark - Date Of Birth Handling

- (NSString *)getDateOfBirthFromDate
{
    if (!_dateOfBirth)
    {
        return @"";
    }
    
    return [DateUtils stringFromUnixTimeStamp:[NSNumber numberWithFloat:[_dateOfBirth timeIntervalSince1970]]];
}

- (NSDate *)parseDateOfBirth:(NSNumber *)dateNumber
{
    if (!dateNumber || dateNumber == 0)
    {
        return NULL;
    }
    
    return [NSDate dateWithTimeIntervalSince1970:[dateNumber doubleValue] / 1000.0];
}

- (NSNumber *)getNumberForDate
{
    if (!_dateOfBirth)
    {
        return NULL;
    }
    
    return [NSNumber numberWithDouble:[_dateOfBirth timeIntervalSince1970]];
}

- (NSString *)getMemberSince
{
    if (!_memberSince)
    {
        return @"";
    }
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[_memberSince doubleValue]/1000];
    return [DateUtils stringFromDate:date withFormat:@"MMM yyyy" inUTC:NO];
}

#pragma mark - Profile And Attribute Privacy Handling

- (BOOL)isProfilePublic;
{
    return ([StringUtils compareString:_profilePrivacy withString:PROFILE_PRIVACY_PUBLIC]);
}

- (void)updateProfilePrivacy:(BOOL)isPublic
{
    _profilePrivacy = (isPublic) ? PROFILE_PRIVACY_PUBLIC : PROFILE_PRIVACY_PRIVATE;
}

- (ProfileAttributeAccess)getPrivacyForAttribute:(NSString *)attributeKey
{
    //If Profile is Private OR Attribute is Either Guru/Explorer Tag. Privacy doesnot apply
    if (![self isProfilePublic] || [attributeKey isEqualToString:KEY_EXPLORER_TAGS] || [attributeKey isEqualToString:KEY_GURU_TAGS])
    {
        return ProfileAttributeAccessNotApplicable;
    }
    
    return ![_privateAttributes containsObject:attributeKey];
}

- (NSArray *)getAllPrivateAttributes
{
    if ([_privateAttributes containsObject:KEY_FIRST_NAME] || [_privateAttributes containsObject:KEY_LAST_NAME])
    {
        [_privateAttributes addObject:KEY_FIRST_NAME];
        [_privateAttributes addObject:KEY_LAST_NAME];
    }
    return [_privateAttributes allObjects];
}

#pragma mark - NSCoder Methods

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    
    if (self != nil)
    {
        _firstName          = [coder decodeObjectForKey:KEY_FIRST_NAME];
        _lastName           = [coder decodeObjectForKey:KEY_LAST_NAME];
        
        _level              = [coder decodeIntegerForKey:KEY_LEVEL];
        _dateOfBirth        = [coder decodeObjectForKey:KEY_DOB];
        _thumbsUps          = [coder decodeIntegerForKey:KEY_RATING_THUMBS_UP];
        _thumbsDown         = [coder decodeIntegerForKey:KEY_RATING_THUMBS_DOWN];
        _thanks             = [coder decodeIntegerForKey:KEY_RATING_THANKS];
        
        _stage              = [coder decodeObjectForKey:KEY_STAGE];
        _points             = [coder decodeIntegerForKey:KEY_TURTLE_POINTS];
        _bucks              = [coder decodeIntegerForKey:KEY_TURTLE_BUCKS];
        _isUserPremium      = [coder decodeBoolForKey:KEY_IS_SUBSCRIBED];
        
        self.gender         = [coder decodeObjectForKey:KEY_GENDER];
        self.country        = [coder decodeObjectForKey:KEY_COUNTRY];
        self.state          = [coder decodeObjectForKey:KEY_STATE];
        self.city           = [coder decodeObjectForKey:KEY_CITY];
        self.orientation    = [coder decodeObjectForKey:KEY_ORIENTATION];
        self.religion       = [coder decodeObjectForKey:KEY_RELIGION];
        self.ethnicity      = [coder decodeObjectForKey:KEY_ETHNICITY];
        self.politicalAffiliation = [coder decodeObjectForKey:KEY_POLITICAL_AFFILIATION];
        self.maritalStatus  = [coder decodeObjectForKey:KEY_MARITIAL_STATUS];
        self.educationLevel = [coder decodeObjectForKey:KEY_EDUCATION];
        
        self.schools        = [coder decodeObjectForKey:KEY_SCHOOL];
        self.professions    = [coder decodeObjectForKey:KEY_PROFESSION];
        self.employers      = [coder decodeObjectForKey:KEY_EMPLOYER];
        self.guruTags       = [coder decodeObjectForKey:KEY_GURU_TAGS];

        self.kids           = [coder decodeObjectForKey:KEY_KIDS];
        self.siblings       = [coder decodeObjectForKey:KEY_SIBLINGS];
        
        self.pending        = [coder decodeObjectForKey:KEY_PENDING];
        self.following      = [coder decodeObjectForKey:KEY_FOLLOWING];
        
        self.pages          = [coder decodeObjectForKey:KEY_PAGES];
        
        self.emailVerified = [[coder decodeObjectForKey:KEY_EMAIL_VERIFIED] boolValue];
        
        self.turtleIdentifier = [coder decodeObjectForKey:KEY_TURTLE_IDENTIFIER];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:_firstName          forKey:KEY_FIRST_NAME];
    [coder encodeObject:_lastName           forKey:KEY_LAST_NAME];
    
    [coder encodeInteger:_level             forKey:KEY_LEVEL];
    [coder encodeObject:_dateOfBirth        forKey:KEY_DOB];
    [coder encodeInteger:_thumbsUps         forKey:KEY_RATING_THUMBS_UP];
    [coder encodeInteger:_thumbsDown        forKey:KEY_RATING_THUMBS_DOWN];
    [coder encodeInteger:_thanks            forKey:KEY_RATING_THANKS];
    
    [coder encodeObject:_stage              forKey:KEY_STAGE];
    [coder encodeInteger:_points            forKey:KEY_TURTLE_POINTS];
    [coder encodeInteger:_bucks             forKey:KEY_TURTLE_BUCKS];
    [coder encodeBool:_isUserPremium forKey:KEY_IS_SUBSCRIBED];
    
    [coder encodeObject:self.gender         forKey:KEY_GENDER];
    [coder encodeObject:self.country        forKey:KEY_COUNTRY];
    [coder encodeObject:self.state          forKey:KEY_STATE];
    [coder encodeObject:self.city           forKey:KEY_CITY];
    [coder encodeObject:self.orientation    forKey:KEY_ORIENTATION];
    [coder encodeObject:self.religion       forKey:KEY_RELIGION];
    [coder encodeObject:self.ethnicity      forKey:KEY_ETHNICITY];
    [coder encodeObject:self.politicalAffiliation forKey:KEY_POLITICAL_AFFILIATION];
    [coder encodeObject:self.maritalStatus  forKey:KEY_MARITIAL_STATUS];
    [coder encodeObject:self.educationLevel forKey:KEY_EDUCATION];
    
    [coder encodeObject:self.schools        forKey:KEY_SCHOOL];
    [coder encodeObject:self.professions    forKey:KEY_PROFESSION];
    [coder encodeObject:self.employers      forKey:KEY_EMPLOYER];
    [coder encodeObject:self.guruTags       forKey:KEY_GURU_TAGS];
    
    [coder encodeObject:self.kids           forKey:KEY_KIDS];
    [coder encodeObject:self.siblings       forKey:KEY_SIBLINGS];
    
    [coder encodeObject:self.pending forKey:KEY_PENDING];
    [coder encodeObject:self.following forKey:KEY_FOLLOWING];
    
    [coder encodeObject:self.pages forKey:KEY_PAGES];
    
    [coder encodeObject:[NSNumber numberWithBool:self.emailVerified] forKey:KEY_EMAIL_VERIFIED];
    
    [coder encodeObject:self.turtleIdentifier forKey:KEY_TURTLE_IDENTIFIER];
}

@end
