//
//  SeekingAdvisor.h
//  TurtleWise
//
//  Created by Waleed Khan on 12/29/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseProfile.h"

@class UserProfile;
@interface SeekingAdvisor : BaseProfile

@property(nonatomic, strong) NSString *question;
@property(nonatomic, strong) NSArray *questionOptions;
@property(nonatomic, strong) NSString *questionType;
@property(nonatomic, strong) NSString *searchOptions;
@property(nonatomic, strong) NSString *minAge;
@property(nonatomic, strong) NSString *maxAge;

@property(nonatomic, strong) NSArray *list;
@property(nonatomic, strong) NSString *pageName;
@property(nonatomic, strong) NSString *who;

- (NSMutableArray *)attributeHashTags;
- (void)resetValueForTag:(NSString *)tag;
- (void)resetAllTags;

- (id)initWithUserProfile:(UserProfile *)userProfile;

@end
