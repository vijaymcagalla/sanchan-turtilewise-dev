//
//  Answer.m
//  TurtleWise
//
//  Created by Irfan Gul on 25/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "Answer.h"
#import "Keys.h"

#define KEY_ANSWER @"answer"
#define KEY_REASON @"reason"
#define KEY_FEEDBACK @"feedback"
#define KEY_CREATED_AT @"createdAt"
#define KEY_WHY_CHOOSE_ME @"whyChooseMe"
#define KEY_CHAT_AVAILABILITY @"chatAvailability"
#define KEY_FEEDBACK_BY_SEEKER @"feedbackBySeeker"

@implementation Answer

-(void)set:(NSDictionary*)input{
    
    NSDictionary *data = [ParserUtils object:input key:KEY_API_DATA];
    BOOL isAdvisorAnswer = (data)? YES:NO;
    
    if (isAdvisorAnswer)
    {
        NSDictionary *answer = [ParserUtils object:data key:KEY_ANSWER];
        input = answer;
    }
    
    self.answerId = [ParserUtils stringValue:input key:KEY_CONTENT_ID];
    self.answerText = [ParserUtils stringValue:input key:KEY_ANSWER];
    self.reason = [ParserUtils stringValue:input key:KEY_REASON];
    self.whyChooseMe = [ParserUtils stringValue:input key:KEY_WHY_CHOOSE_ME];
    self.createdAt = [ParserUtils numberValue:input key:KEY_CREATED_AT];
    self.seekersFeedback = [ParserUtils stringValue:input key:(isAdvisorAnswer)?KEY_FEEDBACK:KEY_FEEDBACK_BY_SEEKER];
    self.advisorAvailableForChat = [ParserUtils boolValue:input key:KEY_CHAT_AVAILABILITY];
    
    Advisor *advisor = [Advisor new];
    [advisor set:[ParserUtils object:input key:KEY_ADVISOR]];
    advisor.chatAvailability = [ParserUtils boolValue:input key:KEY_CHAT_AVAILABILITY defaultValue:NO];
    self.advisor = advisor;
}

@end
