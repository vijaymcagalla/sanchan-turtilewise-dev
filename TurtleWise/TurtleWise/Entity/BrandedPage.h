//
//  BrandedPage.h
//  TurtleWise
//
//  Created by Sunflower on 8/22/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseEntity.h"

@interface BrandedPage : BaseEntity {
    
    NSString * ownerId;
    NSString * pageId;
    NSString * type;
    NSString * name;
    NSString * privacy;
    NSString * summary;
    NSString * facebookLink;
    NSString * twitterLink;
    NSString * linkedInLink;
    UIImage * cover;
    UIImage * avatar;
    NSMutableArray * articles;
    NSMutableArray * events;
    NSMutableArray * pending;
    NSMutableArray * approved;
}

@property(nonatomic, retain) NSString* ownerId;
@property(nonatomic, retain) NSString* pageId;
@property(nonatomic, retain) NSString* type;
@property(nonatomic, retain) NSString* name;
@property(nonatomic, retain) NSString* privacy;
@property(nonatomic, retain) NSString* summary;
@property(nonatomic, retain) NSString* facebookLink;
@property(nonatomic, retain) NSString* twitterLink;
@property(nonatomic, retain) NSString* linkedInLink;
@property(nonatomic, retain) UIImage* cover;
@property(nonatomic, retain) UIImage* avatar;
@property(nonatomic, retain) NSMutableArray* articles;
@property(nonatomic, retain) NSMutableArray* events;
@property(nonatomic, retain) NSMutableArray* pending;
@property(nonatomic, retain) NSMutableArray* approved;


- (BrandedPage *) initWithDictionary:(NSDictionary*) dictionary;
//- (NSDictionary*) getDictionary;
@end
