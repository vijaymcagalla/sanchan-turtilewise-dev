//
//  QuestionTypeController.m
//  TurtleWise
//
//  Created by Irfan Gul on 05/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "AdvisorSelectionController.h"
#import "SubmitQuestionController.h"
#import "MCQOptionsController.h"
#import "AdvisorSelectionView.h"
#import "TWWizardManager.h"
#import "SeekingAdvisor.h"
#import "UserDefaults.h"
#import "UserProfile.h"

#define TITLE @"Choose Type"

@interface AdvisorSelectionController () <MCQOptionsControllerDelegate>

@property(nonatomic, strong) NSString *question;
@property(nonatomic, strong) NSMutableDictionary *answerer;
@property(nonatomic, strong) MCQOptionsController *mcqOptionController;

- (void)setupNavigationButtons;

@end

@implementation AdvisorSelectionController

#pragma mark - Life Cycle Methods

- (id)initWithQuestion:(NSString *)question andAnswerer:(NSMutableDictionary*)dic
{
    if (self = [super init])
    {
        _question = question;
        _answerer = dic;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:TITLE];
    [self setupNavigationButtons];
    
    _profileWizardManager = [[TWWizardManager alloc] initWithPresentingController:self];
    [_profileWizardManager setProfile:[SeekingAdvisor new]];
    
    [super loadServices:@[@(ServiceTypeTags)]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setAppearenceForAddAdvisorWizard];
    
    if (_answerer.count > 0) {
        if ([[_answerer valueForKey:KEY_WHO] isEqualToString:@"owner"]) {
            [(AdvisorSelectionView *)self.view setupHiddenUI];
        } else {
            [(AdvisorSelectionView *)self.view setupShowUI];
        }
    }
    
}

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - Helper Method

- (void)saveGuruTags:(NSArray *)guruTags
{
    SeekingAdvisor *seekingAdvisor = (SeekingAdvisor *)[_profileWizardManager profile];
    
    if (!seekingAdvisor)
    {
        seekingAdvisor = [SeekingAdvisor new];
    }
    
    [seekingAdvisor setGuruTags:guruTags];
    [_profileWizardManager setProfile:seekingAdvisor];
}

- (void)clearMCQOptionsController
{
    _mcqOptionController = NULL;
}

#pragma mark - Navigation

- (void)startWizardForAdvisorAttributes
{
    [_profileWizardManager startAdvisorWizard];
}

- (void)proceedToSubmitQuestion
{
    [(SeekingAdvisor *)[_profileWizardManager profile] setQuestion:_question];
    if (_answerer != NULL) {
        [(SeekingAdvisor *)[_profileWizardManager profile] setPageName:[_answerer valueForKey:KEY_PAGENAME]];
        [(SeekingAdvisor *)[_profileWizardManager profile] setWho:[_answerer valueForKey:KEY_WHO]];
        [(SeekingAdvisor *)[_profileWizardManager profile] setList:[_answerer objectForKey:KEY_LIST]];
    }
    
    SubmitQuestionController *submitQuestionController = [[SubmitQuestionController alloc] initWithSeekingAdvisor:(SeekingAdvisor *)[_profileWizardManager profile]];
    
    [[self navigationController] pushViewController:submitQuestionController animated:YES];
}

- (void)proceedToSelectChoicesForMCQ
{
    if (!_mcqOptionController)
    {
        _mcqOptionController = [[MCQOptionsController alloc] initWithDelegate:self];
    }
    
    [[self navigationController] pushViewController:_mcqOptionController animated:YES];
}

#pragma mark - MCQOptionsController Delegate Method

- (void)saveMCQChoices:(NSArray *)choices
{
    [(SeekingAdvisor *)[_profileWizardManager profile] setQuestionOptions:choices];
}

#pragma mark - Service Call

- (void)requestSuggestedTagsFor:(NSString *)tag success:(void (^)(NSArray *))success
{
    [[service tags] getSuggestedTagsFor:tag withSuccess:^(NSArray * response)
     {
         if (success)
             success(response);
     }];
}

- (void)requestToSaveTag:(NSString *)tag
{
    [[service tags] saveTag:tag];
}

@end
