//
//  SocialLoginController.m
//  TurtleWise
//
//  Created by Irfan Gul on 02/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "RegistrationController.h"
#import "SocialLoginController.h"
#import "AboutMeController.h"
#import "AnalyticsHelper.h"
#import "LoginController.h"
#import "FacebookManager.h"
#import "LinkedInManager.h"
#import "SocketIOManager.h"
#import "UserDefaults.h"
#import "AuthResponse.h"
#import "AppDelegate.h"
#import "Alert.h"

#define APP_NAME_FACEBOOK @"facebook"
#define APP_NAME_LINKED_IN @"linkedin"

@interface SocialLoginController ()

- (void)loginWithSocial:(NSString *)appName andAccessToken:(NSString *)accessToken;
- (void)openAboutMeController;
- (void)openHomeController;

@end

@implementation SocialLoginController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super loadServices:@[@(ServiceTypeAuth)]];
    [[self navigationController] setNavigationBarHidden:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Social Login

-(void)startFacebookLoginProcess
{
    [FacebookManager loginFromController:self WithSuccess:^(id response)
    {
        [self showLoader];
        [self loginWithSocial:APP_NAME_FACEBOOK andAccessToken:(NSString *)response];
    }
        failure:^(NSError *error)
    {
        [self serviceCallFailed:error];
    }];

}

- (void)startLinkedInLoginProcess
{
    [LinkedInManager startLoginProcessWithSuccess:self WithSuccess:^(id response)
    {
        [self showLoader];
        [self loginWithSocial:APP_NAME_LINKED_IN andAccessToken:(NSString *)response];
    }
    andfailure:^(NSError *error)
    {
        [Alert show:error];
    }];
}

#pragma mark - Navigation

-(void)openRegisterController
{
    [self.navigationController pushViewController:[RegistrationController new] animated:YES];
}

-(void)openLoginController
{
    [self.navigationController pushViewController:[LoginController new] animated:YES];
}

- (void)openAboutMeController
{
    AboutMeController * controller = [AboutMeController new];
    [controller setShouldDisplayName:YES];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)openHomeController
{
    [UserDefaults setShouldCheckForUnratedChats:YES];
    [self.navigationController setViewControllers:@[[APP_DELEGATE getHomeControllerWithSlideMenu]] animated:YES];
}

#pragma mark - Service Calls

- (void)loginWithSocial:(NSString *)appName andAccessToken:(NSString *)accessToken
{
    [[service auth] loginWithSocial:appName andAccessToken:accessToken withSuccess:^(id response)
    {
        [self loginSucessfull:appName response:response];
    }
       andfailure:^(NSError *error)
    {
        [self serviceCallFailed:error];
    }];
}

#pragma mark - Service Callbacks

- (void)loginSucessfull:(NSString *)appName response:(id)response
{
    [self hideLoader];
    [UserDefaults setLoginFlowType:LoginFlowTypeSocial];
    [UserDefaults setUserRole:UserRoleAdvisor];
    
    if ([(AuthResponse *)response isNewUser])
    {
        [AnalyticsHelper logEvent:self
                         category:CATEGORY_AUTHORIZATION
                           action:EVENT_SIGNUP
                            label:[appName isEqualToString:APP_NAME_FACEBOOK]? LABEL_FACEBOOK : LABEL_LINKEDIN
                            value:NULL];

        [self openAboutMeController];
        return;
    }
    
    [AnalyticsHelper logEvent:self
                     category:CATEGORY_AUTHORIZATION
                       action:EVENT_SIGNIN
                        label:[appName isEqualToString:APP_NAME_FACEBOOK]? LABEL_FACEBOOK : LABEL_LINKEDIN
                        value:NULL];

    [[SocketIOManager instance] connectScocket];
    
    [self openHomeController];
}

@end
