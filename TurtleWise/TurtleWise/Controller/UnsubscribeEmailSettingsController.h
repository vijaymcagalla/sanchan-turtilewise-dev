//
//  UnsubscribeEmailSettingsController.h
//  TurtleWise
//
//  Created by Usman Asif on 11/15/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"

@interface UnsubscribeEmailSettingsController : BaseController

@property (nonatomic) NSString * unsubscribeToken;
@end
