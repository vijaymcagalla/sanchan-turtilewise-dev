//
//  UserTagsController.h
//  TurtleWise
//
//  Created by Waleed Khan on 1/8/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardController.h"

@interface UserTagsController : BaseProfileWizardController

- (void)requestSuggestedTagsFor:(NSString *)tag success:(void (^)(NSArray * tags))success;
- (void)requestToSaveTag:(NSString *)tag;
@end
