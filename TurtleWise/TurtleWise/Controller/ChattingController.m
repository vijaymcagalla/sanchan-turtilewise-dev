//
//  ChattingController.m
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "NewCheckAdvisorController.h"
#import "RatingAdvisorsController.h"
#import "MessagesPagedResponse.h"
#import "ChatOptionsController.h"
#import "ChattingController.h"
#import "Conversation.h"
#import "ChattingView.h"
#import "UserDefaults.h"
#import "StringUtils.h"
#import "ChatMessage.h"
#import "AppDelegate.h"
#import "UserProfile.h"
#import "Question.h"
#import "Advisor.h"
#import "Alert.h"

//Constants
#define SCREEN_TITLE @"Chat"
#define MESSAGE_TYPE @"text"

//Keys
#define KEY_API_MESSAGE_TYPE @"type"

@interface ChattingController ()

@property(nonatomic, strong) NSString *conversationId;
@property(nonatomic, strong) ChattingView *view;
@property(nonatomic, strong) Conversation *conversation;
@property(nonatomic, strong) MessagesPagedResponse *messagePagedResponse;

- (void)commonInitialization;
- (void)initiateServiceCalls;
- (void)customizeNavigationBarForUserRole:(UserRole)userRole;
- (void)determineUserRoleBaseOnNotification;

- (void)getMessagesForConversationWithReset:(BOOL)shouldReset;
- (void)getConversationDetails;

- (void)gotMessagesSucessfully;
- (void)gotConversationDetailsSucessfully:(Conversation *)conversation;
- (void)gettingMessagesFailed:(NSError *)error;

@end

@implementation ChattingController
@dynamic view;

#pragma mark - Life Cycle Methods

- (id)initWithConversation:(Conversation *)conversation {
    if (self = [super init]) {
        _conversation = conversation;
        _conversationId = [conversation conversationId];
        
        [self commonInitialization];
    }
    
    return self;
}

- (id)initWithConversationId:(NSString *)conversationId {
    if (self = [super init]) {
        _conversationId = conversationId;
        
        [self commonInitialization];
    }
    
    return self;
}

- (id)initWithConversation:(Conversation *)conversation withMessage:(NSString *)message {
    if (![StringUtils isEmptyOrNull:message])
        [Alert show:@"SUCCESS!" andMessage:message];
    
    return [self initWithConversation:conversation];
}

- (void)commonInitialization {
    _messagePagedResponse = [MessagesPagedResponse new];
    [_messagePagedResponse setConversation:_conversation];
    
    [APP_DELEGATE setActiveConversationId:_conversationId];
}

- (void)initiateServiceCalls
{
    if (_conversation)
    {
        [self getMessagesForConversationWithReset:YES];
        [self checkIfConversationExpiredOrClosed];
        
        return;
    }
    
    [self getConversationDetails];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadServices:@[@(ServiceTypeChat)]];
    [self initiateServiceCalls];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self customizeNavigationBarForUserRole:[UserDefaults getUserRole]];
    [APP_DELEGATE setActiveConversationId:_conversationId];
}

- (void)customizeNavigationBarForUserRole:(UserRole)userRole
{    
    NSString *backButtonImage;
    
    if (userRole == UserRoleAdvisor) {
        backButtonImage = BACK_BUTTON_WHITE_IMAGE;
        [self setAppearenceForAdvisor];
    }
    
    else {
        backButtonImage = BACK_BUTTON_BLACK_IMAGE;
        [self setAppearenceForSeeker];
    }
    
    [self setupRightNavigationButtonWithName:TITLE_CHAT_OPTIONS withOnClickAction:^{
        [self goToChatOptionsController];
    }];
    
    [self.navigationItem setTitle:SCREEN_TITLE];
    
    [self setupLeftNavigationButtonWithImage:backButtonImage withOnClickAction:^{
        [APP_DELEGATE setActiveConversationId:NULL];
        [self popViewController];
    }];
}

//FIXME: Need Refactoring.
-(void)popViewController {
    if(self.popTwiceOnBackBtnTap){
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        for (UIViewController *vC in allViewControllers) {
            if ([vC isKindOfClass:[NewCheckAdvisorController class]])
                [self.navigationController popToViewController:vC animated:YES];
        }
    }
    else
        [super popViewController];
}

- (void)recievedNewMessage:(ChatMessage *)newChatMessage
{
    [[newChatMessage messageSender] determineSenderNameForConversation:_conversation];
    [[self view] updateChatForNewMessage:newChatMessage];
}

#pragma mark - Helper Methods

- (void)loadMoreMessages
{
    [self getMessagesForConversationWithReset:NO];
}

- (void)checkIfConversationExpiredOrClosed
{
    if ([_conversation status] == ConversationStatusClosed)
    {
        [[self view] markChatClosed];
    }
    
    if (!((([UserDefaults getUserRole] == UserRoleAdvisor) || ([_conversation status] == ConversationStatusActive || [_conversation isRated]))))
    {
        [[self view] markChatUnrated];
    }
}

- (NSDictionary *)getParamsForSendMessage:(NSString *)message andTemporaryId:(NSString *)tempId
{
    return @{KEY_API_MESSAGE_TYPE : MESSAGE_TYPE,
             KEY_API_MESSAGE      : message,
             KEY_CHAT_ID          :_conversationId,
             KEY_TEMP_MESSAGE_ID  : tempId
             };
}

- (void)determineUserRoleBaseOnNotification
{
    NSString *userId = [UserDefaults getUserID];
    NSString *seekerId = [[[_conversation question] seeker] userId];
    
    UserRole currentUserRole = [UserDefaults getUserRole];
    UserRole newUserRole = UserRoleAdvisor;
    
    if ([userId isEqualToString:seekerId])
    {
        newUserRole = UserRoleSeeker;
    }
    
    if (newUserRole != currentUserRole)
    {
        [UserDefaults setUserRole:newUserRole];
        [self customizeNavigationBarForUserRole:newUserRole];
    }
}

#pragma mark - Navigation

- (void)proceedToRateAdvisors
{
    RatingAdvisorsController *ratingAdvisorsController = [[RatingAdvisorsController alloc] initWithConversation:_conversation];
    [[self navigationController] pushViewController:ratingAdvisorsController animated:YES];
}

- (void)goToChatOptionsController
{
    [APP_DELEGATE setActiveConversationId:NULL];
    
    [[self view] inputbarResign];
    
    ChatOptionsController *cont = [[ChatOptionsController alloc] init];
    [cont setConversation:_conversation];
    [self.navigationController pushViewController:cont animated:YES];
}

#pragma mark - Service Calls

- (void)getMessagesForConversationWithReset:(BOOL)shouldReset
{
    if (!_conversation || [_messagePagedResponse isLoadingData] || ![_messagePagedResponse hasMoreData])
    {
        return;
    }
    
    if (shouldReset)
    {
        [self showLoader];
    }
    
    [[self view] enableActivityIndicator:YES];
    
    [_messagePagedResponse setIsLoadingData:YES];
    [[service chat] getChatMessagesByChatId:[_conversation conversationId] withSuccess:^(id response)
    {
        [self gotMessagesSucessfully];
        
    } andFailure:^(NSError *error)
    {
        [self serviceCallFailed:error];
        
    } response:_messagePagedResponse];
}

- (void)sendMessage:(NSString *)message withTemporaryId:(NSString *)temporaryId
{
    [[service chat] sendMessage:[self getParamsForSendMessage:message andTemporaryId:temporaryId]
                         toChat:[_conversation conversationId]
                    withSuccess:^(id response){}
                     andFailure:^(NSError *error){}];
}

- (void)getConversationDetails
{
    [self showLoader];
    
    [[service chat] getConversationDetails:_conversationId onSuccess:^(id response)
    {
        [self gotConversationDetailsSucessfully:response];
        
    } andFailure:^(NSError *error)
    {
        [self serviceCallFailed:error];
    }];
}

#pragma mark - Service Call Backs

- (void)gotMessagesSucessfully
{
    [[self view] enableActivityIndicator:NO];
    [self hideLoader];
    
    [_messagePagedResponse setIsLoadingData:NO];
    
     [[self view] setChatMessages:[NSMutableArray arrayWithArray:[_messagePagedResponse dataArray]]];
}

- (void)gettingMessagesFailed:(NSError *)error
{
    [[self view] enableActivityIndicator:NO];
    [_messagePagedResponse setIsLoadingData:NO];
    
    [self serviceCallFailed:error];
}

- (void)gotConversationDetailsSucessfully:(Conversation *)conversation
{
    _conversation = conversation;
    _conversationId = [conversation conversationId];
    [_messagePagedResponse setConversation:_conversation];
    
    [APP_DELEGATE setActiveConversationId:_conversationId];
    [self hideLoader];
    [self checkIfConversationExpiredOrClosed];
    [self getMessagesForConversationWithReset:YES];
    [self determineUserRoleBaseOnNotification];
}

@end
