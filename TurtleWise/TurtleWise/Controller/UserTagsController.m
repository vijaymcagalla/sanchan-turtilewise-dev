//
//  UserTagsController.m
//  TurtleWise
//
//  Created by Waleed Khan on 1/8/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "UserTagsController.h"

@interface UserTagsController ()

@end

@implementation UserTagsController

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [super loadServices:@[@(ServiceTypeTags)]];
}

#pragma mark - Service Call

- (void)requestSuggestedTagsFor:(NSString *)tag success:(void (^)(NSArray *))success
{
    [[service tags] getSuggestedTagsFor:tag withSuccess:^(NSArray * response)
     {
         if (success)
             success(response);
     }];
}

- (void)requestToSaveTag:(NSString *)tag
{
    [[service tags] saveTag:tag];
}

@end
