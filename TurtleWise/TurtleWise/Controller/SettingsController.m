//
//  SettingsController.m
//  TurtleWise
//
//  Created by Usman Asif on 2/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "SettingsController.h"
#import "AnalyticsHelper.h"
#import "SettingsView.h"
#import "Service.h"
#import "Keys.h"

#define TITLE @"Settings"

@implementation SettingsController

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:TITLE];
    
    [self setupNavigationButtons];
    [super loadServices:@[@(ServiceTypeSettings)]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setAppearenceForHome];
    [[[self navigationController] navigationBar] setTranslucent:NO];
    [self getSettings];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Public/Private Methods

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - Service Call
- (void)resendAccountVerificationEmail {
    [super showLoader];
    [service.settings resendVerificationEmail:^(id response) {
        [super hideLoader];
        [Alert show:@"" andMessage:@"Verification email sent."];
    } andfailure:^(NSError *error) {
        [super hideLoader];
        [Alert show:@"" andMessage:@"Failed to send email."];
    }];
}

- (void)getSettings {
    [super showLoader];
    
    [[service settings] getSettingsWthSuccess:^(id response)
     {
         [super hideLoader];
         [(SettingsView *)self.view updateViewWithSettings:response];
     }
                                   andfailure:^(NSError *error)
     {
         [super serviceCallFailed:error];
     }];
}

- (void)resetOldPassword:(NSString *)oldPassword withPassword:(NSString *)newPassword {
    [super showLoader];
    
    NSDictionary * params = @{
                              KEY_CURRENT_PASSWORD  : oldPassword,
                              KEY_NEW_PASSWORD      : newPassword,
                              KEY_CONFIRM_NEW_PASSWORD  : newPassword
                              };
    
    [[service settings] resetPassword:params withSuccess:^(id response)
     {
         [super hideLoader];
         [self resetPasswordSuccessful:response];
     }
                           andfailure:^(NSError *error)
     {
         [super serviceCallFailed:error];
     }];
}

- (void)updateSettingsWithParams:(NSDictionary *)params {
    [super showLoader];
    
    [[service settings] updateSettings:params withSuccess:^(id response) {
        [super hideLoader];
        [Alert show:@"" andMessage:[response get]];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}

#pragma mark - Service Call Back Methods

- (void)resetPasswordSuccessful:(BaseResponse *)response {
    [Alert show:@"" andMessage:[response get]];
    [(SettingsView *)self.view resetTxtFieldsOnSuccess];
}

@end
