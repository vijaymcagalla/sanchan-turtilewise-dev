//
//  UserBasicInfoController.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/4/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardController.h"

@interface UserBasicInfoController : BaseProfileWizardController

- (UIImage *)getSquareImage:(UIImage *)image;
- (void)updateProfilePhoto:(UIImage *)image;

@end
