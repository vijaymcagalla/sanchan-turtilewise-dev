//
//  SubscribeController.m
//  TurtleWise
//
//  Created by Irfan Gul on 09/07/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SubscribeController.h"
#import "SubscribeView.h"
#import "UserDefaults.h"
#import "StringUtils.h"
#import "IAPManager.h"
#import "Question.h"

//Constants
#define SCREEN_TITLE @"Subscription"

#define TITLE_LOADING_PRODUCTS @"Loading Purchases!"
#define TITLE_BUY_MONTHLY_SUBSCRIPTION @"Buying Monthly Subscription!"
#define TITLE_BUY_PER_QUESTION_SUBSCRIPTION @"Buying This Question!"
#define TITLE_BUY_HALF_YEARLY_SUBSCRIPTION @"Buying Half Yearly Subscription!"
#define TITLE_BUY_ANNUAL_SUBSCRIPTION @"Buying Annual Subscription!"
#define TITLE_VERIFYING @"Verifying!"

#define MONTLY_SUBSCRIPTION @"monthly"
#define PER_QUESTION_SUBSCRIPTION @"singleQuestion"
#define HALF_YEARLY_SUBSCRIPTION @"halfyearly"
#define ANNUAL_SUBSCRIPTION @"yearly"

#define PAYMENT_GATEWAY @"iap"

//Keys
#define KEY_GATEWAY @"gateway"
#define KEY_RECEIPT_DATA @"receiptData"
//@"receiptData"


@interface SubscribeController ()

@property(nonatomic, strong) NSString *purchasingProductName;
@property(nonatomic, strong) Question *question;

- (void)fetchIAPProducts;
- (NSDictionary *)getParamsForVerifyReceipt;
- (void)verifyRecieptSucessfull:(id)response;
- (void)verifyRecipt;

@end

@implementation SubscribeController


#pragma mark - Life Cycle Methods

- (id)initWithQuestion:(Question *)question
{
    if (self = [super init])
    {
        _question = question;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadServices:@[@(ServiceTypePurchase),@(ServiceTypeUser)]];
    
    [self setTitle:SCREEN_TITLE];
    [self setupNavigationBar];
    [self fetchIAPProducts];
}

- (void)setupNavigationBar
{
    NSString *backButtonImage;
    
    if ([UserDefaults getUserRole] == UserRoleAdvisor)
    {
        backButtonImage = BACK_BUTTON_WHITE_IMAGE;
        
        [self setAppearenceForAdvisor];
        
      //  [(SubscribeView *)self.view hideSingleQuestionSubscription];
    }
    else
    {
        backButtonImage = BACK_BUTTON_BLACK_IMAGE;
        [self setAppearenceForSeeker];
    }
     
    [self setupLeftNavigationButtonWithImage:backButtonImage withOnClickAction:^{
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

#pragma mark - IAP Service Methods

- (void)fetchIAPProducts
{
    [self showLoader:TITLE_LOADING_PRODUCTS];
    
    [[IAPManager sharedInstance] requestProductsWithCompletion:^(SKProductsRequest *request, SKProductsResponse *response)
    {
        [self hideLoader];
    }];
}

- (void)buyMonthlySubscription
{
    _purchasingProductName = MONTLY_SUBSCRIPTION;
    
    [self showLoader:TITLE_BUY_MONTHLY_SUBSCRIPTION];
    
    [[IAPManager sharedInstance] buyMonthlySubsciptionWithCompletion:^(SKPaymentTransaction *transcation)
    {
        [self subscriptionTransactionCompleted:transcation];
    }];
}

- (void)buyThisQuestionSubscription
{
    _purchasingProductName = PER_QUESTION_SUBSCRIPTION;
    
    [self showLoader:TITLE_BUY_PER_QUESTION_SUBSCRIPTION];
    
    [[IAPManager sharedInstance] buyOneQuestionSubsctiptionWithCompletion:^(SKPaymentTransaction *transcation)
    {
        [self subscriptionTransactionCompleted:transcation];
    }];
}
-(void)buyHalfYearlySubscription{
    _purchasingProductName = HALF_YEARLY_SUBSCRIPTION;
    
    [self showLoader:TITLE_BUY_HALF_YEARLY_SUBSCRIPTION];
    
    [[IAPManager sharedInstance] buyHalfYearlySubsciptionWithCompletion:^(SKPaymentTransaction *transcation) {
        
        [self subscriptionTransactionCompleted:transcation];
    }];
    
}
-(void)buyAnnualSubscription{
    
    _purchasingProductName = ANNUAL_SUBSCRIPTION;
    
    [self showLoader:TITLE_BUY_ANNUAL_SUBSCRIPTION];
    
    [[IAPManager sharedInstance] buyYearlySubsciptionWithCompletion:^(SKPaymentTransaction *transcation) {
        
        [self subscriptionTransactionCompleted:transcation];
    }];
}

- (void)verifyRecipt
{
    NSData *reciptData = [[IAPManager sharedInstance] getReceiptData];
    
    if (!reciptData)
    {
        return;
    }
    
    [self showLoader:TITLE_VERIFYING];
    
    [[service purchase] verifyReceipt:[self getParamsForVerifyReceipt] withSuccess:^(id response)
    {
        [self verifyRecieptSucessfull:response];
        
    }
      andfailure:^(NSError *error)
    {
        [super serviceCallFailed:error];
    }];
}

#pragma mark - Service Call Back

- (void)subscriptionTransactionCompleted:(SKPaymentTransaction *)transcation
{
    [self hideLoader];
    
    if ([transcation transactionState] == SKPaymentTransactionStateFailed)
    {
        return;
    }
    
    [self verifyRecipt];
}

- (void)verifyRecieptSucessfull:(id)response
{
    [self hideLoader];
    
    [self dismissViewControllerAnimated:YES completion:^{
        if (_delegate && [_delegate respondsToSelector:@selector(didSubscribeSuccessfully)])
        {
            [_delegate didSubscribeSuccessfully];
        }
    }];
}

-(void)applyForCoupon:(NSString *)coupon{
    if([coupon isEqualToString:@""])
    {
        [Alert show:@"Coupon Code" andMessage:@"Please enter a coupon code." andDelegate:nil];
    }
    else
    {
        [service.user getCouponCode:@{@"type":@"coupon",@"couponcode":coupon} onSuccess:^(id response) {
            NSLog(@"Success:%@",response);
            [UserDefaults setCoupon:coupon];
        } andFailure:^(NSError *error) {
            [self serviceCallFailed:error];
        } response:nil];
    }
}

#pragma mark - Helper Methods

- (NSDictionary *)getParamsForVerifyReceipt
{
    return     @{
                    KEY_GATEWAY      : PAYMENT_GATEWAY,
                    KEY_TYPE         : _purchasingProductName,
                    /*KEY_QUESTION_ID  : (_question) ? [_question questionId] : @"",*/
                    KEY_RECEIPT_DATA : [StringUtils base64forData:[[IAPManager sharedInstance] getReceiptData]],
                    @"userId" : @"",
                    @"coupon":@""};
}
/*
- (void)hideSingleSubscriptionOption
{
    [(SubscribeView *)self.view hideSingleQuestionSubscription];
}*/

@end
