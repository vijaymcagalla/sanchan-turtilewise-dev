//
//  FAQController.m
//  TurtleWise
//
//  Created by Anum Amin on 3/21/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "FAQController.h"

#define TITLE @"FAQ"

@interface FAQController ()

@end

@implementation FAQController

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:TITLE];
    [self setupNavigationButtons];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setAppearenceForHome];
    [[[self navigationController] navigationBar] setTranslucent:NO];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Public/Private Methods

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
@end
