//
//  NewUserInfoController.m
//  TurtleWise
//
//  Created by Usman Asif on 1/22/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "NewUserInfoController.h"
#import "NewUserInfoView.h"
#import "MFSideMenu.h"

@class User;
@class UserProfile;

#define TITLE_GURU @"Guru's Info"
#define TITLE_SEEKER @"Explorer's Info"

@interface NewUserInfoController ()
{
    User *user;
    UserProfile *profile;
}
- (void)setupNavigationButtons;

@end

@implementation NewUserInfoController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:self.isSeeker ? TITLE_SEEKER : TITLE_GURU];
    
    [self setupNavigationButtons];
    [super loadServices:@[@(ServiceTypeUser)]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setAppearenceForHome];

    [self getUserProfileWithId:_advisorId];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - Service Call

- (void)getUserProfileWithId:(NSString *)advisorId
{
    [super showLoader];
    
    [[service user] getProfileWthId:advisorId
                            success:^(id response)
    {
        [self getProfileSucuss:response];
    }
                         andfailure:^(NSError *error)
    {
        [self serviceCallFailed:error];
    }];
}


#pragma mark - Service Call Back Methods

- (void)getProfileSucuss:(id)response
{
    [super hideLoader];
    
    [(NewUserInfoView *)self.view setUser:(UserProfile *)response];
}


#pragma mark - Navigation

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

@end
