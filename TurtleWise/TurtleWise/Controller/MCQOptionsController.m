//
//  MCQOptionsController.m
//  TurtleWise
//
//  Created by Waleed Khan on 2/2/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "MCQOptionsController.h"
#import "MCQOptionsView.h"

@interface MCQOptionsController ()

- (void)setupNavigationButtons;

@property(nonatomic, strong) MCQOptionsView *view;
@property(nonatomic, weak) id <MCQOptionsControllerDelegate> delegate;

@end

@implementation MCQOptionsController
@dynamic view;

#pragma mark - Life Cycle Methods

- (id)initWithDelegate:(id)delegate
{
    if (self == [super init])
    {
        _delegate = delegate;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:ASK_SCREEN_TITLE];
    [self setupNavigationButtons];
}

- (void)setupNavigationButtons
{
    [self setupRightNavigationButtonWithName:LEFT_BUTTON_TITLE withOnClickAction:^{
        [[self view] clearOptions];
        [self backToAskQuestionWithChoices:[NSArray new]];
    }];
}

#pragma mark - Navigation

- (void)backToAskQuestionWithChoices:(NSArray *)choices
{
    if (_delegate && [_delegate respondsToSelector:@selector(saveMCQChoices:)])
    {
        [_delegate saveMCQChoices:choices];
    }

    [[self navigationController] popViewControllerAnimated:YES];
}

@end
