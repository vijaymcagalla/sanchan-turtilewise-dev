//
//  SelectSignUpAsViewController.h
//  TurtleWise
//
//  Created by Hardeep Kaur on 03/06/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardController.h"

@class TWWizardManager;

@interface SelectSignUpAsController : BaseProfileWizardController
@end
