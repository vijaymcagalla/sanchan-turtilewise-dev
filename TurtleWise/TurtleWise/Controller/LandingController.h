//
//  LandingController.h
//  TurtleWise
//
//  Created by Anum Amin on 3/14/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"

@interface LandingController : BaseController

-(void) onNavigationToLandingVideoController;

@end
