//
//  AskQuestionController.h
//  TurtleWise
//
//  Created by Irfan Gul on 05/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"

@interface AskQuestionController : BaseController

@property(nonatomic) BOOL clearQuestion;

- (void)startAdvisorSelectionForQuestion:(NSString *)question;
- (id)initWithAnswereDictionary:(NSMutableDictionary*)dic;
@end
