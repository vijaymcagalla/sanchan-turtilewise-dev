//
//  SelectHobbiesController.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/8/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardController.h"

@interface SelectHobbiesController : BaseProfileWizardController

-(void)getHobbiesListwith:(void(^)(id responseObject))completion;

@end
