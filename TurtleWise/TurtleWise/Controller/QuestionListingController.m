//
//  SeekerController.m
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "QuestionListingController.h"
#import "QuestionListingView.h"
#import "AnswerController.h"
#import "UserDefaults.h"
#import "QuestionListingView.h"
#import "Question.h"
#import "Service.h"
#import "Answer.h"
#import "NewGiveAnswerController.h"
#import "SocialShareManager.h"
#import "ChattingController.h"
#import "Conversation.h"
#import "AnalyticsHelper.h"


#define SCREEN_TITLE_MY_QUESTIONS @"My Questions"
#define SCREEN_TITLE_THEIR_QUESTIONS @"Their Questions"

#define SCREEN_TITLE_MY_ADVICE @"My Advice"
#define SCREEN_TITLE_THEIR_ADVICE @"Their Advice"

#define SCREEN_TITLE_MY_CHATS @"My Chats"
#define SCREEN_TITLE_THEIR_CHATS @"Their Chats"

@interface QuestionListingController ()<TabBarBtnTapEvent,NewGiveAnswerControllerProtocol>{
    NSString *_questionIdForChat;
}

@end

@implementation QuestionListingController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadServices:@[@(ServiceTypeAnswer)]];
}

-(void)viewWillAppear:(BOOL)animated
{
    _userRole = [UserDefaults getUserRole];
    
    [super viewWillAppear:animated];
    [self setupNavigationBarAppearence];
    [self setupRightNavigationButton];
}

- (void)setupNavigationBarAppearence
{
    _userRole == UserRoleAdvisor ? [self setAppearenceForAdvisor] : [self setAppearenceForSeeker];
    [self setupLeftNavigationButtonWithImage:(_userRole == UserRoleAdvisor) ? BACK_BUTTON_WHITE_IMAGE : BACK_BUTTON_BLACK_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)setupRightNavigationButton
{
    if (_userRole == UserRoleAdvisor && _listType == TabbarViewTypeQuestion)
    {
        [self setupRightNavigationButtonWithName:@"Edit" withOnClickAction:^{
            [(QuestionListingView*)self.view onEditBtnTap];
        }];
        return;
    }
    [self.navigationItem setRightBarButtonItem:nil];
}

#pragma mark - Notifications

-(id)initWithListType:(TabbarViewType)listType andUserRole:(UserRole)userRole{
    if(self = [super init]){
        _listType = listType;
        _userRole = userRole;
    }
    return self;
}

#pragma Getters
-(TabbarViewType)getListType{
    return _listType;
}
-(UserRole)getUserRole{
    return _userRole;
}

#pragma UpdateListType
-(void)setupWithListType:(TabbarViewType)type
{
    _listType = type;
    [self setupRightNavigationButton];
    
    switch (type)
    {
        case TabbarViewTypeQuestion:
        {
            [self loadQuestionsAndReset:YES];
            [self setTitle:(_userRole == UserRoleAdvisor) ? SCREEN_TITLE_THEIR_QUESTIONS : SCREEN_TITLE_MY_QUESTIONS];
        }
            break;
        case TabbarViewTypeAnswer:
        {
            [self loadAnswersAndReset:YES];
            [self setTitle:(_userRole == UserRoleAdvisor) ? SCREEN_TITLE_MY_ADVICE : SCREEN_TITLE_THEIR_ADVICE];
        }
            break;
            
        case TabbarViewTypeChat:
        {
            [self loadConversationsAndReset:YES];
            self.navigationItem.rightBarButtonItem = nil;
            [self setTitle:(_userRole == UserRoleAdvisor) ? SCREEN_TITLE_THEIR_CHATS : SCREEN_TITLE_MY_CHATS];
        }
            break;
        default:
            break;
    }
}

#pragma mark - Load Conversations
-(void)loadMoreConversations{
    [self loadConversationsAndReset:NO];
}
-(void)loadConversationsAndReset:(BOOL)reset{
    
    if(!_conversations)
        _conversations = [ConversationsPagedResponse new];
    
    if(reset){
        [_conversations reset];
    }
    
    if(!service.chat)
        [super loadServices:@[@(ServiceTypeChat)]];
    
    if(!_conversations.hasMoreData)
        return;
    
    if(_conversations.isLoadingData)
        return;
    
    [(QuestionListingView*)self.view addTableFooterWithActivityIndicator];
    
    
    if(_questionIdForChat)
        [self loadConversationServiceCallForQuestion];
    else
        [self loadConversationServiceCallForAllQuestions];
    
}
#pragma mark - Private
-(void)loadConversationServiceCallForQuestion{
    [service.chat getConversationsForQuestion:_questionIdForChat andParams:[self getParamsForChatServiceCall] onSuccess:^(id response) {
        _questionIdForChat = nil;
        [super hideLoader];
        _conversations.isLoadingData = NO;
        [(QuestionListingView*)self.view populateTableView:_conversations.dataArray];
    } andFailure:^(NSError *error) {
        _questionIdForChat = nil;
        [super serviceCallFailed:error];
        _conversations.isLoadingData = NO;
        [(QuestionListingView*)self.view hideTableFooter];
    } response:_conversations];
}

-(void)loadConversationServiceCallForAllQuestions{
    [service.chat getConversationsForUserType:([UserDefaults getUserRole] == UserRoleSeeker) ? KEY_SEEKER : KEY_ADVISOR andParams:[self getParamsForChatServiceCall]  onSuccess:^(id response) {
        [super hideLoader];
        _conversations.isLoadingData = NO;
        [(QuestionListingView*)self.view populateTableView:_conversations.dataArray];
    } andFailure:^(NSError *error) {
        [super serviceCallFailed:error];
        _conversations.isLoadingData = NO;
        [(QuestionListingView*)self.view hideTableFooter];
    } response:_conversations];
}

-(NSDictionary*)getParamsForChatServiceCall{
    return @{KEY_PAGE_SIZE : [NSNumber numberWithInt:_conversations.pageSize],
             KEY_SKIP      : [NSNumber numberWithInt:_conversations.skip]
             };
}
#pragma mark - Load Questions
-(void)loadMoreQuestions{
    [self loadQuestionsAndReset:NO];
}
-(void)loadAnswersAndReset:(BOOL)reset{
    [self loadQuestionsAndReset:reset];
}

-(void)loadQuestionsAndReset:(BOOL)reset{
    
    if(!_questions)
        _questions = [QuestionsResponsePaged new];
    
    if(reset){
        [_questions reset];
    }
    
    if(!service.question)
        [super loadServices:@[@(ServiceTypeQuestion)]];
    
    if(!_questions.hasMoreData)
        return;
    
    if(_questions.isLoadingData)
        return;
    
    [(QuestionListingView*)self.view addTableFooterWithActivityIndicator];
    if(_isArchived)
  {
      [service.question getArchiveQuestions:[self getParamsForArchiveQuestionServiceCall] onSuccess:^(id response) {
          [super hideLoader];
          _questions.isLoadingData = NO;
          [(QuestionListingView*)self.view populateTableView:_questions.dataArray];
          
          if ([_questions.dataArray count] == 0) {
              self.navigationItem.rightBarButtonItem = nil;
          }
      } andFailure:^(NSError *error) {
          [super serviceCallFailed:error];
          _questions.isLoadingData = NO;
          [(QuestionListingView*)self.view hideTableFooter];
      } response:_questions];
  }
  else{
    [service.question getQuestions:[self getParamsForQuestionsServiceCall] onSuccess:^(id response) {
        [super hideLoader];
        _questions.isLoadingData = NO;
        [(QuestionListingView*)self.view populateTableView:_questions.dataArray];
        
        if ([_questions.dataArray count] == 0) {
            self.navigationItem.rightBarButtonItem = nil;
        }
    } andFailure:^(NSError *error) {
        [super serviceCallFailed:error];
        _questions.isLoadingData = NO;
        [(QuestionListingView*)self.view hideTableFooter];
    } response:_questions];
  }
    
}

-(NSDictionary*)getParamsForQuestionsServiceCall{
    return @{KEY_USER_ROLE : [UserDefaults getUserRole] == UserRoleSeeker ? KEY_SEEKER : KEY_ADVISOR,
             KEY_PAGE_SIZE : [NSNumber numberWithInt:_questions.pageSize],
             KEY_SKIP      : [NSNumber numberWithInt:_questions.skip],
             KEY_ANSWERED  : (_listType == TabbarViewTypeAnswer) ? @"true" : @"false"
             };
}
-(NSDictionary*)getParamsForArchiveQuestionServiceCall{
    
    return @{KEY_PAGE_SIZE : [NSNumber numberWithInt:_questions.pageSize],
             KEY_SKIP : [NSNumber numberWithInt:_questions.skip],
             KEY_ANSWERED  : (_listType == TabbarViewTypeAnswer) ? @"true" : @"false"};
}


#pragma mark - Close Question
- (void)closeQuestion:(NSString *)questionId{
    [super showLoader];

    [service.question closeQuestion:@{KEY_QUESTION_ID:questionId} onSuccess:^(id response){
        [super hideLoader];
        [self loadQuestionsAndReset:YES];
        [(QuestionListingView *)self.view closeQuestionSuccess];
        [self askToShareContent];
    } andfailure:^(NSError *error){
        [super serviceCallFailed:error];
    }];
}

- (void)refreshOnCloseQuestionSuccess{
    [self loadQuestionsAndReset:YES];
    [self askToShareContent];
}
#pragma mark - Delete Questions
- (void)deleteQuestions:(NSArray*)questions{
    [super showLoader];
    
    [service.question deleteQuestions:questions onSuccess:^(id response){
        [super hideLoader];
        [self loadQuestionsAndReset:YES];
        [(QuestionListingView *)self.view deleteQuestionsSuccess];
    } andfailure:^(NSError *error){
        [super hideLoader];
        [super serviceCallFailed:error];
    }];
}

#pragma mark - Navigation
-(void)openAnswerControllerWithQuestion:(Question*)question{
    AnswerController *controller = [AnswerController new];
    controller.customDelegate = self;
    controller.question = question;
    controller.isArchived = _isArchived;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)openGiveAnswerControllerWithQuestion:(Question *)question isReadOnly:(BOOL)readOnly {
    NewGiveAnswerController *controller = [NewGiveAnswerController new];
    [controller setCustomDelegate:self];
    [controller setQuestion:question forReadOnly:readOnly];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)didSelectAnswerControllerTabBarBtn:(TabbarViewType)tabViewType{
    [(QuestionListingView*)self.view didSelectTabBarBtn:tabViewType];
}
-(void)loadConversationsForQuestion:(NSString*)questionId{
    _questionIdForChat = questionId;
    _listType = TabbarViewTypeChat;
    [self.navigationController popToViewController:self animated:NO];
}

-(void)openChattingControllerForConversation:(Conversation *)conversation{
    ChattingController *chattingController = [[ChattingController alloc] initWithConversation:conversation];
    [self.navigationController pushViewController:chattingController animated:YES];
}

#pragma mark - NewGiveAnswerController Delegate
-(void)didSuccessfullyPostAnswer{
    //[self loadQuestionsAndReset:YES];
}
-(void)didSelectGiveAnswerControllerTabBarBtn:(TabbarViewType)tabViewType{
    [(QuestionListingView*)self.view didSelectTabBarBtn:tabViewType];
}

#pragma mark - Social Sharing
-(void)askToShareContent{
    UIAlertController * controller = [UIAlertController alertControllerWithTitle:NULL
                                                                         message:@"Would you like to share TurtleWise with your social connections?"
                                                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * ok = [UIAlertAction actionWithTitle:@"YES"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * _Nonnull action) {
                                                    [self showSocialShareActionSheet];
                                                }];
    [controller addAction:ok];
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"NO"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                    }];
    [controller addAction:cancel];
    
    
    
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)showSocialShareActionSheet{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Share"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Share on Facebook"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action){
                                                [SocialShareManager shareOnFB];
                                            }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Share on Linkedin"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action){
                                                [SocialShareManager shareOnLinkedin:self];
                                            }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Share on Google+"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action){
                                                [SocialShareManager shareOnGooglePlus:self];
                                            }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Share on Twitter"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action){
                                                [SocialShareManager shareOnTwitter:self];
                                            }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                            handler:^(UIAlertAction * action) {}]];
    
    [self presentViewController:alert animated:YES completion:nil];
}


@end
