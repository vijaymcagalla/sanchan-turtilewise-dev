//
//  SubscribeView.h
//  TW-QA
//
//  Created by Vijay on 12/01/18.
//  Copyright © 2018 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ActionDelegate

-(void)closeWindow;
-(void)applyForCoupon:(NSString*)coupon;
-(void)paymentForSubscribe;

@end

@interface SubscribePopView : UIView<UITextFieldDelegate>

@property(nonatomic,weak) IBOutlet UITextField *couponCodeField;

@property(nonatomic,weak) IBOutlet UIButton *annualBtn;
@property(nonatomic,weak) IBOutlet UIButton *halfYearlyBtn;
@property(nonatomic,weak) IBOutlet UIButton *monthlyBtn;

@property(nonatomic,assign) id<ActionDelegate>delegate;

@end
