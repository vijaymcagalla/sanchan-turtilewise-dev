//
//  RegistrationController.m
//  iOSTemplate
//
//  Created by mohsin on 11/5/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import "RegistrationController.h"
#import "AboutMeController.h"
#import "TSMiniWebBrowser.h"
#import "RegistrationView.h"
#import "AnalyticsHelper.h"
#import "SocketIOManager.h"
#import "UserDefaults.h"
#import "Service.h"


@interface RegistrationController ()

- (void)openAboutMeController;
- (void)signUpSucessful:(id)response;

@end

@implementation RegistrationController

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super loadServices:@[@(ServiceTypeAuth)]];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Navigation

- (void)openAboutMeController
{
    AboutMeController *aboutController = [AboutMeController new];
    aboutController.fromSignup = YES;
    [self.navigationController pushViewController:aboutController animated:YES];
}

#pragma mark - Service Calls

- (void)signUpWithEmail:(NSString *)email andPassword:(NSString *)password confirmPassword:(NSString *)confirmPassword
{
    [self showLoader];
    
    [[service auth] signUpWithEmail:email andPassword:password confirmPassword:confirmPassword withSuccess:^(id response)
    {
        [self signUpSucessful:response];
    }
       andfailure:^(NSError *error)
    {
        [self serviceCallFailed:error];
    }];
}

#pragma mark - Serivce Call Backs

- (void)signUpSucessful:(id)response
{
    [self hideLoader];
    [UserDefaults setUserRole:UserRoleAdvisor];
    [UserDefaults setLoginFlowType:LoginFlowTypeEmail];
    [UserDefaults setUserRole:UserRoleAdvisor];

    [AnalyticsHelper logEvent:self
                     category:CATEGORY_AUTHORIZATION
                       action:EVENT_SIGNUP
                        label:LABEL_EMAIL
                        value:NULL];

    [[SocketIOManager instance] connectScocket];
    [self openAboutMeController];
}

#pragma mark - Navigation

- (void)openInAppLink:(NSString *)link andTitle:(NSString *)title
{
    TSMiniWebBrowser *webBrowser = [[TSMiniWebBrowser alloc] initWithUrl:[NSURL URLWithString:link]];
    [webBrowser setBarTintColor:[UIColor whiteColor]];
    [webBrowser setHidesShareBtn:YES];
    [webBrowser setShowActionButton:NO];
    [webBrowser setFixedTitleBarText:title];
    [webBrowser setAdjustTitleToFitWidth:YES];
    webBrowser.mode = TSMiniWebBrowserModeModal;
    
    [self presentViewController:webBrowser animated:YES completion:nil];
}

@end
