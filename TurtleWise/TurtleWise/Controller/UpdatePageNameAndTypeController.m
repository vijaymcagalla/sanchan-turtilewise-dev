//
//  UpdatePageNameAndTypeController.m
//  TurtleWise
//
//  Created by Sunflower on 8/25/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "UpdatePageNameAndTypeController.h"
#import "SelectSummaryView.h"
#import "BrandedPage.h"
#import "UpdatePageNameAndTypeView.h"
#import "UserDefaults.h"

#define TITLE @"Edit Name & Type"

@interface UpdatePageNameAndTypeController ()

@end

@implementation UpdatePageNameAndTypeController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super loadServices:@[@(ServiceTypeBrandedPage)]];
    
    [self setTitle];
    [self setupNavigationButtons];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setAppearenceForHome];
    
    [[self navigationController] setNavigationBarHidden:NO];
    pageManager = [BrandedPageManager sharedManager];
    [(UpdatePageNameAndTypeView *)self.view updateData:[pageManager brandedPage]];
    
}


- (void)setTitle
{
    [self setTitle:[NSString stringWithFormat:TITLE]];
}

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)savePageDetail
{
    
    [(UpdatePageNameAndTypeView *)self.view saveWizardData:[pageManager brandedPage]];
    
    NSDictionary * params = [pageManager getDictionaryFromBrandedPage:[pageManager brandedPage]];
    [self showLoader];
    [[service brandedPageService] updatePage:params withPageId:[UserDefaults getPageID] withSuccess:^(id response) {
        [super hideLoader];
        [self popViewController];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}


@end
