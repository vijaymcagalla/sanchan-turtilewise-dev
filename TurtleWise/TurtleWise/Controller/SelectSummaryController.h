//
//  SelectSummaryController.h
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"
#import "BrandedPageManager.h"

@interface SelectSummaryController : BaseController {
    BrandedPageManager *pageManager;
}
- (void)savePageDetail;
@end
