//
//  BoardOfAdvisorsViewController.m
//  TurtleWise
//
//  Created by Vijay Bhaskar on 12/11/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BoardOfAdvisorsController.h"
#import "BoardOfAdvisorsView.h"

@interface BoardOfAdvisorsController ()

@end

@implementation BoardOfAdvisorsController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:@"Board of Advisors"];
    
    [self setupNavigationButtons];
     [super loadServices:@[@(ServiceTypeQuestion)]];
    
    [(BoardOfAdvisorsView*)self.view getAdvisorsList];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setAppearenceForHome];
    [[[self navigationController] navigationBar] setTranslucent:NO];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Public/Private Methods

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}


-(void)getBoardofAdvisorsWith:(NSString *)status
{
    [self showLoader];
    [[service question]getboardOfAdvisors:nil onSuccess:^(id response) {
        
        NSDictionary *dict = response[@"data"];
        [(BoardOfAdvisorsView*)self.view setListofAdvisors:dict[@"boardOfAdvisors"]];
        [[(BoardOfAdvisorsView*)self.view listofAdvisorsView] reloadData];
        [self hideLoader];
    } andFailure:^(NSError *error) {
        NSLog(@"%@",error.localizedDescription);
        [self hideLoader];
    } response:nil];
}
@end
