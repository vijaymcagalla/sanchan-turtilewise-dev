//
//  ForgotPasswordController.m
//  TurtleWise
//
//  Created by Usman Asif on 11/8/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "ForgotPasswordController.h"
#import "ForgotPasswordView.h"

@implementation ForgotPasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super loadServices:@[@(ServiceTypeUser)]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [(ForgotPasswordView *)self.view initWithEmail:_email];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Service Calls

- (void)forgotPasswordForEmail:(NSString *)email
{
    [super showLoader];
    
    [[service user] forgotPassword:email success:^(id response) {
        [super hideLoader];
        [Alert show:EMPTY_STRING andMessage:@"Thank you. You will receive password reset email shortly."];
        [self.navigationController popViewControllerAnimated:YES];
    } andfailure:^(NSError *error) {
        [super hideLoader];
        [Alert show:error];
    }];
}

@end
