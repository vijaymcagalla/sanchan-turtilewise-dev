//
//  SelectHobbiesController.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/8/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "SelectHobbiesController.h"
#import "UserService.h"
#import "SelectHobbiesView.h"
#import "TWTableView.h"

@interface SelectHobbiesController ()

@end

@implementation SelectHobbiesController

#pragma mark - Life cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadServices:@[@(ServiceTypeUser)]];

}

-(void)getHobbiesListwith:(void(^)(id responseObject))completion{
    
    [self showLoader];
    
    [[service user]getHobbiesListSuccess:^(id response) {
        completion(response);
        [self hideLoader];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    } response:nil];
}
- (void)selectedItem:(NSArray *)selectedList{
    NSLog(@"%@",selectedList);
        [[(SelectHobbiesView*)self.view tableView] prepareDataSet:selectedList];
}


@end
