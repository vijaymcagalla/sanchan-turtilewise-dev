//
//  AddArticleController.h
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardController.h"
#import "BrandedPageManager.h"

@class TWWizardManager;
@class Article;
@interface AddArticlesController : BaseController {
    BrandedPageManager *pageManager;
}
@property(nonatomic) int articleNo;
@property(nonatomic, strong) TWWizardManager *profileWizardManager;
- (void)savePageDetail;
@end
