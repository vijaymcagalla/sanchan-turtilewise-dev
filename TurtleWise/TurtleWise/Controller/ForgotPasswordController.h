//
//  ForgotPasswordController.h
//  TurtleWise
//
//  Created by Usman Asif on 11/8/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"

@interface ForgotPasswordController : BaseController

@property (nonatomic) NSString * email;

- (void)forgotPasswordForEmail:(NSString *)email;
@end
