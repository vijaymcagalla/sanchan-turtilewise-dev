//
//  AboutMeController.h
//  TurtleWise
//
//  Created by Irfan Gul on 26/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"

@class TWWizardManager;

@interface AboutMeController : BaseController

@property(nonatomic, strong) TWWizardManager *profileWizardManager;
@property(nonatomic) BOOL shouldDisplayName;
@property(assign) BOOL fromSignup;

- (id)initWithProfileWizardManager:(TWWizardManager *)profileWizardManager;
- (void)showEditProfileController;
- (void)startProfileWizard;
- (void)setFlowType:(AboutMeViewFlowType)flowType;

@end
