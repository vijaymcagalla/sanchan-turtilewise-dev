//
//  AboutMeController.m
//  TurtleWise
//
//  Created by Irfan Gul on 26/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "TWWizardManager.h"
#import "AboutMeController.h"
#import "AboutMeView.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "UserProfile.h"
#import "UserDefaults.h"

@interface AboutMeController ()<UIAlertViewDelegate>
{
    BOOL shownSignupAlert;
}
@property(nonatomic, assign) AboutMeViewFlowType flowType;

@end

@implementation AboutMeController

#pragma mark - Life Cycle Methods

- (id)initWithProfileWizardManager:(TWWizardManager *)profileWizardManager
{
    if (self = [super init])
    {
        _profileWizardManager = profileWizardManager;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [super loadServices:@[@(ServiceTypeUser)]];
    
    if (!_profileWizardManager)
    {
        _profileWizardManager = [[TWWizardManager alloc] initWithPresentingController:self];
        [_profileWizardManager setProfile:[UserProfile new]];
        [_profileWizardManager setShouldSaveWizardOnDone:YES];
    }
    
    if (_shouldDisplayName)
    {
        [self getUserProfile];
    }
    
    if(_fromSignup)
        [Alert show:TITLE_ALERT_ACCOUNT_ACTIVATION andMessage:MESSAGE_ACCOUNT_ACTIVATION andDelegate:self];
    else
        shownSignupAlert = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)setFlowType:(AboutMeViewFlowType)flowType
{
    _flowType = flowType;
    [_profileWizardManager setShouldSaveWizardOnDone:NO];
    [(AboutMeView *)self.view adjustViewForFlow:flowType];
}

#pragma mark - Navigaion

- (void)startProfileWizard
{
    [_profileWizardManager startProfileWizard];
}

- (void)showEditProfileController
{
    [self popViewController];
}

- (void)getUserProfile
{
    [[service user] getProfileWthSuccess:^(id response)
     {
         [self getProfileSuccessful:response];
     } andfailure:nil];
}

#pragma mark - Service Call backs

- (void)getProfileSuccessful:(UserProfile *)response
{
    [self hideLoader];
    [UserDefaults setUserProfile:response];
    
    [_profileWizardManager setProfile:response];
    
    if([UserDefaults showAlertForIncompleteProfile:response] && shownSignupAlert) {
        [Alert show:@"" andMessage:ALERT_INCOMPLETE_PROFILE];
    }
}

#pragma mark - Alertview Delegate

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    shownSignupAlert = YES;
    
    if(_flowType != AboutMeViewFlowTypeEditProfile)
        [Alert show:@"" andMessage:ALERT_INCOMPLETE_PROFILE];
}

@end
