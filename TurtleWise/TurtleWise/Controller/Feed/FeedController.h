//
//  FeedViewController.h
//  TurtleWise
//
//  Created by Vijay Bhaskar on 15/03/18.
//  Copyright © 2018 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"

@interface FeedController : BaseController

-(void)getTurtleWisePages;

@end
