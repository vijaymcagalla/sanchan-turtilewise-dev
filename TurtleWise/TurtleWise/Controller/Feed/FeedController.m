//
//  FeedViewController.m
//  TurtleWise
//
//  Created by Vijay Bhaskar on 15/03/18.
//  Copyright © 2018 Waaleed Khan. All rights reserved.
//

#import "FeedController.h"
#import "FeedView.h"
#import "EnvironmentConstants.h"

@interface FeedController ()

@end

@implementation FeedController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self showLoader];
    self.title = @"Feed";
    
    [self setupNavigationButtons];
    [super loadServices:@[@(ServiceTypeTags)]];
    
    [(FeedView*)self.view getFeedList];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setAppearenceForHome];
    [[[self navigationController] navigationBar] setTranslucent:NO];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Public/Private Methods

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
-(void)getTurtleWisePages{
   
    [[service tags] getPages:nil onSuccess:^(id response) {
   
//        if ([SERVICE_TURTLE_WISE_PAGES  isEqual: @"users/contentfeed"])
//        {
//            [(FeedView*)self.view setFeedList:response[@"data"]];
//            [[(FeedView*)self.view feedsListView] reloadData];
//        }
//        else
//        {
//            [(FeedView*)self.view setFeedList:response[@"data"][0][@"articles"]];
//            [[(FeedView*)self.view feedsListView] reloadData];
//        }
        [(FeedView*)self.view setFeedList:response[@"data"]];
        [[(FeedView*)self.view feedsListView] reloadData];
    
        [self hideLoader];
    } andFailure:^(NSError *error) {
        NSLog(@"MUS%@",error.localizedDescription);
        [self hideLoader];
    } response:nil];
}



@end
