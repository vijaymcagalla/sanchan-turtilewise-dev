//
//  ChatOptionsController.m
//  TurtleWise
//
//  Created by Ajdal on 4/27/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "ChatOptionsController.h"
#import "Conversation.h"
#import "ChatOptionsView.h"
#import "UserDefaults.h"
#import "RatingAdvisorsController.h"
#import "NewUserInfoController.h"

@interface ChatOptionsController (){
    Conversation *_conversation;
}

@end

@implementation ChatOptionsController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super loadServices:@[@(ServiceTypeChat)]];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self customizeNavigationBarForUserRole:[UserDefaults getUserRole]];
}

- (void)customizeNavigationBarForUserRole:(UserRole)userRole{
    NSString *backButtonImage;
    
    if (userRole == UserRoleAdvisor){
        backButtonImage = BACK_BUTTON_WHITE_IMAGE;
        [self setAppearenceForAdvisor];
    }
    else{
        backButtonImage = BACK_BUTTON_BLACK_IMAGE;
        [self setAppearenceForSeeker];
    }

    [self.navigationItem setTitle:TITLE_CHAT_OPTIONS];
    
    [self setupLeftNavigationButtonWithImage:backButtonImage withOnClickAction:^{
        [self popViewController];
    }];
}
-(void)setConversation:(Conversation*)conversation{
    _conversation = conversation;
    [(ChatOptionsView*)self.view setConversation:conversation];
}
//- (void)getConversationDetails{//Only to get the current server time
//    [service.chat getConversationDetails:_conversation.conversationId onSuccess:^(id response){
//        _conversation = response;
//        [(ChatOptionsView*)self.view setConversation:_conversation];
//         
//     }andFailure:^(NSError *error){
//         
//     }];
//}
-(void)endChat{
    [self showLoader];
    
    [service.chat endChatWithId:_conversation.conversationId withSuccess:^(id response){
        [super hideLoader];
        [self showChatEndedAlert];
        _conversation.status = ConversationStatusClosed;
     } andFailure:^(NSError *error){
         [self serviceCallFailed:error];
     }];
}

-(void)showChatEndedAlert{
    UIAlertController * controller = [UIAlertController alertControllerWithTitle:@"Chat Ended!"
                                                                         message:@"Your chat session has come to an end."
                                                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * _Nonnull action) {
                                                    [self showRatingAdvisorsController];
                                                }];
    [controller addAction:ok];

    
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)addAdvisorWith:(NSString*)advisorID{
    [[service chat]addAdvisor:nil withId:advisorID withSuccess:^(id response) {
        NSLog(@"%@",response);
        
    } andFailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}

-(void)showRatingAdvisorsController{
    RatingAdvisorsController *ratingAdvisorsController = [[RatingAdvisorsController alloc] initWithConversation:_conversation];
    [[self navigationController] pushViewController:ratingAdvisorsController animated:YES];
}
-(void)showUserProfileController:(NSString*)advisorId{
    NewUserInfoController *controller = [NewUserInfoController new];
    controller.advisorId = advisorId;
    [self.navigationController pushViewController:controller animated:YES];
}
@end
