//
//  AddEventsController.m
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "AddEventsController.h"
#import "AddEventsView.h"
#import "TWWizardManager.h"
#import "SeekingAdvisor.h"
#import "UserProfile.h"
#import "BrandedPage.h"
#import "UserDefaults.h"

#define TITLE @"Events"
#define TITLE_REVIEW_PROFILE @"Review Profile"

@interface AddEventsController ()

- (void)setTitle;
- (void)setupNavigationButtons;

@end

@implementation AddEventsController

#pragma mark - UIViewController Life Cycle

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [super loadServices:@[@(ServiceTypeBrandedPage)]];
    // Do any additional setup after loading the view.
    
    [self setTitle];
    [self setupNavigationButtons];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[self navigationController] setNavigationBarHidden:NO];
    pageManager = [BrandedPageManager sharedManager];
    [(AddEventsView *)self.view updateData:[pageManager brandedPage] withArticleNo:_eventNo];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[self view] endEditing:YES];
}


- (void)setTitle
{
    
    [self setTitle:[NSString stringWithFormat:TITLE]];
}

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_WHITE_IMAGE withOnClickAction:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)savePageDetail
{
    [(AddEventsView *)self.view saveWizardData:[pageManager brandedPage]];
    
    NSDictionary * params = [pageManager getDictionaryFromBrandedPage:[pageManager brandedPage]];
    [self showLoader];
    [[service brandedPageService] updatePage:params withPageId:[UserDefaults getPageID] withSuccess:^(id response) {
        [super hideLoader];
        [self popViewController];
    } andfailure:^(NSError *error) {
        [self serviceCallFailed:error];
    }];
}


@end
