//
//  SubscribeView.m
//  TW-QA
//
//  Created by Vijay on 12/01/18.
//  Copyright © 2018 Waaleed Khan. All rights reserved.
//

#import "SubscribePopView.h"
#import "Constant.h"

#define kRadioActive @"tw-radio-active"
#define kRadioInActive @"tw-radio-inactive"


@implementation SubscribePopView

- (IBAction)action:(UIButton *)sender {
    
    switch (sender.tag) {
        case 10:
        case 16:
                [_delegate closeWindow];
            break;
        case 11:
                [_delegate applyForCoupon:_couponCodeField.text];
            break;
        case 13:
            [_annualBtn setImage:[UIImage imageNamed:kRadioActive] forState:UIControlStateNormal];
            [_halfYearlyBtn setImage:[UIImage imageNamed:kRadioInActive] forState:UIControlStateNormal];
            [_monthlyBtn setImage:[UIImage imageNamed:kRadioInActive] forState:UIControlStateNormal];
            [[NSUserDefaults standardUserDefaults] setObject:kYearly forKey:kSubscriptionType];
            break;
        case 14:
            [_halfYearlyBtn setImage:[UIImage imageNamed:kRadioActive] forState:UIControlStateNormal];
            [_monthlyBtn setImage:[UIImage imageNamed:kRadioInActive] forState:UIControlStateNormal];
            [_annualBtn setImage:[UIImage imageNamed:kRadioInActive] forState:UIControlStateNormal];
            [[NSUserDefaults standardUserDefaults] setObject:kHalfYearly forKey:kSubscriptionType];
            break;
        case 15:
            [_monthlyBtn setImage:[UIImage imageNamed:kRadioActive] forState:UIControlStateNormal];
            [_annualBtn setImage:[UIImage imageNamed:kRadioInActive] forState:UIControlStateNormal];
            [_halfYearlyBtn setImage:[UIImage imageNamed:kRadioInActive] forState:UIControlStateNormal];
            [[NSUserDefaults standardUserDefaults] setObject:kMonthly forKey:kSubscriptionType];
            break;
        case 12:
                [_delegate paymentForSubscribe];
            break;
            
        default:
            break;
    }
}

@end
