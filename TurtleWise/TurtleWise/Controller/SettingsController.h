//
//  SettingsController.h
//  TurtleWise
//
//  Created by Usman Asif on 2/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseController.h"

@interface SettingsController : BaseController

- (void)resetOldPassword:(NSString *)oldPassword withPassword:(NSString *)newPassword;
- (void)updateSettingsWithParams:(NSDictionary *)params;
- (void)resendAccountVerificationEmail;
@end
