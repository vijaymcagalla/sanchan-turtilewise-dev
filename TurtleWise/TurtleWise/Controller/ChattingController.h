//
//  ChattingController.h
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"

@class Conversation;
@class ChatMessage;

@interface ChattingController : BaseController

@property(nonatomic) BOOL popTwiceOnBackBtnTap;
@property(nonatomic, strong) NSString * message;

- (id)initWithConversation:(Conversation *)conversation;
- (id)initWithConversationId:(NSString *)conversationId;
- (id)initWithConversation:(Conversation *)conversation withMessage:(NSString *)message;

- (void)loadMoreMessages;
- (void)sendMessage:(NSString *)message withTemporaryId:(NSString *)temporaryId;
- (void)recievedNewMessage:(ChatMessage *)newChatMessage;
- (void)proceedToRateAdvisors;

@end
