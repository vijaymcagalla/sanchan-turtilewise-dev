//
//  ChatRequestController.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/22/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "ChatRequestController.h"
#import "ChattingController.h"
#import "AnalyticsHelper.h"
#import "ChatRequestView.h"
#import "Conversation.h"
#import "Question.h"
#import "Advisor.h"

#define TITLE @"Chat Request"

@interface ChatRequestController ()

@property(nonatomic, strong) NSArray *advisors;
@property(nonatomic, strong) Question *question;

- (void)setupNavigationButtons;
- (void)showChattingScreenWithConversation:(Conversation *)conversation;

@end

@implementation ChatRequestController

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:TITLE];
    [self setupNavigationButtons];
}

- (void)setupNavigationButtons
{
    [self setupLeftNavigationButtonWithImage:BACK_BUTTON_BLACK_IMAGE withOnClickAction:^{
        [[self navigationController] popViewControllerAnimated:YES];
    }];
    
    [self setupRightNavigationButtonWithName:SUBMIT_BUTTON_TITLE withOnClickAction:^{
        [self beginChat];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setAppearenceForSeeker];
}

#pragma mark - Public Private Methods

- (void)setAdvisors:(NSArray *)advisors forQuestion:(Question *)question
{
    _advisors = advisors;
    _question = question;
    
    [(ChatRequestView*)self.view populateAdvisorsList:advisors];
}

- (NSArray *)getAdvisorIds
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for(Advisor *advisor in _advisors)
    {
        [array addObject:[advisor advisorID]];
    }
    
    return array;
}

#pragma mark - Navigation

- (void)showChattingScreenWithConversation:(Conversation *)conversation
{
    ChattingController *chattingController = [[ChattingController alloc] initWithConversation:conversation];
//    chattingController.popTwiceOnBackBtnTap = YES;
    [[self navigationController] pushViewController:chattingController animated:YES];
}

#pragma mark - Service Calls

- (void)beginChat
{
    if([self getAdvisorIds].count > MAXIMUM_CHAT_USERS)
    {
        [Alert show:TITLE_ALERT_ERROR andMessage:MESSAGE_MAXIMUM_CHAT_USERS];
        
        return;
    }
    
    [super loadServices:@[@(ServiceTypeChat)]];
    
    [self showLoader];
    
    [service.chat initiateChatWithAdvisors:[self getAdvisorIds] forQuestion:_question.questionId onSuccess:^(Conversation *conversation)
    {
        [self getChatDetails:conversation];
    }
     andFailure:^(NSError *error)
    {
        [self serviceCallFailed:error];
    }];
}

-(void)getChatDetails:(Conversation*)conversation
{
    [service.chat getConversationDetails:conversation.conversationId onSuccess:^(id response)
    {
        [self hideLoader];
        [self showChattingScreenWithConversation:response];
        if(self.delegate && [self.delegate respondsToSelector:@selector(didStartNewChat)]){
            [self.delegate didStartNewChat];
        }
        
        [AnalyticsHelper logEvent:self category:CATEGORY_CHAT action:EVENT_CHAT_INITIATED];
        
    } andFailure:^(NSError *error)
    {
        [self serviceCallFailed:error];
    }];
}

@end
