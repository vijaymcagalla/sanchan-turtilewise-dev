//
//  RegistrationController.h
//  iOSTemplate
//
//  Created by mohsin on 11/5/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import "BaseController.h"

@interface RegistrationController : BaseController

- (void)openInAppLink:(NSString *)link andTitle:(NSString *)title;
- (void)signUpWithEmail:(NSString *)email andPassword:(NSString *)password confirmPassword:(NSString *)confirmPassword;

@end
