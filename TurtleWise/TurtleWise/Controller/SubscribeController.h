//
//  SubscribeController.h
//  TurtleWise
//
//  Created by Irfan Gul on 09/07/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"

@protocol SubscribeControllerDelegate;

@interface SubscribeController : BaseController

@property (nonatomic, assign) id<SubscribeControllerDelegate> delegate;

- (id)initWithQuestion:(Question *)question;

- (void)buyMonthlySubscription;
- (void)buyThisQuestionSubscription;
-(void)buyHalfYearlySubscription;
-(void)buyAnnualSubscription;
//- (void)hideSingleSubscriptionOption;
-(void)applyForCoupon:(NSString *)coupon;

@end

@protocol SubscribeControllerDelegate <NSObject>

- (void)didSubscribeSuccessfully;

@end

