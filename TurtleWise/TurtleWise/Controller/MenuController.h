//
//  MenuController.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/12/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseController.h"

@interface MenuController : BaseController

- (void)handleViewStatusBar:(UIViewController *)viewController;
- (void)pushController:(UIViewController *)viewController;
- (void)updateMenu;
- (void)userLogOut;
- (void)openRedeemInSafari;
- (void)updateBrandedpage;

@end
