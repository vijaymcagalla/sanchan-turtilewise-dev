//
//  UserLocationController.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/7/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardController.h"

@interface UserLocationController : BaseProfileWizardController

- (NSArray *)getAllCountries;
- (NSArray *)getAllStates;
- (BOOL)shouldEnableStatesForCountry:(NSString *)countryName;
- (NSInteger)getIndexForCountry:(NSString *)countryName;
- (NSInteger)getIndexForState:(NSString *)stateName;
@end
