//
//  UserService.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/8/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "ConfigResponse.h"
#import "StringResponse.h"
#import "UserDefaults.h"
#import "UserService.h"
#import "UserProfile.h"
#import "UserLevel.h"

@implementation UserService

#define SERVICE_GET_PROFILE @"users/%@/profile"
#define SERVICE_PUT_PROFILE @"users/profile"
#define SERVICE_GET_LEVEL_TABLE @"config"
#define SERVICE_PUT_AVATAR @"users/avatar"
#define SERVICE_POST_FORGOT_PASSWORD @"forgotPassword"
#define SERVICE_POST_BRANDED_PAGE @"pages"
#define SERVICE_POST_PAGE_AVATAR @"pages/avatar"
#define SERVICE_POST_PAGE_COVER @"pages/cover"
#define BRAINTREE_CLIENT_TOKEN_API @"payment/token"
#define BRAINTREE_CLIENT_PAYMENT_API @"payment"
#define PAYMENT_COUPON_CODE_API @"paymentcoupon"
#define SERVICE_POST_HOBBIES_LIST @"searchhelps/habbit"
#define SERVICE_POST_UNIVERSITIES_LIST @"searchhelps/university"
#define SERVICE_POST_INTERESTS_LIST @"searchhelps/interest"



#pragma mark - Service Calls

- (void)saveProfile:(NSDictionary *)profile withSuccess:(successCallback)success andfailure:(failureCallback)failure
{
    [http put:SERVICE_PUT_PROFILE parameters:profile success:success failure:failure response:[[StringResponse alloc] init:KEY_ERROR_MESSAGE]];
}

- (void)savePage:(NSDictionary *)profile withSuccess:(successCallback)success andfailure:(failureCallback)failure
{
    [http post:SERVICE_POST_BRANDED_PAGE parameters:profile success:success failure:failure response:[[StringResponse alloc] init:KEY_ERROR_MESSAGE]];
}

- (void)getProfileWthSuccess:(successCallback)success andfailure:(failureCallback)failure
{
    [self getProfileWthId:[UserDefaults getUserID] success:success andfailure:failure];
}

- (void)getProfileWthId:(NSString *)userId success:(successCallback)success andfailure:(failureCallback)failure
{
    NSString *serviceCall = [NSString stringWithFormat:SERVICE_GET_PROFILE, userId];
    
    [http get:serviceCall success:success failure:failure entity:[UserProfile new]];
}

- (void)getLevelTableOnSuccess:(successCallback)success andfailure:(failureCallback)failure
{
    [http get:SERVICE_GET_LEVEL_TABLE success:success failure:failure response:[ConfigResponse new]];
}

- (void)saveAvatar:(NSString *)avatarString success:(successCallback)success andfailure:(failureCallback)failure {
    NSDictionary * params = @{KEY_AVATAR : avatarString};
    [http put:SERVICE_PUT_AVATAR parameters:params success:success failure:failure entity:nil];
}

- (void)savePageAvatar:(NSString *)avatarString success:(successCallback)success andfailure:(failureCallback)failure {
    NSDictionary * params = @{KEY_AVATAR : avatarString, KEY_OWNER : [UserDefaults getUserID]};
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@", SERVICE_POST_PAGE_AVATAR,[UserDefaults getPageID]];
    [http put:urlStr parameters:params success:success failure:failure entity:nil];
}

- (void)savePageCover:(NSString *)avatarString success:(successCallback)success andfailure:(failureCallback)failure {
    NSDictionary * params = @{KEY_COVER_PIC : avatarString, KEY_OWNER : [UserDefaults getUserID]};
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@", SERVICE_POST_PAGE_COVER,[UserDefaults getPageID]];
    [http put:urlStr parameters:params success:success failure:failure entity:nil];
}

- (void)forgotPassword:(NSString *)email success:(successCallback)success andfailure:(failureCallback)failure {
    NSDictionary * params = @{KEY_EMAIL : email};
    [http post:SERVICE_POST_FORGOT_PASSWORD parameters:params success:success failure:failure response:[[StringResponse alloc] init:KEY_API_MESSAGE]];
}

-(void)getBraintreeClientToken:(NSDictionary *)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse *)response
{
    [http get:BRAINTREE_CLIENT_TOKEN_API success:success failure:failure response:response];
}

-(void)getCouponCode:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response
{
    [http post:PAYMENT_COUPON_CODE_API parameters:params success:success failure:failure response:nil];
}

-(void)payment:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response
{
    
    [http post:BRAINTREE_CLIENT_PAYMENT_API parameters:params success:success failure:failure response:nil];
}

- (void)getHobbiesListSuccess:(successCallback)success andfailure:(failureCallback)failure response:(PagedResponse*)response
{
    [http get:SERVICE_POST_HOBBIES_LIST  success:success failure:failure response:response];
}




@end
