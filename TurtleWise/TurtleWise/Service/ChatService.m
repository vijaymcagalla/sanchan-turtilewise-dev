//
//  ChatService.m
//  TurtleWise
//
//  Created by Irfan Gul on 25/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "MessagesPagedResponse.h"
#import "StringResponse.h"
#import "Conversation.h"
#import "ChatService.h"
#import "ChatMessage.h"
#import "Question.h"
#import "Constant.h"
#import "Keys.h"

//Service Calls
#define SERVICE_POST_INITIATE_CHAT @"questions/%@/chats"
#define SERVICE_POST_SAVE_MESSAGE @"chats/%@/message"
#define SERVICE_POST_ADD_ADVISOR @"boardOfAdvisor/%@"

#define SERVICE_PUT_END_CHAT @"chats/%@/end"
#define SERVICE_PUT_RATE_ADVISORS @"chats/%@/rate"
#define SERVICE_PUT_RATE_ALL_CHATS @"chats/rateAll"

#define SERVICE_GET_ALL_CONVERSATIONS @"%@/chats?skip=%@&pageSize=%@"
#define SERVICE_GET_QUESTION_CONVERSATIONS @"questions/%@/chats?skip=%@&pageSize=%@"
#define SERVICE_GET_CHAT_MESSAGES @"chats/%@/messages?before=%@&skip=%d"
#define SERVICE_GET_CHAT_DETAILS @"chats/%@"
#define SERVICE_GET_AVAILABILITY @"/api/chats/%@/%@/availability"

//Keys
#define KEY_ADVISORS_IDS @"advisorIds"
#define KEY_RATINGS @"ratings"

@implementation ChatService

- (void)initiateChatWithAdvisors:(NSArray *)advisors forQuestion:(NSString *)questionId onSuccess:(successCallback)success andFailure:(failureCallback)failure
{
    NSString *method = [NSString stringWithFormat:SERVICE_POST_INITIATE_CHAT, questionId];
    [http post:method parameters:@{KEY_ADVISORS_IDS : advisors} success:success failure:failure entity:[Conversation new]];
}

- (void)endChatWithId:(NSString *)chatId withSuccess:(successCallback)success andFailure:(failureCallback)failure
{
    NSString *method = [NSString stringWithFormat:SERVICE_PUT_END_CHAT, chatId];
    
    [http put:method parameters:NULL success:success failure:failure response:[[StringResponse alloc] init:KEY_CONTENT_ID]];
}

- (void)getConversationsForUserType:(NSString*)userType andParams:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response
{
    [http cancelAllOperations];
    
    NSString *method = [NSString stringWithFormat:SERVICE_GET_ALL_CONVERSATIONS,userType,[params objectForKey:KEY_SKIP],[params objectForKey:KEY_PAGE_SIZE]];
    [http get:method success:success failure:failure response:response];
}

- (void)getConversationsForQuestion:(NSString*)questionId andParams:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response
{
    [http cancelAllOperations];
 
    NSString *method = [NSString stringWithFormat:SERVICE_GET_QUESTION_CONVERSATIONS,questionId,[params objectForKey:KEY_SKIP],[params objectForKey:KEY_PAGE_SIZE]];
    [http get:method success:success failure:failure response:response];
    
}

- (void)rateAdvisors:(NSArray *)advisorsRating onChat:(NSString *)chatId withSuccess:(successCallback)success andFailure:(failureCallback)failure
{
    NSDictionary *params = @{ KEY_RATINGS : advisorsRating };
    NSString *method = [NSString stringWithFormat:SERVICE_PUT_RATE_ADVISORS, chatId];
    
    [http put:method parameters:params success:success failure:failure response:[[StringResponse alloc] init:KEY_CONTENT_ID]];
}

- (void)getChatMessagesByChatId:(NSString *)chatId withSuccess:(successCallback)success andFailure:(failureCallback)failure response:(MessagesPagedResponse *)response
{
    NSString *method = [NSString stringWithFormat:SERVICE_GET_CHAT_MESSAGES, chatId, [response messageOffset], [response skip]];
    
    [http get:method success:success failure:failure response:response];
}

- (void)getConversationDetails:(NSString*)conversationId onSuccess:(successCallback)success andFailure:(failureCallback)failure
{
    NSString *method = [NSString stringWithFormat:SERVICE_GET_CHAT_DETAILS ,conversationId];

    [http get:method success:success failure:failure entity:[Conversation new]];
}

- (void)sendMessage:(NSDictionary *)params toChat:(NSString *)chatId withSuccess:(successCallback)success andFailure:(failureCallback)failure
{
    NSString *method = [NSString stringWithFormat:SERVICE_POST_SAVE_MESSAGE, chatId];
    
    [http post:method parameters:params success:success failure:failure response:[[StringResponse alloc] init:KEY_CONTENT_ID]];
}

- (void)addAdvisor:(NSDictionary *)params withId:(NSString *)advisorId withSuccess:(successCallback)success andFailure:(failureCallback)failure
{
    NSString *method = [NSString stringWithFormat:SERVICE_POST_ADD_ADVISOR, advisorId];
    
    [http post:method parameters:params success:success failure:failure response:[[StringResponse alloc] init:KEY_CONTENT_ID]];
}

- (void)rateAllChatsWithSuccess:(successCallback)success andFailure:(failureCallback)failure
{
    [http put:SERVICE_PUT_RATE_ALL_CHATS parameters:NULL success:success failure:failure response:[[StringResponse alloc] init:KEY_API_DATA]];
}

- (void)checkAvailablityForAdvisor:(NSString *)advisorId forQuestion:(NSString *)questionId withSuccess:(successCallback)success andFailure:(failureCallback)failure {
    NSString *method = [NSString stringWithFormat:SERVICE_GET_AVAILABILITY, questionId, advisorId];
    [http get:method success:success failure:failure response:nil];
}

@end
