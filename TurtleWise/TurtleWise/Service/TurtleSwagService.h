//
//  TurtleSwagService.h
//  TurtleWise
//
//  Created by Waleed Khan on 7/12/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseService.h"

@interface TurtleSwagService : BaseService

- (void)redeemAmount:(NSInteger)amount withSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)donateAmount:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure;

@end
