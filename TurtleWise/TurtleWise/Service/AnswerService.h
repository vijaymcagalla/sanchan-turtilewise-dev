//
//  AnswerService.h
//  TurtleWise
//
//  Created by Irfan Gul on 25/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseService.h"
@class Answer;
@interface AnswerService : BaseService

-(void)getAnswersForQuestion:(NSString*)questionId andParams:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response;

-(void)postAnswerForQuestion:(NSString*)questionId andParams:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure;

-(void)postSeekersFeedbackToAdvisorsAnswer:(NSString*)feedback forQuestion:(NSString*)questionId andAnswer:(Answer*)answer;


@end
