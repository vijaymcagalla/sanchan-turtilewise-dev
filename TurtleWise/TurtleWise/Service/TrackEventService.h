//
//  TrackEventService.h
//  TurtleWise
//
//  Created by Usman Asif on 10/3/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseService.h"

@interface TrackEventService : BaseService

- (void)trackEvent:(NSString *)paramString;
@end
