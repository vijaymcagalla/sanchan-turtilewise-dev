//
//  Service.m
//  10Pearls
//
//  Created by mohsin on 3/11/14.
//  Copyright (c) 2014 SocialRadar. All rights reserved.
//

#import "Service.h"

@implementation Service
@synthesize delegate;

-(void)loadAuthService {
    if (_auth != nil) {
        return;
    }
    
    _auth = [[AuthService alloc] init];
}

-(void)loadCatalogService {
    if (_catalog != nil) {
        return;
    }
    
    _catalog = [[CatalogService alloc] init];
}

-(void)loadAnswerService {
    if (_answer != nil) {
        return;
    }
    
    _answer = [[AnswerService alloc] init];
}

-(void)loadQuestionService {
    if (_question != nil) {
        return;
    }
    
    _question = [[QuestionService alloc] init];
}

-(void)loadChatService {
    if (_chat != nil) {
        return;
    }
    
    _chat = [[ChatService alloc] init];
}

-(void)loadUserService {
    if (_user != nil) {
        return;
    }
    
    _user = [[UserService alloc] init];
}


-(void)loadSettingService {
    if (_settings != nil) {
        return;
    }
    
    _settings = [[SettingsService alloc] init];
}

-(void)loadStatsService {
    if (_stats != nil) {
        return;
    }
    
    _stats = [[StatsService alloc] init];
}

-(void)loadTagsService {
    if (_tags != nil) {
        return;
    }
    
    _tags = [[TagsService alloc] init];
}

-(void)loadPurchaseService {
    if (_purchase != nil) {
        return;
    }
    
    _purchase = [[PurchaseService alloc] init];
}

-(void)loadTurtleSwagService {
    if (_turtleSwag != nil) {
        return;
    }
    
    _turtleSwag = [[TurtleSwagService alloc] init];
}

-(void)loadTrackEventService {
    if (_trackEvent != nil) {
        return;
    }
    
    _trackEvent = [[TrackEventService alloc] init];
}

-(void)loadBrandedPageService {
    if (_brandedPageService != nil) {
        return;
    }
    
    _brandedPageService = [[BrandedPageService alloc] init];
}

#pragma mark setup

- (void)load:(NSArray*)services{
    [self loadServiceFromPList];
    
    for (NSNumber *object in services) {
        NSString *key = [NSString stringWithFormat:@"%d",[object intValue]];
        NSString *selectorString = [_servicesMap objectForKey:key];
        if(![selectorString isKindOfClass:[NSString class]]){
            NSLog(@"%@",[NSString stringWithFormat:@"service enum(%@) is not available", object]);
            @throw @"service not availble";
        }
        
        SEL selector = NSSelectorFromString([_servicesMap objectForKey:key]);
        
        if(![self respondsToSelector:selector]){
            NSLog(@"%@",[NSString stringWithFormat:@"%@ load method  not created",selectorString]);
            @throw @"service load method not created";
        }
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        
        [self performSelector:selector];
        
#pragma clang diagnostic pop
    }
    //    while( value = va_arg( args, NSNumber * ) )
}

-(void)loadServiceFromPList {
    if(_servicesMap != nil)
        return;
    
    _servicesMap = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Service" ofType:@"plist"]];
}

+ (Service*)get:(id)_delegate{
    Service *instance = [Service new];
    instance.delegate = _delegate;
    return instance;
}

@end
