//
// Created by mohsin on 8/14/13.
//


#import "UserDefaults.h"
#import "BaseService.h"
#import "Constant.h"
#import "Keys.h"

@implementation BaseService



- (id)init {
    if (self = [super init]) {
        http = [HttpRequestManager new];
        [http setHttpHeader:@{@"Content-Type": @"application/json", KEY_TOKEN : [UserDefaults getAccessToken]}];
    }
    return self;
}

-(NSArray *)loadObjectsPlist:(NSString *)fileName
{
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:PLIST_EXTENSION];
    NSArray *arrRawData = [[NSArray alloc] initWithContentsOfFile:path];
    return arrRawData;
}

@end