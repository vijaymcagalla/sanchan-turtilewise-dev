//
//  PurchaseService.m
//  TurtleWise
//
//  Created by Waleed Khan on 6/7/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "PurchaseService.h"
#import "StringResponse.h"

#define SERVICE_POST_VERIFY_RECEIPT @"payment"

@implementation PurchaseService

- (void)verifyReceipt:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure
{
    [http post:SERVICE_POST_VERIFY_RECEIPT parameters:params success:success failure:failure response:[[StringResponse alloc] init:@""]];
}

@end
