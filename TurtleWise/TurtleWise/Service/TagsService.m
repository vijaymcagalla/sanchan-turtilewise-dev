//
//  TagsService.m
//  TurtleWise
//
//  Created by Usman Asif on 2/22/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "TagsService.h"
#import "StringResponse.h"
#import "Keys.h"
#import "EnvironmentConstants.h"

#define SERVICE_TAGS @"tags/%@"
#define SERVICE_TURTLE_WISE_PAGES @"users/contentfeed"
//@"getTurtlewisepages"
#define SERVICE_AUTO_QUESTIONS @"autoquestions"
#define SERVICE_SIGNUP_QUESTIONS @"signupques"

@implementation TagsService

#pragma mark - Service Calls

- (void)getSuggestedTagsFor:(NSString *)tag withSuccess:(successCallback)success
{
    NSString * method = [[NSString stringWithFormat:SERVICE_TAGS, tag] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [http get:method success:^(id response)
     {
         NSDictionary *data = [ParserUtils object:response key:KEY_API_DATA];
         NSArray *tags = [ParserUtils object:data key:KEY_TAGS];
         
         if (success)
             success(tags);
     }
      failure:^(NSError *error)
     {
         
     }
       response:nil];
}

- (void)saveTag:(NSString *)tag
{
    NSString * method = [[NSString stringWithFormat:SERVICE_TAGS, tag] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [http post:method parameters:nil
       success:^(id response)
     {
         
     }
       failure:^(NSError *error)
     {
         
     }
      response:nil];
}

-(void)getPages:(NSDictionary *)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse *)response{
    
    [http get:SERVICE_TURTLE_WISE_PAGES success:^(id response) {
        if (success)
        {
            success(response);
        }
        else
        {
            success(nil);
        }
    
    } failure:^(NSError *error) {
        
    } response:nil];
    
}

-(void)getAutoQuestions:(NSDictionary *)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse *)response{
    
    [http get:SERVICE_AUTO_QUESTIONS success:^(id response) {
        
    } failure:^(NSError *error) {
        
    } response:nil];
    
}

-(void)getSignUpQuestions:(NSDictionary *)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse *)response{
    
    [http get:SERVICE_SIGNUP_QUESTIONS success:^(id response) {
        
    } failure:^(NSError *error) {
        
    } response:nil];
    
}

@end
