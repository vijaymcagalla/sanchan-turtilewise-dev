//
//  AuthService.h
//  TurtleWise
//
//  Created by Waaleed Khan on 11/30/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseService.h"

@interface AuthService : BaseService

- (void)loginUserViaEmail:(NSString *)email andPassword:(NSString *)password withSuccess:(successCallback)success andfailure:(failureCallback)failure;

- (void)loginWithSocial:(NSString *)appName andAccessToken:(NSString *)accessToken withSuccess:(successCallback)success andfailure:(failureCallback)failure;

- (void)signUpWithEmail:(NSString *)email andPassword:(NSString *)password confirmPassword:(NSString *)confirmPassword withSuccess:(successCallback)success andfailure:(failureCallback)failure;

- (void)logoutUserWithSuccess:(successCallback)success andFailure:(failureCallback)failure;


@end
