//
//  QuestionService.m
//  TurtleWise
//
//  Created by Irfan Gul on 25/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "QuestionService.h"
#import "StringResponse.h"
#import "UserDefaults.h"
#import "Question.h"
#import "Answer.h"
#import "Keys.h"

@implementation QuestionService

//Keys
#define KEY_COUNT @"count"
#define KEY_PROFILE @"profile"

//Services
#define SERVICE_POST_ASK_QUESTION @"questions?=userId%@"
#define SERVICE_GET_QUESTIONS     @"%@/questions?skip=%@&pageSize=%@&answered=%@"
#define SERVICE_CLOSE_QUESTION    @"questions/%@/close"
#define SERVICE_DELETE_QUESTIONS  @"questions/skip"
#define SERVICE_QUESTION_DETAILS  @"questions/%@"
#define SERVICE_GET_QUESTION_BY_ID  @"questions/%@"
#define SERVICE_POST_ADVISOR_COUNT @"questions/count"
#define SERVICE_PUT_ARCHIVE_QUESTION @"questions/%@/archive"
#define SERVICE_GET_ARCHIVE_QUESTIONS @"seeker/arcquestions?answered=%@&pageSize=%@&skip=%@"
#define SERVICE_GET_BOARD_ADVISORS @"boardOfAdvisor/%@"

#pragma mark - Serivce Calls

- (void)getQuestionForId:(NSString *)questionId withSuccess:(successCallback)success andFailure:(failureCallback)failure
{
    NSString *service = [NSString stringWithFormat:SERVICE_GET_QUESTION_BY_ID, questionId];
    
    [http get:service success:success failure:failure entity:[Question new]];
}


- (void)askQuestionWithParams:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure{
    NSString *serviceCall = [NSString stringWithFormat:SERVICE_POST_ASK_QUESTION, [UserDefaults getUserID]];
        
    [http post:serviceCall parameters:params success:success failure:failure response:[[StringResponse alloc] init:KEY_ERROR_MESSAGE]];
}



-(void)getQuestions:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response{
    
    [http cancelAllOperations];
    
    NSString *method = [NSString stringWithFormat:SERVICE_GET_QUESTIONS,[params objectForKey:KEY_USER_ROLE],[params objectForKey:KEY_SKIP],[params objectForKey:KEY_PAGE_SIZE],[params objectForKey:KEY_ANSWERED]];
    
    [http get:method success:success failure:failure response:response];
}

-(void)getArchiveQuestions:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response{
    
    [http cancelAllOperations];
    
    NSString *method = [NSString stringWithFormat:SERVICE_GET_ARCHIVE_QUESTIONS,[params objectForKey:KEY_ANSWERED],[params objectForKey:KEY_PAGE_SIZE],[params objectForKey:KEY_SKIP]];
    
    [http get:method success:success failure:failure response:response];
}

-(void)getboardOfAdvisors:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response{
    
    
    NSString *method = [NSString stringWithFormat:SERVICE_GET_BOARD_ADVISORS,@"approved"];
    
    [http get:method success:success failure:failure response:response];
}


- (void)archiveQuestion:(NSString*)qId WithParams:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure {
    
    NSString *method = [NSString stringWithFormat:SERVICE_PUT_ARCHIVE_QUESTION, qId];
    
    [http put:method parameters:params success:success failure:failure response:nil];
}

- (void)closeQuestion:(NSDictionary *)params onSuccess:(successCallback)success andfailure:(failureCallback)failure{
    NSString *method = [NSString stringWithFormat:SERVICE_CLOSE_QUESTION,[params objectForKey:KEY_QUESTION_ID]];
    
    [http put:method parameters:params success:success failure:failure response:nil];
}

-(void)deleteQuestions:(NSArray *)questions onSuccess:(successCallback)success andfailure:(failureCallback)failure{
    
    [http put:SERVICE_DELETE_QUESTIONS parameters:@{@"questionIds" : questions} success:success failure:failure response:nil];
}


-(void)getQuestionDetails:(NSString*)questionId onSuccess:(successCallback)success andFailure:(failureCallback)failure{
    NSString *method = [NSString stringWithFormat:SERVICE_QUESTION_DETAILS,questionId];
    [http get:method success:^(id response) {
        if(success)
            success([self getSeekersName:response]);
        
    } failure:^(NSError *error) {
        if(failure)
            failure(error);
    } response:nil];
}

-(NSString*)getSeekersName:(id)response{
    NSDictionary *data = [ParserUtils object:response key:KEY_API_DATA];
    NSDictionary *seeker = [ParserUtils object:data key:KEY_SEEKER];
    NSDictionary *seekerAttributes = [ParserUtils object:[ParserUtils object:seeker key:KEY_PROFILE] key:KEY_ATTRIBUTES];
    
    NSString *firstName = [ParserUtils stringValue:seekerAttributes key:KEY_FIRST_NAME];
    NSString *lastName = [ParserUtils stringValue:seekerAttributes key:KEY_LAST_NAME];
    
    NSString *fullName = nil;
    
    if(firstName && lastName){
        fullName = [NSString stringWithFormat:@"%@ %@.",firstName,[[lastName substringWithRange:NSMakeRange(0, 1)] uppercaseString]];
    }
    else if (firstName)
        fullName = firstName;
    else if (lastName)
        fullName = lastName;
    
    else
        fullName = [ParserUtils stringValue:seeker key:KEY_TURTLE_IDENTIFIER];
    
    return fullName;
}

- (void)getAdvisorCountForAskQuestion:(NSDictionary *)params onSuccess:(successCallback)success andFailure:(failureCallback)failure
{
    [http post:SERVICE_POST_ADVISOR_COUNT parameters:params success:^(id response)
    {
        NSDictionary *data = [ParserUtils object:response key:KEY_API_DATA];
        
        success([ParserUtils numberValue:data key:KEY_COUNT]);
    }
      failure:failure response:nil];
}

-(void)getAnswerWithQuestionId:(NSString *)questionId onSuccess:(successCallback)success andFailure:(failureCallback)failure{
    NSString *method = [[NSString stringWithFormat:SERVICE_QUESTION_DETAILS,questionId] stringByAppendingString:@"/answer"];
    [http get:method success:success failure:failure entity:[Answer new]];
}

@end
