//
//  AnswerService.m
//  TurtleWise
//
//  Created by Irfan Gul on 25/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "StringResponse.h"
#import "AnswerService.h"
#import "Question.h"
#import "Constant.h"
#import "Answer.h"

#define kMethodGetAnswers @"questions/%@/answers"
#define kPostAnswer       @"questions/%@/answers"
#define kPostFeedback     @"questions/%@/answers/%@/%@"

@implementation AnswerService

-(void)getAnswersForQuestion:(NSString*)questionId andParams:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response{
    
    NSString *method = [NSString stringWithFormat:kMethodGetAnswers,questionId];
    NSMutableString *methodWithParams = [NSMutableString stringWithString:method];
    
    if(params.allKeys.count){
        [methodWithParams appendString:@"?"];
        
        int index = 0;
        for(NSString *key in params.allKeys){
            
            [methodWithParams appendString:[NSString stringWithFormat:@"%@=%@",key,[params objectForKey:key]]];
            
            if(index!=params.allKeys.count-1)
                [methodWithParams appendString:@"&"];
            
            index++;
        }
    }
    
    [http get:methodWithParams success:success failure:failure response:response];
}

-(void)postAnswerForQuestion:(NSString*)questionId andParams:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure{
    
    NSString *method = [NSString stringWithFormat:kPostAnswer,questionId];
    [http post:method parameters:params success:success failure:failure response:[[StringResponse alloc] init:KEY_ERROR_MESSAGE]];
    
}
-(void)postSeekersFeedbackToAdvisorsAnswer:(NSString*)feedback forQuestion:(NSString*)questionId andAnswer:(Answer*)answer{
    
    NSString *method = [NSString stringWithFormat:kPostFeedback,questionId,answer.answerId,feedback];
    [http post:method parameters:nil success:^(id response) {
        answer.seekersFeedback = feedback;
    } failure:^(NSError *error) {
        NSLog(@"");
    } response:nil];
}
@end
