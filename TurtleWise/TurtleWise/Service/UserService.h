//
//  UserService.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/8/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseService.h"

@interface UserService : BaseService

- (void)saveProfile:(NSDictionary *)profile withSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)savePage:(NSDictionary *)profile withSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)getProfileWthSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)getProfileWthId:(NSString *)userId success:(successCallback)success andfailure:(failureCallback)failure;
- (void)getLevelTableOnSuccess:(successCallback)success andfailure:(failureCallback)failure;
- (void)saveAvatar:(NSString *)avatarString success:(successCallback)success andfailure:(failureCallback)failure;
- (void)savePageAvatar:(NSString *)avatarString success:(successCallback)success andfailure:(failureCallback)failure;
- (void)savePageCover:(NSString *)avatarString success:(successCallback)success andfailure:(failureCallback)failure;
- (void)forgotPassword:(NSString *)email success:(successCallback)success andfailure:(failureCallback)failure;

-(void)getBraintreeClientToken:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response;

-(void)getCouponCode:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response;

-(void)payment:(NSDictionary*)params onSuccess:(successCallback)success andFailure:(failureCallback)failure response:(PagedResponse*)response;
- (void)getHobbiesListSuccess:(successCallback)success andfailure:(failureCallback)failure response:(PagedResponse*)response;


@end
