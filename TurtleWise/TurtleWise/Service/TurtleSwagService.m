//
//  TurtleSwagService.m
//  TurtleWise
//
//  Created by Waleed Khan on 7/12/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "TurtleSwagService.h"
#import "StringResponse.h"
#import "Keys.h"

//Services
#define SERVICE_POST_REDEEM @"bucks/redeem"
#define SERVICE_POST_DONATE @"bucks/donate"


@implementation TurtleSwagService

- (void)redeemAmount:(NSInteger)amount withSuccess:(successCallback)success andfailure:(failureCallback)failure
{
    [http post:SERVICE_POST_REDEEM parameters:@{KEY_AMOUNT : [NSNumber numberWithInteger:amount]} success:success failure:failure response:[[StringResponse alloc] init:KEY_API_MESSAGE]];
}

- (void)donateAmount:(NSDictionary *)params withSuccess:(successCallback)success andfailure:(failureCallback)failure
{
    [http post:SERVICE_POST_DONATE parameters:params success:success failure:failure response:[[StringResponse alloc] init:KEY_API_MESSAGE]];
}

@end
