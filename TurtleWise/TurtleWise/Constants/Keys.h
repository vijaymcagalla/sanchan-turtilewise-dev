//
//  Keys.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/4/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#ifndef Keys_h
#define Keys_h

#define KEY_TOKEN @"token"
#define KEY_USER_ID @"id"
#define KEY_PAGE_ID @"pageID"
#define KEY_OWNER @"owner"
#define KEY_API_DATA @"data"
#define KEY_CONTENT_ID @"_id"
#define KEY_ERROR_MESSAGE @"message"
#define KEY_DEVICE_TYPE @"deviceType"
#define KEY_CURRENT @"current"
#define KEY_ID @"_id"

#define KEY_ATTRIBUTES @"attributes"

#define KEY_PERCENTAGE @"percentage"
#define KEY_USER_IDs @"userId"
#define KEY_TURTLE @"turtle"
#define KEY_IS_READ @"isRead"
#define KEY_TAGS @"tags"
#define KEY_PAGES @"pages"

//For User Profiles
#define KEY_EMAIL_VERIFIED @"emailVerified"
#define KEY_FIRST_NAME @"firstName"
#define KEY_LAST_NAME @"lastName"
#define KEY_CITY @"city"
#define KEY_STATE @"state"
#define KEY_COUNTRY @"country"
#define KEY_GENDER @"gender"
#define KEY_DOB @"dob"
#define KEY_AGE @"age"
#define KEY_MIN_AGE @"minAge"
#define KEY_MAX_AGE @"maxAge"
#define KEY_RELIGION @"religion"
#define KEY_ETHNICITY @"ethnicity"
#define KEY_POLITICAL_AFFILIATION @"politicalAffiliation"
#define KEY_ORIENTATION @"sexualOrientation"
#define KEY_MARITIAL_STATUS @"maritalStatus"
#define KEY_KIDS @"kids"
#define KEY_SIBLINGS @"siblings"
#define KEY_EDUCATION @"education"
#define KEY_SCHOOL @"school"
#define KEY_EMPLOYER @"employer"
#define KEY_PROFESSION @"profession"
#define KEY_HOBBIES @"hobbies"
#define KEY_SIGNUP_AS @"type"
#define KEY_NAME @"name"
#define KEY_SUMMARY @"writeSummary"
#define KEY_ARTICLE_ONE_URL @"articleOne"
#define KEY_ARTICLE_TWO_URL @"articleTwo"
#define KEY_ARTICLE_THREE_URL @"articleThree"
#define KEY_EVENT_ONE_URL @"EventOne"
#define KEY_EVENT_TWO_URL @"EventTwo"
#define KEY_EVENT_THREE_URL @"EventThree"
#define KEY_PUBLIC_OR_PRIVATE @"selectPublicOrPrivate"
#define KEY_GURU_TAGS @"guru"
#define KEY_EXPLORER_TAGS @"explorer"
#define KEY_ABOUT @"about"
#define KEY_PROFILE_PRIVACY @"privacy"
#define KEY_PRIVATE_ATTRIBUTES @"hiddenAttributes"
#define KEY_AVATAR @"avatar"
#define KEY_COVER_PIC @"cover"
#define KEY_PROFILE @"profileImage"
#define KEY_COVER @"coverImage"

#define KEY_RATING @"rating"
#define KEY_RATING_THUMBS_UP @"up"
#define KEY_RATING_THUMBS_DOWN @"down"
#define KEY_RATING_THANKS @"thanks"
#define KEY_SEARCH_OPTIONS @"searchOption"
#define KEY_CHATS @"chats"
#define KEY_MEMBER_SINCE @"memberSince"
#define KEY_QUESTIONS_ANSWERED @"questionsAnswered"
#define KEY_TURTLE_IDENTIFIER @"turtleIdentifier"
#define KEY_LEVEL @"level"
#define KEY_STAGE @"stage"
#define KEY_TURTLE_POINTS @"points"
#define KEY_TURTLE_BUCKS @"bucks"

//For Cell Entity
#define KEY_HEADER @"header"
#define KEY_RESPONSE_KEY @"responseKey"
#define KEY_PRIVACY @"privacy"
#define KEY_STEP_IN_WIZARD @"stepInWizard"
#define HEADER_ETHNICITY @"Ethnicity:"
#define HEADER_GENDER @"Gender:"
#define HEADER_RELIGION @"Religion:"
#define HEADER_ORIENTATION @"Orientation:"

//For Settings
#define KEY_EMAIL @"email"
#define KEY_QUESTION_RECEIVED @"questionReceived"
#define KEY_ANSWER_RECEIVED @"answerReceived"
#define KEY_DAILY_DIGEST @"dailyDigest"

#define KEY_TYPE @"type"
#define KEY_CREATED_AT @"createdAt"

#define KEY_CHAT_ID @"chatId"

#define KEY_SEARCH_ATTRIBUTES @"searchAttributes"
#define KEY_SEARCH_TAGS @"searchTags"

#define QUESTION_TPYE_YES_NO @"yesOrNo"
#define QUESTION_TYPE_MCQ @"mcq"

#define KEY_QUESTION @"question"
#define KEY_ANSWER @"answer"
#define KEY_REASON @"reason"
#define KEY_STATUS @"status"
#define KEY_WHY_CHOOSE_ME @"whyChooseMe"
#define KEY_CHAT_AVAILABILITY @"chatAvailability"
#define KEY_FEEDBACK @"feedback"
#define KEY_PROFILE @"profile"
#define KEY_ATTRIBUTES @"attributes"
#define KEY_CONTENT @"content"
#define KEY_ADVISOR @"advisor"
#define KEY_LIST @"list"
#define KEY_PAGENAME @"pageName"
#define KEY_WHO @"who"
#define KEY_ANSWERER @"answerer"

#define KEY_META @"meta"
#define KEY_SEEKER @"seeker"
#define KEY_SERVER_TIME @"serverTime"
#define KEY_EXPIRED_AT @"expiredAt"
#define KEY_QUESTION_ID @"questionId"
#define KEY_CURRENT @"current"
#define KEY_SENDER_ID @"senderId"
#define KEY_API_MESSAGE @"message"

#define KEY_CURRENT_PASSWORD @"currentPassword"
#define KEY_NEW_PASSWORD @"newPassword"
#define KEY_CONFIRM_NEW_PASSWORD @"confirmNewPassword"

//For Branded Page
#define KEY_PAGE @"id"
#define KEY_PAGE_DELETE_ID @"pageId"
#define KEY_TYPE @"type"
#define KEY_PRIVACY @"privacy"
#define KEY_BRANDED_SUMMARY @"summary"
#define KEY_SOCIAL @"social"
#define KEY_FACEBOOK @"facebook"
#define KEY_TWITTER @"twitter"
#define KEY_LINKEDIN @"linkedin"
#define KEY_APPROVED @"approved"
#define KEY_PENDING @"pending"
#define KEY_MEMBERS @"members"
#define KEY_TOFOLLOW @"toFollow"
#define KEY_TOUNFOLLOW @"toUnfollow"
#define KEY_FOLLOWING @"following"

//For Article
#define KEY_ARTICLES @"articles"
#define KEY_IMAGE @"image"
#define KEY_LINK @"link"
#define KEY_TITLE @"title"

//For Events
#define KEY_EVENTS @"events"
#define KEY_DATE @"date"

//Push Notification Events
#define NOTIFICATION_TYPE_RECIEVED_QUESTION @"questionReceived"
#define NOTIFICATION_TYPE_RECIEVED_ANSWER @"answerReceived"
#define NOTIFICATION_TYPE_RECIEVED_NEW_MESSAGE @"chatMessageReceived"
#define NOTIFICATION_TYPE_RECIEVED_CHAT_INVITATION @"chatInvitation"
#define NOTIFICATION_TYPE_CHAT_ENDED @"chatEnded"
#define NOTIFICATION_TYPE_UPDATE_PROFILE @"profileUpdated"
#define NOTIFICATION_TYPE_END_SUBSCRIPTION @"endSubscription"

#define KEY_TEMP_MESSAGE_ID @"tempId"

#define KEY_AMOUNT @"amount"

#define KEY_IS_SUBSCRIBED @"userSubscribed"

#endif /* Keys_h */
