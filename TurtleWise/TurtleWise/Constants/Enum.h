//
//  Enum.h
//  wusup
//
//  Created by Mohsin on 3/6/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

typedef enum  {
    HttpMethodGET       = 0,
    HttpMethodPOST      = 1,
    HttpMethodPUT       = 2,
    HttpMethodDELETE    = 3,
    HttpMethodHEAD      = 4
    
} HttpMethod;

typedef NS_ENUM(NSUInteger, UserRole)
{
    UserRoleSeeker = 0,
    UserRoleAdvisor = 1
};

typedef NS_ENUM(NSUInteger, NotificationType)
{
    NotificationTypeNewQuestion = 0,
    NotificationTypeNewAnswer = 1,
    NotificationTypeChatPenalist = 2
    
};

typedef NS_ENUM(NSUInteger, TabbarViewType) {
    TabbarViewTypeQuestion = 0,
    TabbarViewTypeAnswer,
    TabbarViewTypeChat,
};

typedef NS_ENUM(NSUInteger, QuestionType) {
    QuestionTypeYN = 0,
    QuestionTypeMultipleChoice,
    QuestionTypeComplex,
};

typedef NS_ENUM(NSUInteger, QuestionStatus) {
    QuestionStatusClosed = 0,
    QuestionStatusAdvice,
    QuestionStatusChat,
};

typedef NS_ENUM(NSUInteger, ConversationStatus) {
    ConversationStatusClosed = 0,
    ConversationStatusActive = 1
};

typedef NS_ENUM(NSUInteger, PaymentPackageType)
{
    PaymentPackageTypeMonthly = 0,
    PaymentPackageTypePack,
    PaymentPackageTypeOnlyOneQuestion
};

typedef NS_ENUM(NSUInteger, PaymentOptionType)
{
    PaymentOptionTypePaypal = 0,
    PaymentOptionTypeCard
};

typedef NS_ENUM(NSUInteger, LoginFlowType)
{
    LoginFlowTypeSocial = 0,
    LoginFlowTypeEmail = 1
};

typedef NS_ENUM(NSUInteger, AboutMeViewFlowType)
{
    AboutMeViewFlowTypeSignUp = 0,
    AboutMeViewFlowTypeEditProfile = 1
};

typedef NS_ENUM(NSUInteger, ProfileAttributeAccess)
{
    ProfileAttributeAccessPublic = 1,
    ProfileAttributeAccessPrivate = 0,
    ProfileAttributeAccessNotApplicable = 2
};

typedef NS_ENUM (NSUInteger, ChatMessageStatus)
{
    ChatMessageStatusSending = 1,
    ChatMessageStatusSent = 0
};

#pragma mark - SocketEvents

typedef NS_ENUM (NSUInteger, SocketEvents)
{
    SocketEventsRegister         = 1,
    SocketEventsQuestionRecieved = 2,
    SocketEventsAnswerRecieved   = 3,
    SocketEventsConnected        = 4,
    SocketEventsRecievedNewMessage = 5,
    SocketEventsRecievedChatInvitation = 6,
    SocketEventsChatEnded = 7,
    SocketEventsReadMessage = 8,
    SocketEventsUpdateProfile = 9,
    SocketEventsError = 10,
    SocketEventsDisconnect = 11,
    SocketEventsSubscriptionEnded = 12
};

NSString *NSStringFromSocketEvent(SocketEvents type);
SocketEvents SocketEventsFromNSString(NSString *string);

