//
//  ChatMessageCell.m
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "ChatMessageCell.h"
#import "MessageSender.h"
#import "HexColor.h"
#import "Label.h"

#define RECIEVER_CHAT_CELL @"ChatMessageCell"
#define SENDER_CHAT_CELL @"MyChatMessageCell"

#define TIP_IMAGE_SEEKER_SENDER @"chat-bubble-tip-seeker-sender"
#define TIP_IMAGE_SEEKER_RECIEVER @"chat-bubble-tip-seeker-reciever"

#define TIP_IMAGE_ADVISOR_SENDER @"chat-bubble-tip-advisor-sender"
#define TIP_IMAGE_ADVISOR_RECIEVER @"chat-bubble-tip-advisor-reciever"

#define ADVISOR_TIP_IMAGE_NAME @"chat-bubble-tip-green"

#define SEEKER_MESSAGE_VIEW_COLOR [HXColor colorWithHexString:@"#F3EAD9"]
#define ADVISOR_MESSAGE_VIEW_COLOR [HXColor colorWithHexString:@"#E5F0EC"]

@interface ChatMessageCell ()

@property(nonatomic, strong) ChatMessage *message;

@end

@implementation ChatMessageCell

#pragma mark - Life Cycle Methods

- (void)awakeFromNib
{
    // Initialization code
    [[_messageView layer] setCornerRadius:10.0f];
}

#pragma mark - Cell XIB Handling

+ (NSString *)myCellName
{
    return SENDER_CHAT_CELL;
}

+ (NSString *)cellName
{
    return RECIEVER_CHAT_CELL;
}

#pragma mark - Setter

- (void)set:(ChatMessage *)chatMessage
{
    _message = chatMessage;
    
    [_lblUserName setText:[[chatMessage messageSender] displayName]];
    
    [_lblTime setText:[chatMessage messageTimeString]];
    
    [_lblMessage setText:[chatMessage message]];
    [_lblMessage addLineSpacing:1.6f];
    
    UIColor *bgColor = SEEKER_MESSAGE_VIEW_COLOR;
    NSString *tipImageName = ([chatMessage isMyMessage]) ? TIP_IMAGE_SEEKER_SENDER : TIP_IMAGE_SEEKER_RECIEVER;
    
    if ([[chatMessage messageSender] userRole] == UserRoleAdvisor)
    {
        bgColor = ADVISOR_MESSAGE_VIEW_COLOR;
        tipImageName = ([chatMessage isMyMessage]) ? TIP_IMAGE_ADVISOR_SENDER : TIP_IMAGE_ADVISOR_RECIEVER;
    }
    
    [_messageView setBackgroundColor:bgColor];
    [_tipImage setImage:[UIImage imageNamed:tipImageName]];
    
    if ([chatMessage messageStatus] == ChatMessageStatusSending)
    {
        [_messageView setAlpha:0.5f];
    }
}

- (void)markMessageSent
{
    [_message setMessageStatus:ChatMessageStatusSent];
    [_messageView setAlpha:1.0f];
}

@end
