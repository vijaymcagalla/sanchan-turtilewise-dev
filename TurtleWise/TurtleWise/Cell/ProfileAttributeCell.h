//
//  ProfileAttributeCell.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/8/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TextField;

@protocol ProfileAttributeCellDelegate;
@interface ProfileAttributeCell : UITableViewCell

@property(weak, nonatomic) id<ProfileAttributeCellDelegate> delegate;
@property(weak, nonatomic) IBOutlet TextField *textField;

- (void)set:(NSString *)content;

@end

@protocol ProfileAttributeCellDelegate <NSObject>

- (void)saveDataForCell:(ProfileAttributeCell *)cell;


@end

