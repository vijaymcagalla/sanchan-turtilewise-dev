//
//  ViewIAndOCell.m
//  TurtleWise
//
//  Created by Sunflower on 8/31/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "ViewIAndOCell.h"
#import "BrandedPage.h"
#import "UserDefaults.h"

@implementation ViewIAndOCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [_checkIconButton setImage:[UIImage imageNamed:@"available-chat-icon-unchecked"] forState:UIControlStateNormal];
    [_checkIconButton setImage:[UIImage imageNamed:@"available-chat-icon-checked"] forState:UIControlStateSelected];
}

-(void) setData:(BrandedPage*)page {
    
    if([page avatar]) {
        [_imgAvatar setImage:[page avatar]];
    }
    
    [UserDefaults unfollow:page.pageId];
    
    if ([page.approved containsObject:[UserDefaults getUserID]]) {
        [self.followButton setTitle:@"Unfollow" forState:UIControlStateNormal];
        [UserDefaults followPublic:page.pageId];
    } else if ([page.pending containsObject:[UserDefaults getUserID]]) {
        [self.followButton setTitle:@"Pending" forState:UIControlStateNormal];
        [UserDefaults followPrivate:page.pageId];
    } else {
        [self.followButton setTitle:@"Follow" forState:UIControlStateNormal];
    }
    [_lblName setText:page.name];
    [_lblFollowers setText:[NSString stringWithFormat:@"%lu followers",(unsigned long)[[page approved] count]]];
}

@end
