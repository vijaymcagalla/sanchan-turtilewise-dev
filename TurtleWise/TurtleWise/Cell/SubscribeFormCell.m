//
//  SubscribeFormCell.m
//  TurtleWise
//
//  Created by Irfan Gul on 15/07/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SubscribeFormCell.h"
#import "Color.h"

@implementation SubscribeFormCell

+ (NSString *)cellName
{
    return NSStringFromClass([SubscribeFormCell class]);
}

+ (CGFloat)cellHeight
{
    return 50.0f;
}

- (void)awakeFromNib
{
    _txtField.layer.borderColor         = [Color greyColor999].CGColor;
    _txtField.layer.borderWidth         = 1.0;
    
    _leftHalfField.layer.borderColor    = [Color greyColor999].CGColor;
    _leftHalfField.layer.borderWidth    = 1.0;
    
    _rightHalfField.layer.borderColor   = [Color greyColor999].CGColor;
    _rightHalfField.layer.borderWidth   = 1.0;
}


-(void)setFieldType:(SubscribeFieldType)fieldType
{
    switch (fieldType) {
        case SubscribeFieldTypeFull:
        {
            _txtField.hidden        = NO;
            _leftHalfField.hidden   = YES;
            _rightHalfField.hidden  = YES;
        }
            break;
        case SubscribeFieldTypeDouble:
        {
            _txtField.hidden        = YES;
            _leftHalfField.hidden   = NO;
            _rightHalfField.hidden  = NO;
        }
            break;
        case SubscribeFieldTypeHalf:
        {
            _txtField.hidden        = YES;
            _leftHalfField.hidden   = NO;
            _rightHalfField.hidden  = YES;
        }
            break;
        default:
            break;
    }
}

@end
