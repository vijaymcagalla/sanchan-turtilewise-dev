//
//  TurtleswagCell.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Label;
@class Charity;
@class AsyncImageView;

@interface TurtleswagCell : UITableViewCell

@property (weak, nonatomic) IBOutlet Label *lblItemName;
@property (weak, nonatomic) IBOutlet Label *lblItemPrice;
@property (weak, nonatomic) IBOutlet AsyncImageView *itemImage;

- (void)set:(Charity *)catalogItem;

@end
