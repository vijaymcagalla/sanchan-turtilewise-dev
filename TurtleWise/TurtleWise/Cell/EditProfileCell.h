//
//  EditProfileCell.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/17/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ProfileCell;

@interface EditProfileCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblCellHeading;
@property (weak, nonatomic) IBOutlet UILabel *lblCellContent;

@property (weak, nonatomic) IBOutlet UISwitch *switchPrivacy;
@property (weak, nonatomic) IBOutlet UIButton *privacyText;

- (void)set:(ProfileCell *)profileCellData;
- (void)enablePrivacyOptions;

- (IBAction)setPrivacy:(id)sender;

@end
