//
//  TurtleBucksInfoCell.h
//  TurtleWise
//
//  Created by Ajdal on 6/3/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UserLevel;
@interface TurtleBucksInfoCell : UITableViewCell

@property(nonatomic,weak) IBOutlet UILabel *rewardLabel;
@property(nonatomic,weak) IBOutlet UILabel *levelLabel;
@property(nonatomic,weak) IBOutlet UILabel *rangeLabel;
@property(nonatomic,weak) IBOutlet UILabel *titleLabel;

+ (NSString *)cellName;

-(void)set:(UserLevel*)level;
@end
