//
//  ChatListTableCell.m
//  TurtleWise
//
//  Created by Ajdal on 4/21/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "ChatListTableCell.h"
#import "Question.h"
#import "Color.h"
#import "Constant.h"
#import "StringUtils.h"
#define kChatEnded @"Chat Ended"

@interface ChatListTableCell() < MZTimerLabelDelegate >{
    Conversation *_conversation;
    NSString *_participants;
}


@end

@implementation ChatListTableCell

+ (NSString *)cellName{
    return NSStringFromClass([ChatListTableCell class]);
}

-(void)awakeFromNib{
    self.unreadMessagesIndicator.layer.cornerRadius = self.unreadMessagesIndicator.frame.size.height/2;
}

-(void)set:(Conversation *)conversation forRole:(UserRole)userRole{
    _conversation = conversation;
    self.questionDescriptionLabel.text = conversation.question.questionText;
    [self.lastMessageLabel setText:[StringUtils isEmptyOrNull:conversation.lastMessage] ? @"" : conversation.lastMessage];
    [self configureTimerLabel];
    [self.participantNamesLabel setText:_participants];
    self.unreadMessagesIndicator.hidden = conversation.isRead;
    
    self.lblRatingStatus.hidden = ((userRole == UserRoleAdvisor) || ([conversation status] == ConversationStatusActive || [conversation isRated]));
}


-(void)setParticipantList:(NSString *)participants{
    _participants = participants;
}

-(void)configureTimerLabel{
    [self.timerLbl reset];
    [self.timerLbl setHidden:NO];
    
    if (_conversation.status == ConversationStatusClosed){
        [self.timerLbl setText:kChatEnded];
        [self.timerLbl setTextColor:[Color timerNormalColorForCell]];
        return;
    }
    
    [self.timerLbl setTimerType:MZTimerLabelTypeTimer];
    [self.timerLbl setShouldCountBeyondHHLimit:YES];
    [self.timerLbl setCountDownTime:[_conversation getRemainingTime]];
    [self.timerLbl startWithEndingBlock:^(NSTimeInterval countTime){
        _conversation.status = ConversationStatusClosed;
        [self.timerLbl setText:kChatEnded];
        [self.timerLbl setTextColor:[Color timerNormalColorForCell]];
    }];
    
    [self.timerLbl setDelegate:self];
}
#pragma mark - MZTimer delegate
- (void)timerLabel:(MZTimerLabel *)timerLabel countingTo:(NSTimeInterval)time timertype:(MZTimerLabelType)timerType
{
    if (time < TIMER_GOING_TO_EXPIRE_TIME) {
        [timerLabel setTextColor:[Color timerWillExpireColor]];
    }
}
@end
