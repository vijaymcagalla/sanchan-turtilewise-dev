//
//  NoContentCell.h
//  TurtleWise
//
//  Created by Sunflower on 9/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoContentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lable;
- (void) setData:(NSString *)data;
@end
