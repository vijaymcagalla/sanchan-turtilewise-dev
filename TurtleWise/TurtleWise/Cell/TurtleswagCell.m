//
//  TurtleswagCell.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "AsyncImageView.h"
#import "TurtleswagCell.h"
#import "CatalogItem.h"
#import "Constant.h"
#import "Charity.h"
#import "Label.h"

@interface TurtleswagCell ()

@end

@implementation TurtleswagCell

- (void)awakeFromNib
{
    // Initialization code
    
    [[_itemImage layer] setBorderWidth:0.5];
    [[_itemImage layer] setBorderColor:[UIColor lightGrayColor].CGColor];
}

#pragma mark - Setter

- (void)set:(Charity *)catalogItem
{
    [_lblItemName setText:[catalogItem title]];
    [_lblItemPrice setText:[catalogItem details]];
    
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:_itemImage];
    
//    [_itemImage setImage:NULL];
    [_itemImage setImageURL:[NSURL URLWithString:[catalogItem imageUrl]]];
}

@end
