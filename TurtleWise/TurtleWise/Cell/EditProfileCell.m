//
//  EditProfileCell.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/17/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "EditProfileCell.h"
#import "ProfileCell.h"
#import "Enum.h"

@interface EditProfileCell ()

- (void)showPrivacyText:(ProfileAttributeAccess)access;

@property(nonatomic, strong) ProfileCell *profileCellData;

@end

@implementation EditProfileCell

#pragma mark - Life Cycle Methods

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)enablePrivacyOptions
{
    [_privacyText setHidden:NO];
    [_switchPrivacy setHidden:NO];
}

- (void)set:(ProfileCell *)profileCellData
{
    _profileCellData = profileCellData;
    
    [_lblCellHeading setText:[profileCellData cellHeading]];
    [_lblCellContent setText:[profileCellData cellContent]];
    
    BOOL showPrivacyOptions = ![_privacyText isHidden];
    
    if (showPrivacyOptions)
    {
        [self showPrivacyText:[profileCellData profileAttributeAccess]];
    }
}

#pragma mark - IBActions

- (IBAction)setPrivacy:(id)sender
{
    BOOL isProfilePublic = [_switchPrivacy isOn];
    
    [_privacyText setSelected:!isProfilePublic];
    [_profileCellData setProfileAttributeAccess:isProfilePublic];
}

#pragma mark - Helper Methods

- (void)showPrivacyText:(ProfileAttributeAccess)access
{
    BOOL isPublic = (access == ProfileAttributeAccessNotApplicable) ? YES : access;
    BOOL isPrivacyApplicable = (access == ProfileAttributeAccessNotApplicable);
    
    [_switchPrivacy setHidden:isPrivacyApplicable];
    [_switchPrivacy setOn:isPublic];
    
    [_privacyText setHidden:isPrivacyApplicable];
    [_privacyText setSelected:!isPublic];
}

@end
