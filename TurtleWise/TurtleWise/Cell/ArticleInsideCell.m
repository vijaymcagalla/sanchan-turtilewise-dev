//
//  ArticleInsideCell.m
//  TurtleWise
//
//  Created by Sunflower on 8/28/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "ArticleInsideCell.h"
#import "BrandedPageManager.h"

@implementation ArticleInsideCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setImage:(NSString*)url withTitle:(NSString*)title {
    
    if ([[BrandedPageManager sharedManager] isOwn]) {
        [_editButton setHidden:NO];
        [_deleteButton setHidden:NO];
    } else {
        [_editButton setHidden:YES];
        [_deleteButton setHidden:YES];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData* imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:url]];
        UIImage* img = [UIImage imageWithData:imageData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_imgArticle setImage:img];
        });
        
    });
    
    self.txtTitle.text = title;
}

@end
