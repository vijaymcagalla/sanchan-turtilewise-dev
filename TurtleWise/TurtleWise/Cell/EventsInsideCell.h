//
//  EventsInsideCell.h
//  TurtleWise
//
//  Created by Sunflower on 8/29/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsInsideCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblEventName;
@property (weak, nonatomic) IBOutlet UILabel *lblEventDate;
@property (weak, nonatomic) IBOutlet UIImageView *imgEvent;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

-(void) setImage:(NSString*)url withTitle:(NSString*)title andDate:(NSString*)date;
@end
