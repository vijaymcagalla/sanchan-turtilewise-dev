//
//  UserProfileCell.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/9/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "UserProfileCell.h"
#import "ProfileCell.h"

@implementation UserProfileCell

#pragma mark - Life cycle Methods

- (void)awakeFromNib
{
    // Initialization code
    _txtViewContent.textContainerInset = UIEdgeInsetsZero;
    _txtViewContent.textContainer.lineFragmentPadding = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Setter

- (void)set:(ProfileCell *)profileCell
{
    [_lblHeading setText:[profileCell cellHeading]];
    [_txtViewContent setText:[profileCell cellContent]];
}

@end
