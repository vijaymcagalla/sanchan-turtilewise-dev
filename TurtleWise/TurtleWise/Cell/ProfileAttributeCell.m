//
//  ProfileAttributeCell.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/8/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "ProfileAttributeCell.h"
#import "TextField.h"

@interface ProfileAttributeCell () < UITextFieldDelegate >

@end

@implementation ProfileAttributeCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    [_textField setDelegate:self];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - TextField Delegate Methods

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (_delegate == NULL || ![_delegate respondsToSelector:@selector(saveDataForCell:)])
    {
        return;
    }
    
    [_delegate saveDataForCell:self];
}

- (void)set:(NSString *)content
{
    [_textField setText:content];
}

@end
