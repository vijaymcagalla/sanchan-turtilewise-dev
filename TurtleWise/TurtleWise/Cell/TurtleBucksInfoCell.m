//
//  TurtleBucksInfoCell.m
//  TurtleWise
//
//  Created by Ajdal on 6/3/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "TurtleBucksInfoCell.h"
#import "Font.h"
#import "Color.h"
#import "UserDefaults.h"
#import "UserLevel.h"

@implementation TurtleBucksInfoCell

+ (NSString *)cellName{
    return NSStringFromClass([TurtleBucksInfoCell class]);
}
- (void)awakeFromNib {
    // Initialization code
    
    
}
-(void)set:(UserLevel*)level{
    [self.levelLabel setText:[NSString stringWithFormat:@"LEVEL %ld",(long)level.level]];
    [self.titleLabel setText:level.title];
    [self.rangeLabel setText:[NSString stringWithFormat:@"RANGE: %ld - %ld BUCKS",(long)level.minBucks,(long)level.maxBucks]];
    
    if(level.maxBucks == 0)
        [self.rangeLabel setText:[NSString stringWithFormat:@"RANGE: >=%ld BUCKS",(long)level.minBucks]];
    
    [self setRewardLabelText:level];
}
-(void)setRewardLabelText:(UserLevel*)level{
    NSString *reward = [NSString stringWithFormat:@"%ld TB",(long)level.chatReward];
    NSString *text = [NSString stringWithFormat:@"%@\nCHAT\nREWARD",reward];
    
    NSDictionary *attributesLight = @{NSFontAttributeName            : [Font lightFontWithSize:13],
                                      NSForegroundColorAttributeName : [Color greenColor]};
    
    
    UIFont *boldFont = [Font boldFontWithSize:15];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:text
                                                                                  attributes:attributesLight];
    
    [attrString addAttribute:NSFontAttributeName
                       value:boldFont
                       range:[text rangeOfString:reward]];
    
    
    [self.rewardLabel setAttributedText:attrString];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
