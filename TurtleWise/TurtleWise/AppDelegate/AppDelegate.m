//
//  AppDelegate.m
//  TurtleWise
//
//  Created by Waaleed Khan on 11/27/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//
@import AirshipKit;
@import Braintree;

#import "UINavigationController+PushNotificationRoutes.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Crashlytics/Crashlytics.h>
#import "PushNotificationManager.h"
#import "SocialLoginController.h"
#import "LandingController.h"
#import "EnvironmentConstants.h"
#import "AnalyticsHelper.h"
#import "SocketIOManager.h"
#import "HomeController.h"
#import "MenuController.h"
#import <Fabric/Fabric.h>
#import "UserDefaults.h"
#import "AppDelegate.h"
#import "IAPManager.h"


@interface AppDelegate () < PushNotificationManagerDelegate ,UAPushNotificationDelegate,UARegistrationDelegate>

@property(nonatomic, strong) MFSideMenuContainerViewController *sideMenuContainer;

- (void)setCenterController:(id)centerViewController rightViewController:(id)rightViewController;

@end
NSString *returnSchemeUrl = @"com.turtle.QA.payments";

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    UIViewController *rootController = [LandingController new];
    
    if ([UserDefaults isUserLogin])
    {
        rootController = [self getHomeControllerWithSlideMenu];
    }
    
    _navigationController = [[UINavigationController alloc] initWithRootViewController:rootController];
    
    [[self window] setRootViewController:_navigationController];
    [[self window] makeKeyAndVisible];
    
    [PushNotificationManager registerForPushNotification];
    [self registerPushNotificationUsingUrbanAirShip];
    [BTAppSwitch setReturnURLScheme:returnSchemeUrl];
    
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey] && [UserDefaults isUserLogin])
    {
        [self handleRoutesForNotificationWithPayload:[launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]];
    }
    
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsUserActivityDictionaryKey])
    {
        NSDictionary *userActivityDictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsUserActivityDictionaryKey];
        if (userActivityDictionary) {
            [userActivityDictionary enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                if ([obj isKindOfClass:[NSUserActivity class]]) {
                    [self handleRoutesForUserActivity:obj];
                }
            }];
        }
    }
    
    
    
    _showCheckForSubscriptionExpiray = YES;
    _isShowingHome = NO;
    
    //Crashlytics
    [Fabric with:@[[Crashlytics class]]];
    
    //In App Purchase
    [[SKPaymentQueue defaultQueue] addTransactionObserver:[IAPManager sharedInstance]];
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

-(void)registerPushNotificationUsingUrbanAirShip
{
    UAConfig *config = [UAConfig defaultConfig];
    config.developmentAppKey = @"E-4sikB6SAODOFabxHa2Bw";
    config.developmentAppSecret = @"ndGq9ICbQVSKUptakh1TTA";
    config.productionAppKey = @"pHb4SfXBSxONj6_HhPrPOw";
    config.productionAppSecret = @"Ocw36Mz8QX6kgPPKgIT8Kg";
    
    [UAirship takeOff:config];

    [UAirship push].userPushNotificationsEnabled = YES;
    [UAirship push].defaultPresentationOptions = (UNNotificationPresentationOptionAlert |
        UNNotificationPresentationOptionBadge |
        UNNotificationPresentationOptionSound);
    [UAirship push].registrationDelegate = self;
    NSString *channelId = [UAirship push].channelID;
    NSLog(@"ChannelID:%@",channelId);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getNotificationFromUrbanAir:)
                                                 name:UAChannelCreatedEvent object:nil];
}

-(void)getNotificationFromUrbanAir:(NSNotification*)notify
{
    NSLog(@"%@",[notify description]);
}
- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [AnalyticsHelper endSession];
    [[SocketIOManager instance] disconnectScocket];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [AnalyticsHelper startAnalyticsSession];
    [FBSDKAppEvents activateApp];
    [[SocketIOManager instance] connectScocket];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:[IAPManager sharedInstance]];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    if (([ENVIRONMENT isEqualToString:ENVIRONMENT_QA] && [url.absoluteString isEqualToString:[URL_SCHEME_REDEEM_QA lowercaseString]])  ||
        ([ENVIRONMENT isEqualToString:ENVIRONMENT_STG] && [url.absoluteString isEqualToString:[URL_SCHEME_REDEEM_STG lowercaseString]])||
        ([ENVIRONMENT isEqualToString:ENVIRONMENT_PROD] && [url.absoluteString isEqualToString:[URL_SCHEME_REDEEM_PROD lowercaseString]])) {
        
        [self proceedToHome];
        return YES;
    }
    if([url.scheme localizedCaseInsensitiveCompare:returnSchemeUrl] == NSOrderedSame)
    {
        return [BTAppSwitch handleOpenURL:url sourceApplication:sourceApplication];
    }
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void)application:(UIApplication *)application didFailToContinueUserActivityWithType:(NSString *)userActivityType error:(NSError *)error {
    NSLog(@":: %@, :: %@", error, error.localizedDescription);
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler
{
    if ([userActivity.activityType isEqualToString:NSUserActivityTypeBrowsingWeb]) {
        [self handleRoutesForUserActivity:userActivity];
        return YES;
    }
    return NO;
}

+(AppDelegate*)getInstance
{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}

#pragma mark - Slide Menu Handling

- (MFSideMenuContainerViewController *)getHomeControllerWithSlideMenu
{
    [self setHomeAsRootViewControllerWithSideMenu];
    return _sideMenuContainer;
}

- (void)proceedToHome
{
    [self setHomeAsRootViewControllerWithSideMenu];
    [[self window] setRootViewController:_sideMenuContainer];
}

- (void)setHomeAsRootViewControllerWithSideMenu
{
    _navigationController = [[UINavigationController alloc] initWithRootViewController:[HomeController new]];
    [self setCenterController:_navigationController rightViewController:[MenuController new]];
}

- (void)setCenterController:(id)centerViewController rightViewController:(id)rightViewController
{
    _sideMenuContainer = [MFSideMenuContainerViewController containerWithCenterViewController:centerViewController
                                                        leftMenuViewController:nil
                                                        rightMenuViewController:rightViewController];
    
    [_sideMenuContainer setPanMode:MFSideMenuPanModeNone];
}

-(void) showSocialLoginController {
    [_navigationController setViewControllers:@[[SocialLoginController new]]];
}

#pragma mark - Push Notification Methods

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [PushNotificationManager storeDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{    
    NSLog(@"%@, %@", error, error.localizedDescription);
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [self handleRoutesForNotificationWithPayload:userInfo];
}

- (void)handleRoutesForNotificationWithPayload:(NSDictionary *)payload
{
    PushNotificationManager *pushNoficiationManager = [[PushNotificationManager alloc] initWithPayload:payload andDelegate:self];
    [pushNoficiationManager handleRoutesForNotification];
}

- (void)handleRoutesForUserActivity:(NSUserActivity *)userActivity {
    PushNotificationManager *pushNoficiationManager = [[PushNotificationManager alloc] initWithUserActivity:userActivity andDelegate:self];
    [pushNoficiationManager handleRoutesForUserActivity];
}

#pragma mark -

- (void)proceedToResetPassword:(NSString *)token
{
    [_navigationController proceedToResetPassword:token];
}

- (void)proceedToEmailNotificationSettings:(NSString *)token
{
    [_navigationController proceedToEmailNotificationSettings:token];
}

#pragma mark - PushNotificationManager Delegate Methods

- (void)recievedQuestionWithId:(NSString *)questionId
{
    [_navigationController proceedToAnswerQuestion:questionId];
}

- (void)recievedAnswerForQuestion:(NSString *)questionId
{
    [_navigationController proceedToSeeAnswers:questionId];
}

- (void)recievedNewMessage:(ChatMessage *)chatMessage isChatActive:(BOOL)isChatActive
{
    [_navigationController proceedToChat:chatMessage isChatActive:isChatActive];
}

- (void)recievedNewChat:(NSString *)chatId
{
    [_navigationController proceedToNewlyInvitedChat:chatId];
}

- (void)recievedEndSubscription
{
    [_navigationController proceedToHomeForSubscriptionEnd];
}

@end
