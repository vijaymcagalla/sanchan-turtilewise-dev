//
//  AnswerView.m
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "AnswerView.h"
#import "AnswerController.h"
#import "TabbarView.h"
#import "Constant.h"
#import "Question.h"
#import "Answer.h"
#import "Color.h"
#import "AnswerOption.h"
#import "AnswerOptionDetailCell.h"
#import "StringUtils.h"
#import "Font.h"
#import "UserDefaults.h"

#define kTagAdvisorTagsView 1
#define kTagGuruTagsView 2

#define kMinAnswerOptionLabelHeight 48
#define kMaxGraphHeight 135

#define kSearchOptionDefault 0
#define kSelectedTabIndexNone -1

@interface AnswerView ()<TLTagsControlDelegate,TabbarViewDelegate,UITableViewDataSource,UITableViewDelegate,MZTimerLabelDelegate>

@property(nonatomic, strong) Question *question;
@property(nonatomic, assign) int totalResponses;
@property(nonatomic, assign) int selectedTabIndex;
@property(nonatomic, assign) BOOL shouldUpdateAnswers;
@property (strong, nonatomic) IBOutlet UIButton *archiveBtn;

- (void)setupTabBar;

@end

@implementation AnswerView
#pragma mark --
-(void)viewDidLoad{
    [self.statsView setValue:@(YES) forKey:@"hidden"];
    _selectedTabIndex = kSelectedTabIndexNone;
    [self addConstraintsToContentView];
    [self setupTabBar];
    
    _shouldUpdateAnswers = NO;
    
    
    _archiveBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _archiveBtn.layer.borderWidth = 1.0;
}

- (void)viewWillAppear
{
    [_tabBar updateNotificationValues:[UserDefaults getUserStats]];
    
    if (_shouldUpdateAnswers)
    {
        [(AnswerController *)self.controller loadAnswerMeta];
    }
}

- (void)viewDidDisappear
{
    _shouldUpdateAnswers = YES;
}

- (void)setupTabBar
{
    [_tabBar setDelegate:self];
    [_tabBar setSelectedTab:TabbarViewTypeAnswer];
}

-(void)setQuestion:(Question*)question{
    _question = question;
    self.questionTitle.text = question.questionText;
    [self.questionImage setImage:[self getImageWithQuestionType:_question.questionType]];
    [self addAdvisorAttributeTags];
    [self addGuruTags];
    [self setTimer];
    [self setupAnswerChoicesTableView];
}
-(void)setArchived:(BOOL)archived
{
    self.archiveBtn.hidden = archived;
}

-(void)showError:(NSString *)error{
    [self.activityIndicator stopAnimating];
    [self.errorLabel setText:error];
    [self.errorLabel setHidden:NO];
}
#pragma mark -- Timer 
-(void)setTimer{
    [self.questionTime reset];
    [self.questionTime setTextColor:[Color timerNormalColorForCell]];
    
    if (_question.status == QuestionStatusClosed || [_question isExpired]){
        [self.lblCurrentPeriod setHidden:YES];
        [self.questionTime setText:TITLE_TIMER_CLOSED];
        [self.questionTime setTextColor:[Color timerNormalColorForDetail]];
        [_lblRemaining setText:@""];
        return;
    }
    [self.lblCurrentPeriod setHidden:NO];
    [self.lblCurrentPeriod setText:(_question.status == QuestionStatusAdvice) ? ADVICE_PERIOD : CHAT_PERIOD];
    
    [self.questionTime setTimerType:MZTimerLabelTypeTimer];
    [self.questionTime setShouldCountBeyondHHLimit:YES];
    [self.questionTime setCountDownTime:[_question getRemainingTime]];
    [self.questionTime startWithEndingBlock:^(NSTimeInterval countTime) {
        [self onTimerStop];
    }];
    [self.questionTime setDelegate:self];
    
}
-(void)onTimerStop{
    
    switch (_question.status) {
        case QuestionStatusAdvice:
            [self onTimerStopForQuestionStatusAdvice];
            break;
            
        case QuestionStatusChat:
            [self onTimerStopForQuestionStatusChat];
            break;
            
        default:
            break;
    }
    
}

-(void)onTimerStopForQuestionStatusAdvice{
    _question.status = QuestionStatusChat;
    [self setTimer];
}

-(void)onTimerStopForQuestionStatusChat{
    _question.status = QuestionStatusClosed;
    [self.questionTime setText:TITLE_TIMER_CLOSED];
    [self.questionTime setTextColor:[Color timerNormalColorForDetail]];
    [self.lblCurrentPeriod setHidden:YES];
    [_lblRemaining setText:@""];
    
}
#pragma mark - Override Methods

- (BOOL)hasTabBar
{
    return YES;
}

- (void)updateStatsOnTabBar:(UserStats *)userStats
{
    if (!_tabBar)
    {
        return;
    }
    
    [_tabBar updateNotificationValues:userStats];
}

#pragma mark - Stats View
-(void)setAnswerOptions:(NSArray*)answerOptions andTotalCount:(int)total{
    
    [self.activityIndicator stopAnimating];
    [self.contentView setHidden:NO];
    
    _totalResponses = total;
    switch (answerOptions.count) {
            
        case 0:
            [self layoutViewForComplexQuestion];
            break;
            
        case 2:
            [self layoutViewForTwoChoiceQuestion:answerOptions];
            break;
            
        case 3:
            [self layoutViewForThreeChoiceQuestion:answerOptions];
            break;
            
        case 4:
            [self layoutViewForFourChoiceQuestion:answerOptions];
            break;
            
        default:
            break;
    }
}
-(void)layoutViewForComplexQuestion{
    self.answerButtonsHolderHeightConstraint.constant = 0;
    self.statsViewHeightConstraintTwoChoiceQuestion.constant = 0;
    self.tableViewHeightConstraint.constant = 0;
    [self layoutIfNeeded];
}

-(void)layoutViewForTwoChoiceQuestion:(NSArray*)answerOptions{
    self.threeChoiceQuestionView.hidden = self.fourChoiceQuestionView.hidden = YES;
    self.yesNoQuestionView.hidden = NO;
    
    self.heightConstraintGraphATwoChoiceQuestion.constant = 0;
    self.heightConstraintGraphBTwoChoiceQuestion.constant = 0;
    [self layoutIfNeeded];
    
    
    AnswerOption *optionA = [answerOptions firstObject];

    for(UIButton *btn in self.answerOptionButtonsTwoChoiceQuestion){
        [btn setTitle:[self getMCQBtnTileForAnswerAtIndex:btn.tag] forState:UIControlStateNormal];
    }
    
    double percentA = (((double)optionA.count/_totalResponses) * 100) ;
    double percentB = (100.0 - percentA) ;
    
    if(_totalResponses == 0)
        percentA = percentB = 0.0;
    
    self.percentageATwoChoiceMCQ.text = [NSString stringWithFormat:@"%d%@",(int)percentA,@"%"];
    self.percentageBTwoChoiceMCQ.text = [NSString stringWithFormat:@"%d%@",(int)percentB,@"%"];
    
    double graphHeightA = (percentA/100) * kMaxGraphHeight;
    double graphHeightB = (percentB/100) * kMaxGraphHeight;
    
    self.statsViewHeightConstraintTwoChoiceQuestion.constant = [self getTallestGraphsHeight:@[@(graphHeightA),@(graphHeightB)]] + 115;
    
    [self layoutIfNeeded];
    
    self.heightConstraintGraphATwoChoiceQuestion.constant = graphHeightA;//135 is the maximum height of the graph
    self.heightConstraintGraphBTwoChoiceQuestion.constant = graphHeightB;
    
    if(_question.questionType == QuestionTypeYN)
        self.tableViewHeightConstraint.constant = 0;
    else
        [self updateTableHeight];
    
    //[self updateTableHeight];
    [self.statsView setValue:@(NO) forKey:@"hidden"];
    
    
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
    }];
}
-(void)layoutViewForThreeChoiceQuestion:(NSArray*)answerOptions{
    
    self.yesNoQuestionView.hidden = self.fourChoiceQuestionView.hidden = YES;
    self.threeChoiceQuestionView.hidden = NO;
    
    self.heightConstraintGraphAThreeChoiceQuestion.constant = 0;
    self.heightConstraintGraphBThreeChoiceQuestion.constant = 0;
    self.heightConstraintGraphCThreeChoiceQuestion.constant = 0;
    [self layoutIfNeeded];
    
    
    AnswerOption *optionA = [answerOptions firstObject];
    AnswerOption *optionB = [answerOptions objectAtIndex:1];
    AnswerOption *optionC = [answerOptions objectAtIndex:2];
    
    for(UIButton *btn in self.answerOptionButtonsThreeChoiceQuestion){
        [btn setTitle:[self getMCQBtnTileForAnswerAtIndex:btn.tag] forState:UIControlStateNormal];
    }
    
    double percentA = (((double)optionA.count/_totalResponses) * 100);
    double percentB = (((double)optionB.count/_totalResponses) * 100);
    double percentC = (((double)optionC.count/_totalResponses) * 100);
    
    if(_totalResponses == 0)
        percentA = percentB = percentC = 0.0;
    
    self.percentageAThreeChoiceMCQ.text = [NSString stringWithFormat:@"%d%@",(int)percentA,@"%"];
    self.percentageBThreeChoiceMCQ.text = [NSString stringWithFormat:@"%d%@",(int)percentB,@"%"];
    self.percentageCThreeChoiceMCQ.text = [NSString stringWithFormat:@"%d%@",(int)percentC,@"%"];
    
    double graphHeightA = (percentA/100) * kMaxGraphHeight;
    double graphHeightB = (percentB/100) * kMaxGraphHeight;
    double graphHeightC = (percentC/100) * kMaxGraphHeight;
    
    self.statsViewHeightConstraintThreeChoiceQuestion.constant = [self getTallestGraphsHeight:@[@(graphHeightA),@(graphHeightB),@(graphHeightC)]] + 115;
    
    
    [self layoutIfNeeded];
    
    self.heightConstraintGraphAThreeChoiceQuestion.constant = graphHeightA;//135 is the maximum height of the graph
    self.heightConstraintGraphBThreeChoiceQuestion.constant = graphHeightB;
    self.heightConstraintGraphCThreeChoiceQuestion.constant = graphHeightC;
    
    //self.tableViewHeightConstraint.constant = (65*3)+5;
    [self updateTableHeight];
    
    [self.statsView setValue:@(NO) forKey:@"hidden"];
    
    
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
    }];
}

-(void)layoutViewForFourChoiceQuestion:(NSArray*)answerOptions{
    
    self.yesNoQuestionView.hidden = self.threeChoiceQuestionView.hidden = YES;
    self.fourChoiceQuestionView.hidden = NO;
    
    self.heightConstraintGraphAFourChoiceQuestion.constant = 0;
    self.heightConstraintGraphBFourChoiceQuestion.constant = 0;
    self.heightConstraintGraphCFourChoiceQuestion.constant = 0;
    self.heightConstraintGraphDFourChoiceQuestion.constant = 0;
    [self layoutIfNeeded];
    
    
    AnswerOption *optionA = [answerOptions firstObject];
    AnswerOption *optionB = [answerOptions objectAtIndex:1];
    AnswerOption *optionC = [answerOptions objectAtIndex:2];
    AnswerOption *optionD = [answerOptions objectAtIndex:3];
    
    for(UIButton *btn in self.answerOptionButtonsFourChoiceQuestion){
        [btn setTitle:[self getMCQBtnTileForAnswerAtIndex:btn.tag] forState:UIControlStateNormal];
    }
    
    double percentA = (((double)optionA.count/_totalResponses) * 100);
    double percentB = (((double)optionB.count/_totalResponses) * 100);
    double percentC = (((double)optionC.count/_totalResponses) * 100);
    double percentD = (((double)optionD.count/_totalResponses) * 100);
    
    if(_totalResponses == 0)
        percentA = percentB = percentC = percentD = 0.0;
    
    self.percentageAFourChoiceMCQ.text = [NSString stringWithFormat:@"%d%@",(int)percentA,@"%"];
    self.percentageBFourChoiceMCQ.text = [NSString stringWithFormat:@"%d%@",(int)percentB,@"%"];
    self.percentageCFourChoiceMCQ.text = [NSString stringWithFormat:@"%d%@",(int)percentC,@"%"];
    self.percentageDFourChoiceMCQ.text = [NSString stringWithFormat:@"%d%@",(int)percentD,@"%"];
    
    double graphHeightA = (percentA/100) * kMaxGraphHeight;
    double graphHeightB = (percentB/100) * kMaxGraphHeight;
    double graphHeightC = (percentC/100) * kMaxGraphHeight;
    double graphHeightD = (percentD/100) * kMaxGraphHeight;
    
    self.statsViewHeightConstraintFourChoiceQuestion.constant = [self getTallestGraphsHeight:@[@(graphHeightA),@(graphHeightB),@(graphHeightC),@(graphHeightD)]] + 115;
    
    
    [self layoutIfNeeded];
    
    self.heightConstraintGraphAFourChoiceQuestion.constant = graphHeightA;//135 is the maximum height of the graph
    self.heightConstraintGraphBFourChoiceQuestion.constant = graphHeightB;
    self.heightConstraintGraphCFourChoiceQuestion.constant = graphHeightC;
    self.heightConstraintGraphDFourChoiceQuestion.constant = graphHeightD;
    
    
    //self.tableViewHeightConstraint.constant = (65*4)+5;
    [self updateTableHeight];
    
    [self.statsView setValue:@(NO) forKey:@"hidden"];
    
    
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
    }];
}

-(double)getTallestGraphsHeight:(NSArray*)heights{
    NSArray *sortedArray = [heights sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO]]];
    return [[sortedArray firstObject] doubleValue];
    
}

- (IBAction)archive:(id)sender {
    [(AnswerController*)self.controller archiveQuestionById];
}

-(NSString*)getMCQBtnTileForAnswerAtIndex:(NSInteger)index{
    if(_question.questionType == QuestionTypeYN){
        if(index == 0)
            return @"YES";
        else
            return @"NO";
    }
    else if (_question.questionType == QuestionTypeMultipleChoice){
        switch (index) {
            case 0:
                return @"A";;
                
            case 1:
                return @"B";
                
            case 2:
                return @"C";
                
            case 3:
                return @"D";
                
            default:
                break;
        }
    }
    return @"";
}
#pragma mark - UI Setup
-(void)addConstraintsToContentView{
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                      attribute:NSLayoutAttributeLeading
                                                                      relatedBy:0
                                                                         toItem:self
                                                                      attribute:NSLayoutAttributeLeading
                                                                     multiplier:1.0
                                                                       constant:0];
    [self addConstraint:leftConstraint];
    
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                       attribute:NSLayoutAttributeTrailing
                                                                       relatedBy:0
                                                                          toItem:self
                                                                       attribute:NSLayoutAttributeTrailing
                                                                      multiplier:1.0
                                                                        constant:0];
    [self addConstraint:rightConstraint];
    
    [self layoutIfNeeded];
    
}

- (UIImage *)getImageWithQuestionType:(QuestionType )questionType{
    switch (questionType)
    {
        case QuestionTypeYN:
            return [UIImage imageNamed:@"question-type-icon-yes-no"];
            
        case QuestionTypeComplex:
            return [UIImage imageNamed:@"question-type-icon-complex"];
            
        default:
            return [UIImage imageNamed:@"question-type-icon-multiple"];
    }
}

#pragma mark - TableView DataSource/Delegate
-(void)setupAnswerChoicesTableView{
    [self.tableViewAnswerOptionDetails registerNib:[UINib nibWithNibName:[AnswerOptionDetailCell cellName] bundle:[NSBundle mainBundle]] forCellReuseIdentifier:[AnswerOptionDetailCell cellName]];
    self.tableViewAnswerOptionDetails.estimatedRowHeight = 65;
    self.tableViewAnswerOptionDetails.rowHeight = UITableViewAutomaticDimension;
    [self.tableViewAnswerOptionDetails setLayoutMargins:UIEdgeInsetsZero];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AnswerOptionDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:[AnswerOptionDetailCell cellName]];
    [cell setTitle:[self getMCQBtnTileForAnswerAtIndex:indexPath.row] andDetail:[_question.answerOptions objectAtIndex:indexPath.row]];
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(_question.questionType == QuestionTypeMultipleChoice)
        return _question.answerOptions.count;
    else
        return 0;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 5.0)];
    [view setBackgroundColor:[UIColor colorWithRed:215.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1.0]];
    return view;
}

-(void)updateTableHeight{
    CGFloat totalHeight = 0;
    CGFloat labelWidth = [[UIScreen mainScreen] bounds].size.width - 60;
    
    CGFloat topAndBottonMargin = 16.0;
    
    for(NSString *str in _question.answerOptions){
        CGFloat height = [Font getHeight:str andFont:[Font lightFontWithSize:17.0] andWidth:labelWidth];
        height = (height>kMinAnswerOptionLabelHeight) ? height : kMinAnswerOptionLabelHeight;
        
        height = height + topAndBottonMargin;
        totalHeight = totalHeight + height;
    }
    
    self.tableViewHeightConstraint.constant = totalHeight+5.0;
    [self layoutIfNeeded];
    
}


#pragma mark - Tags View
-(void)addAdvisorAttributeTags{
    [self.advisorTagsView setMode:TLTagsControlModeReadOnly];
    [self.advisorTagsView setControlDelegate:self];
    [self.advisorTagsView setTags:[NSMutableArray arrayWithArray:_question.advisorAttributes]];
    [self.advisorTagsView reload];
}

-(void)addGuruTags{
    [self.guruTagsView setMode:TLTagsControlModeReadOnly];
    [self.guruTagsView setControlDelegate:self];
    [self.guruTagsView setTags:[NSMutableArray arrayWithArray:_question.searchTags]];
    [self.guruTagsView reload];
}

#pragma mark - Actions
-(IBAction)onCheckAdvisorsBtnTap:(id)sender{
    [self goToAdvisorListingControllerWithSearchOption:kSearchOptionDefault];
}

-(IBAction)onViewChatsBtnTap:(id)sender{
    if([(AnswerController*)self.controller customDelegate] && [[(AnswerController*)self.controller customDelegate] respondsToSelector:@selector(loadConversationsForQuestion:)])
        [[(AnswerController*)self.controller customDelegate] loadConversationsForQuestion:_question.questionId];
}

-(IBAction)didTapAnswerChoiceButton:(id)sender{
    [self goToAdvisorListingControllerWithSearchOption:[(UIButton*)sender tag]];
}
-(void)goToAdvisorListingControllerWithSearchOption:(NSInteger)searchOption{
    if(_totalResponses == 0){
        [Alert show:@"" andMessage:@"No response yet."];
        return;
    }
    
    [(AnswerController*)self.controller goToAdvisorListingControllerWithSearchOption:searchOption];
}

#pragma mark - Tab Bar Delegate
-(void)tabbar:(TabbarView *)tabbar didSelectTab:(TabbarViewType)tabViewType{
    
    if(_selectedTabIndex == kSelectedTabIndexNone){
        [(AnswerController*)self.controller loadAnswerMeta];
        _selectedTabIndex = TabbarViewTypeAnswer;
        return;
    }
    
    if([(AnswerController*)self.controller customDelegate] && [[(AnswerController*)self.controller customDelegate] respondsToSelector:@selector(didSelectAnswerControllerTabBarBtn:)])
        [[(AnswerController*)self.controller customDelegate] didSelectAnswerControllerTabBarBtn:tabViewType];
    
}



#pragma mark - MZTimer delegate
- (void)timerLabel:(MZTimerLabel *)timerLabel countingTo:(NSTimeInterval)time timertype:(MZTimerLabelType)timerType
{
    if (time < TIMER_GOING_TO_EXPIRE_TIME) {
        [timerLabel setTextColor:[Color timerWillExpireColor]];
    }
}
@end
