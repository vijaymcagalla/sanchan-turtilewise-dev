//
//  SubmitQuestionView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/11/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SubmitQuestionController.h"
#import "SubmitQuestionView.h"
#import "SeekingAdvisor.h"
#import "TLTagsControl.h"
#import "Constant.h"
#import "Label.h"
#import "Color.h"

#define TITLE_ALERT_CONFIRMATION @"Are You Sure?"
#define MESSAGE_ALERT_CONFIRMATION @"Your Question will be sent to %@ Gurus!\n\nYou can expand your field of Gurus by clicking cancel to modify your search criteria."

@interface SubmitQuestionView () <UIAlertViewDelegate>

- (void)confirgureTagsView;

@end

@implementation SubmitQuestionView

#pragma mark - UIView Life Cycle

- (void)viewDidLoad
{
    [self setSwipeToBackGesture];
    [self confirgureTagsView];
}

- (void)confirgureTagsView
{
    [_attributesTagsView setMode:TLTagsControlModeReadOnly];
    
    [_guruTagsView setMode:TLTagsControlModeReadOnly];
}

- (void)setData:(SeekingAdvisor *)seekingAdvisor
{    
    [_attributesTagsView setTags:[seekingAdvisor attributeHashTags]];
    [_attributesTagsView reload];
    
    [_guruTagsView setTags:[[NSMutableArray alloc] initWithArray:[seekingAdvisor guruTags]]];
    [_guruTagsView reload];
    
    [_lblQuestionText setText:[seekingAdvisor question]];
    
    if ([[_guruTagsView tags] count] != 0 || [[_attributesTagsView tags] count] != 0) //Please dont ask me why i did that. :'(
    {
        [_heightForAttributesTagView setConstant:[_heightForAttributesTagView constant] + 50];
        [_heightForContainerView setConstant:[_heightForContainerView constant] + [_guruTagsView frame].size.height + [_heightForAttributesTagView constant] - 64];
        
        [self setNeedsLayout];
        [self layoutIfNeeded];
    }
}

#pragma mark - Helper Methods

- (void)showAdvisorCountForConfirmation:(NSString *)advisorCount
{
    [Alert show:TITLE_ALERT_CONFIRMATION andMessage:[NSString stringWithFormat:MESSAGE_ALERT_CONFIRMATION, advisorCount] cancelButtonTitle:@"Continue" otherButtonTitles:@"Cancel" andDelegate:self];
}

#pragma mark - IBActions

- (IBAction)submitQuestion:(id)sender
{
//    [(SubmitQuestionController *)self.controller getAdvisorCountForAskQuestion];
    [(SubmitQuestionController *)self.controller askQuestion];
}

#pragma mark - UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex)
    {
        return;
    }
    
    [(SubmitQuestionController *)self.controller askQuestion];
}

@end
