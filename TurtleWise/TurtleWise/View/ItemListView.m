//
//  ItemListView.m
//  TurtleWise
//
//  Created by Vijay Bhaskar on 08/07/18.
//  Copyright © 2018 Waaleed Khan. All rights reserved.
//

#import "ItemListView.h"
#import "ItemViewCell.h"

#define kCellSelectedColor [UIColor colorWithRed:141.0f/255.0f green:181.0f/255.0f blue:87.0f/255.0f alpha:1.0f]
@implementation ItemListView

- (void)awakeFromNib{
    [super awakeFromNib];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ItemViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"Cell"];
    array = [NSMutableArray array];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.list.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ItemViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.label.text = self.list[indexPath.row][@"title"];
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = kCellSelectedColor;
    cell.selectedBackgroundView = view;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ItemViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [array addObject:cell.label.text];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ItemViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [array removeObject:cell.label.text];
}

-(IBAction)done:(id)sender{
    [self.delegate selectedItem:array];
    
}


@end
