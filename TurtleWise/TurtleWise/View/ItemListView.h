//
//  ItemListView.h
//  TurtleWise
//
//  Created by Vijay Bhaskar on 08/07/18.
//  Copyright © 2018 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectedItemDelegate

-(void)selectedItem:(NSArray*)selectedList;

@end

@interface ItemListView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *array;
}

@property(nonatomic,weak) id<SelectedItemDelegate> delegate;
@property(nonatomic,strong) NSArray *list;
@property(nonatomic,strong) IBOutlet UILabel *header;
@property(nonatomic,strong) IBOutlet UITableView *tableView;

@end
