//
//  LandingView.m
//  TurtleWise
//
//  Created by Anum Amin on 3/14/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "LandingView.h"
#import "LandingController.h"
#import "SocialLoginController.h"

#define IS_IPHONE_6 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON)
#define IS_IPHONE_6_PLUS (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)736) < DBL_EPSILON)
#define IS_PHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

@implementation LandingView

-(BOOL)iPhone6PlusDevice{
    if (!IS_PHONE) return NO;
    if ([UIScreen mainScreen].scale > 2.9) return YES;   // Scale is only 3 when not in scaled mode for iPhone 6 Plus
    return NO;
}

-(BOOL) iPhone6PlusUnZoomed{
    if ([self iPhone6PlusDevice]){
        if ([UIScreen mainScreen].bounds.size.height > 720.0) return YES;  // Height is 736, but 667 when zoomed.
    }
    return NO;
}

#pragma mark - Action

-(void) awakeFromNib {
    [super awakeFromNib];
    _btnNext.layer.cornerRadius = floor(_btnNext.frame.size.height/4);
    _btnNext.clipsToBounds = YES;
    
    if(IS_IPHONE_6 || IS_IPHONE_6_PLUS || [self iPhone6PlusDevice] || [self iPhone6PlusUnZoomed]) {
        _txtWelcome.font = [UIFont fontWithName:@"Helvetica" size:17];
    }
}

- (IBAction)onNextButton:(id)sender {
    [self.navigationController pushViewController:[SocialLoginController new] animated:YES];
  //  [(LandingController *)self.controller onNavigationToLandingVideoController];
}

@end
