//
//  SelectAdvisorTypeView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/2/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "AdvisorBasicInfoController.h"
#import "ActionSheetDatePicker.h"
#import "AdvisorBasicInfoView.h"
#import "SeekingAdvisor.h"
#import "NMRangeSlider.h"
#import "StringUtils.h"
#import "TextField.h"
#import "DateUtils.h"
#import "Constant.h"
#import "Color.h"

#define GENDER_TAG_MALE @"male"
#define GENDER_TAG_FEMALE @"female"
#define GENDER_TAG_BOTH @"both"

#define GENDER_BUTTONS_START_TAG 100
#define GENDER_BUTTONS_END_TAG 102

#define ADVISOR_TYPE_BUTTONS_START_TAG 200
#define ADVISOR_TYPE_BUTTONS_END_TAG 202

#define ADVISOR_MIN_AGE_RESTRICTION 13.0
#define ADVISOR_MAX_AGE_RESTRICTION 120.0

@interface AdvisorBasicInfoView ()<UIGestureRecognizerDelegate>

@property(nonatomic, strong) NSString *genderTag;
@property(nonatomic, strong) NSString *advisroTypeTag;
@property(nonatomic, strong) NSString *ageRestrictionTag;

@end

@implementation AdvisorBasicInfoView

#pragma mark - UIView Life Cycle

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self setupUI];
}

- (void)setupUI
{
    _ageRestrictionSlider.minimumValue = ADVISOR_MIN_AGE_RESTRICTION;
    _ageRestrictionSlider.maximumValue = ADVISOR_MAX_AGE_RESTRICTION;

    _ageRestrictionSlider.minimumRange = 0;
    _ageRestrictionSlider.tintColor = [Color lightGreenColor];
}


#pragma mark - Override Methods

- (void)updateData:(SeekingAdvisor *)profile
{
    [self updateGender:[profile gender]];
    [self updateAdvisorType:[profile searchOptions]];
    [self updateAgeSliderWithMinValue:[[profile minAge] floatValue] andMaxValue:[[profile maxAge] floatValue]];
}

- (void)saveWizardData:(SeekingAdvisor *)profile
{
    [profile setGender:_genderTag];
    [profile setSearchOptions:_advisroTypeTag];
    
    
    if ([_ageRestrictionSlider  lowerValue] == ADVISOR_MIN_AGE_RESTRICTION && [_ageRestrictionSlider upperValue] == ADVISOR_MAX_AGE_RESTRICTION)
    {
        [profile setMinAge:@""];
        [profile setMaxAge:@""];
        
        return;
    }
    
    [profile setMinAge:[NSString stringWithFormat:@"%0.f", [_ageRestrictionSlider lowerValue]]];
    [profile setMaxAge:[NSString stringWithFormat:@"%0.f", [_ageRestrictionSlider upperValue]]];
}

#pragma mark - Helper Methods

- (void)updateAgeSliderWithMinValue:(float)minimumValue andMaxValue:(float)maximumValue
{
    if (minimumValue == 0.0)
    {
        minimumValue = ADVISOR_MIN_AGE_RESTRICTION;
    }
    
    if (maximumValue == 0.0)
    {
        maximumValue = ADVISOR_MAX_AGE_RESTRICTION;
    }
    
    [_ageRestrictionSlider setLowerValue:minimumValue];
    [_ageRestrictionSlider setUpperValue:maximumValue];
    
    if (maximumValue == minimumValue)
    {
        [_lblAgeRestriction setText:[NSString stringWithFormat:@"%0.f", minimumValue]];
        
        return;
    }
    
    [_lblAgeRestriction setText:[NSString stringWithFormat:@"%0.f-%0.f", minimumValue, maximumValue]];
}


- (void)updateGender:(NSString *)gender
{
    _genderTag = gender;
    
    if ([StringUtils compareStringIgnorCase:gender withString:GENDER_TAG_MALE])
    {
        [(UIButton *)[self viewWithTag:GENDER_BUTTONS_START_TAG] setSelected:YES];
    }
    
    if ([StringUtils compareStringIgnorCase:gender withString:GENDER_TAG_FEMALE])
    {
        [(UIButton *)[self viewWithTag:GENDER_BUTTONS_START_TAG + 1] setSelected:YES];
    }
    
    if ([StringUtils compareStringIgnorCase:gender withString:GENDER_TAG_BOTH])
    {
        [(UIButton *)[self viewWithTag:GENDER_BUTTONS_END_TAG] setSelected:YES];
    }
}

- (void)updateAdvisorType:(NSString *)advisorType
{
    _advisroTypeTag = advisorType;
    
    for (int i = ADVISOR_TYPE_BUTTONS_START_TAG; i <= ADVISOR_TYPE_BUTTONS_END_TAG; i++)
    {
        UIButton *button = (UIButton *)[self viewWithTag:i];
        
        if ([StringUtils compareString:_advisroTypeTag withString:[[button titleLabel] text]])
        {
            [button setSelected:YES];
        }
    }
}

#pragma mark - IBActions

- (IBAction)submitWizard:(id)sender
{
    [self completeWizard];
}

- (IBAction)advisorTypeSelection:(id)sender
{
    for (int i = ADVISOR_TYPE_BUTTONS_START_TAG; i <= ADVISOR_TYPE_BUTTONS_END_TAG; i++)
    {
        if (i == [sender tag])
        {
            [sender setSelected:![sender isSelected]];
            
            if ([sender isSelected])
            {
                _advisroTypeTag = [[sender titleLabel] text];
            }
            
            continue;
        }
        
        UIButton *unselectedButtons = (UIButton *)[self viewWithTag:i];
        [unselectedButtons setSelected:NO];
    }
}

- (IBAction)genderSelection:(id)sender
{
    NSArray *possibleGenderTags = @[GENDER_TAG_MALE, GENDER_TAG_FEMALE, GENDER_TAG_BOTH];
    
    for (int i = GENDER_BUTTONS_START_TAG; i <= GENDER_BUTTONS_END_TAG; i++)
    {
        if (i == [sender tag])
        {
            [sender setSelected:![sender isSelected]];
            
            if ([sender isSelected])
            {
                int tagIndex = [sender tag] % GENDER_BUTTONS_START_TAG;
                
                _genderTag = [possibleGenderTags objectAtIndex:tagIndex];
                continue;
            }
            _genderTag = nil;
        }
        
        UIButton *unselectedButtons = (UIButton *)[self viewWithTag:i];
        [unselectedButtons setSelected:NO];
    }
}

- (IBAction)changeAgeLimits
{
    [self updateAgeSliderWithMinValue:[_ageRestrictionSlider lowerValue] andMaxValue:[_ageRestrictionSlider upperValue]];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isKindOfClass:[NMRangeSlider class]])
    {
        return NO;
    }
    return YES;
}
@end
