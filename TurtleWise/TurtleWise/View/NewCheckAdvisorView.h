//
//  NewCheckAdvisorView.h
//  TurtleWise
//
//  Created by Ajdal on 1/15/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"

@class Question;
@class TabbarView;
@class Advisor;

@interface NewCheckAdvisorView : BaseView<UITableViewDataSource,UITableViewDelegate>

@property(weak,nonatomic) IBOutlet TabbarView *tabbar;
@property(weak,nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic,weak) IBOutlet UILabel *errorLabel;
@property(nonatomic,weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@property(nonatomic,weak) IBOutlet NSLayoutConstraint *tableViewTopMargin;
@property(nonatomic,weak) IBOutlet UILabel *answerOptionDetail;
@property(nonatomic,weak) IBOutlet UIView *answerOptionDetailView;

#pragma mark - Answer Choice Buttons
@property(nonatomic,strong) IBOutletCollection(UIButton) NSArray *answerOptionButtonsTwoChoiceQuestion;
@property(nonatomic,strong) IBOutletCollection(UIButton) NSArray *answerOptionButtonsThreeChoiceQuestion;
@property(nonatomic,strong) IBOutletCollection(UIButton) NSArray *answerOptionButtonsFourChoiceQuestion;

#pragma mark - Answer Choice Buttons View
@property(nonatomic,strong) IBOutlet UIView *twoChoiceQuestionView;
@property(nonatomic,strong) IBOutlet UIView *threeChoiceQuestionView;
@property(nonatomic,strong) IBOutlet UIView *fourChoiceQuestionView;

@property(nonatomic,weak) IBOutlet UIView *buttonsHolder;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *buttonsHolderHeightConstraint;

@property(nonatomic,weak) IBOutlet UIBarButtonItem *sortByButton;

#pragma mark - Actions
-(IBAction)showSortOptions;
-(IBAction)onAnswerChoiceBtnSelection:(id)sender;

#pragma mark - Methods
-(void)selectTab:(NSInteger)tag;
-(void)showCloseAlert;
-(void)setQuestion:(Question*)question;
-(void)setAnswers:(NSArray*)answers;
-(void)showError:(NSString*)errorString;
-(NSArray*)getSelectedAdvisors;
-(NSInteger)getSelectedTab;
-(void)addAdvisorToChatPanel:(Advisor *)advisor;
-(void)removeAdvisorFromChatPanel:(Advisor*)advisor;
- (void)updateAvailableForChat:(BOOL)selected atIndexPath:(NSIndexPath *)indexPath;

@end
