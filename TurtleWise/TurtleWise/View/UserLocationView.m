//
//  UserLocationView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/7/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActionSheetStringPicker.h"
#import "UserLocationController.h"
#import "UserLocationView.h"
#import "UserProfile.h"
#import "TextField.h"
#import "Constant.h"
#import "Button.h"

#define PICKER_TITLE_COUNTRY    @"Country"
#define PICKER_TITLE_STATE      @"State"

@interface UserLocationView ()

@property(nonatomic, readonly) UserLocationController *controller;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stateHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *statePickerView;

@end

@implementation UserLocationView

@dynamic controller;

#pragma mark - IBActions

- (IBAction)submitWizard:(id)sender
{
    [self completeWizard];
}

- (IBAction)openCountryPicker:(Button *)sender
{
    [self endEditing:YES];
    
    NSArray * arrOfCountries = [(UserLocationController *)self.controller getAllCountries];
    [ActionSheetStringPicker showPickerWithTitle:PICKER_TITLE_COUNTRY
                                            rows:arrOfCountries
                                initialSelection:[(UserLocationController *)self.controller getIndexForCountry:[_btnCountry titleForState:UIControlStateNormal]]
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue)
     {
         NSString * nameOfCountry = [arrOfCountries objectAtIndex:selectedIndex];
         
         [self setCountry:nameOfCountry];
         [self setEnableStateButton:[(UserLocationController *)self.controller shouldEnableStatesForCountry:nameOfCountry]];
     }
                                     cancelBlock:NULL
                                          origin:self];
}

- (void)setEnableStateButton:(BOOL)enable
{
    [_btnState setEnabled:enable];
    [self setState:@""];
    
    [UIView animateWithDuration:0.2 animations:
     ^{
         CGRect frame = _statePickerView.frame;
         (enable)? (frame.size.height = 155): (frame.size.height = 0);
         [_statePickerView setFrame:frame];
         [_stateHeightConstraint setConstant:frame.size.height];
     }];
}

- (IBAction)openStatePicker:(Button *)sender
{
    [self endEditing:YES];
    
    NSArray * arrOfStates = [(UserLocationController *)self.controller getAllStates];
    [ActionSheetStringPicker showPickerWithTitle:PICKER_TITLE_STATE
                                            rows:arrOfStates
                                initialSelection:[(UserLocationController *)self.controller getIndexForState:[_btnState titleForState:UIControlStateNormal]]
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue)
     {
         [self setState:[arrOfStates objectAtIndex:selectedIndex]];
     }
                                     cancelBlock:NULL
                                          origin:self];
}

- (void)setCountry:(NSString *)nameOfCountry
{
    [_btnCountry setTitle:nameOfCountry forState:UIControlStateNormal];
}

- (void)setState:(NSString *)nameOfState
{
    [_btnState setTitle:nameOfState forState:UIControlStateNormal];
}

#pragma mark - Override Methods

- (void)updateData:(UserProfile *)profile
{
    [_txtFieldCity setText:[profile city]];
    
    NSString * nameOfCountry = [profile country];
    [_btnCountry setTitle:nameOfCountry forState:UIControlStateNormal];
    
    if ([(UserLocationController *)self.controller shouldEnableStatesForCountry:nameOfCountry])
    {
        [_btnState setTitle:[profile state] forState:UIControlStateNormal];
    }
    else
    {
        [self setEnableStateButton:NO];
    }
}

- (void)saveWizardData:(UserProfile *)profile
{
    [profile setCity:[_txtFieldCity text]];
    
    [profile setCountry:[_btnCountry titleForState:UIControlStateNormal]];
    [profile setState:[_btnState titleForState:UIControlStateNormal]];
}

@end
