//
//  ThankYouView.h
//  TurtleWise
//
//  Created by redblink on 6/6/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardView.h"


@interface ThankYouView : BaseProfileWizardView

//Actions
- (IBAction)submitWizard:(id)sender;

@end
