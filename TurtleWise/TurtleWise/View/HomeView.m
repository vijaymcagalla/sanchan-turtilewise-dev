//
//  HomeView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/4/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "UIAlertController+Blocks.h"
#import "TurtleBucksInfoView.h"
#import "HomeController.h"
#import "UserDefaults.h"
#import "UserProfile.h"
#import "AppDelegate.h"
#import "UserStats.h"
#import "Constant.h"
#import "HomeView.h"
#import "Utility.h"
#import "Label.h"


#define NOTIFICATION_BUBBLE_RADIUS 14.0

#define TITLE_ASK_QUESTION_BUTTON @"ASK A QUESTION"
#define TITLE_ANSWER_QUESTION_BUTTON @"ANSWER A QUESTION"
#define TITLE_RATE_CHATS @"Rate Chats"
#define TITLE_SUBSCRIPTION_EXPIRED @"Subscription Expired!"

#define MESSAGE_RATE_CHATS @"There are %ld Chat(s) that require your ratings"
#define MESSAGE_SUBSCRIPTION_EXPIRED @"Do you want to continue your premium access?"

#define TITLE_BUTTON_RENEW @"Yes"
#define TITLE_BUTTON_NOT_NOW @"No"

#define TITLE_BUTTON_RATE_SEPARATE_CHAT @"Rate Individually."

#define TURTLE_STAGE_FREE @"free"
#define TITLE_STAGE_FREE @"Turtle Points"
#define TITLE_STAGE_PAID @"Turtle Bucks"
#define FORMATTED_NUMBER @"%ld"
#define PROFILE_IMAGE @"user-placeholder"


@interface HomeView()

@property(nonatomic, assign) UserRole userRole;
@property(nonatomic, strong) UserStats *userStats;
@property(nonatomic, strong) HomeController *controller;

- (void)callBacksOnAlertButtonTap:(NSUInteger)buttonIndex;
- (void)updateNotificationValues;
- (void)setNotificationCountOn:(UILabel *)lblBubble withValue:(long)bubbleValue;

@end

@implementation HomeView
@dynamic controller;

#pragma mark -  UIView Life Cycle

- (void)setupUI
{
    [[_profileImage layer] setCornerRadius:CGRectGetWidth([_profileImage frame])/2.0f];
    [_profileImage setClipsToBounds:YES];
    
    [[_btnAskQuestion layer] setCornerRadius:CGRectGetHeight([_btnAskQuestion frame])/2.0f];
    [[_btnAskQuestion layer] setBorderWidth:1.5];
    [[_btnAskQuestion layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    [self setupNotificationBubbles];
}

- (void)viewDidLoad {
    [self updateUserStats:[UserDefaults getUserProfile]];
    
}

- (void)viewWillAppear
{
    _userRole = [UserDefaults getUserRole];
    
    [self updateViewForUserRoleChange:_userRole];
    //[self updateUserStats:[UserDefaults getUserProfile]];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self layoutIfNeeded];
    
    [self setupUI];
}

- (void)showQuestionList
{
    [[self controller] showQuestionListingController];
}

- (void)callBacksOnAlertButtonTap:(NSUInteger)buttonIndex
{
    [UserDefaults setShouldCheckForUnratedChats:NO];
    [[self controller] askForUnratedChat:[_userStats unratedChats]];
}

- (void)markUserPremium
{
    [_btnUserMode setEnabled:NO];
}

#pragma mark - Override Methods

- (BOOL)hasTabBar
{
    return YES;
}

- (void)updateStatsOnTabBar:(UserStats *)userStats
{
    _userStats = userStats;
    
    [[self controller] setShouldShowChatAlert:YES];
    [self updateNotificationValues];
}

- (void)checkForUnratedChats
{
    if ([UserDefaults shouldCheckForUnratedChats] && [_userStats unratedChats] > 0)
    {        
        //Switch Role to Seeker;
        [self updateViewForUserRoleChange:UserRoleSeeker];
        
        //Show Alert For Un rated Chats
        [UIAlertController showAlertInViewController:[self controller]
                                           withTitle:TITLE_RATE_CHATS
                                             message:[NSString stringWithFormat:MESSAGE_RATE_CHATS, [_userStats unratedChats]]
                                   cancelButtonTitle:NULL
                              destructiveButtonTitle:NULL
                                   otherButtonTitles:@[TITLE_BUTTON_RATE_SEPARATE_CHAT]
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex) {
                                                [self callBacksOnAlertButtonTap:buttonIndex];
        }];
    }
}

#pragma mark - Notificaiton Bubble Work

-(void)setupNotificationBubbles
{
    [[_questionBubble layer] setCornerRadius:NOTIFICATION_BUBBLE_RADIUS];
    [[_answerBubble layer] setCornerRadius:NOTIFICATION_BUBBLE_RADIUS];
    [[_chatBubble layer] setCornerRadius:NOTIFICATION_BUBBLE_RADIUS];
    
    [_questionBubble setClipsToBounds:YES];
    [_answerBubble setClipsToBounds:YES];
    [_chatBubble setClipsToBounds:YES];
}

- (void)updateNotificationValues
{
    long questionValue = [_userStats advisorQuestions];
    long answerValue = 0;
    
    if (_userRole == UserRoleSeeker)
    {
        questionValue = [_userStats seekerQuestions];
        answerValue = [_userStats seekerAnswers];
    }
    
    [self setNotificationCountOn:_questionBubble withValue:questionValue];
    [self setNotificationCountOn:_answerBubble withValue:answerValue];
    [self setNotificationCountOn:_chatBubble withValue:[_userStats unreadChats]];
}

- (void)setNotificationCountOn:(UILabel *)lblBubble withValue:(long)bubbleValue
{
    if(bubbleValue == 0)
    {
        [lblBubble setHidden:YES];
        
        return;
    }
    
    [lblBubble setHidden:NO];
    [lblBubble setText:[NSString stringWithFormat:@"%ld", bubbleValue]];
}


#pragma mark - IBActions

- (IBAction)askQuestion:(id)sender
{
    (_userRole == UserRoleAdvisor) ? [self showQuestionList] : [[self controller] proceedToAskQuestion];
}

- (IBAction)changeUserRole:(id)sender
{
    [self updateViewForUserRoleChange:![sender isSelected]];
}

- (IBAction)showQuestionList:(id)sender
{
    [self showQuestionList];
}

- (IBAction)showAnswerList:(id)sender
{
    [[self controller] showAnswerListingController];
}

- (IBAction)showChatList:(id)sender
{
   [[self controller] showChatListingController];
}
- (IBAction)showTurtleBucksInfoView:(id)sender
{
    CGRect frame = [UIScreen mainScreen].bounds;
    TurtleBucksInfoView *infoView = [[TurtleBucksInfoView alloc] initWithFrame:frame];
    [[APP_DELEGATE window] addSubview:infoView];
}

- (IBAction)showSubscriptionModel:(id)sender
{
    [self.controller showSubscriptionModel];
}

#pragma mark - Public/Private Methods

- (void)updateViewForUserRoleChange:(UserRole)userRole
{
    _userRole = userRole;
    
    [_btnUserRoleChange setSelected:userRole];
    
    [_btnAskQuestion setTitle:(userRole) ? TITLE_ANSWER_QUESTION_BUTTON : TITLE_ASK_QUESTION_BUTTON
                     forState:UIControlStateNormal];
    
    //Save User Role
    [UserDefaults setUserRole:userRole];

    [self updateNotificationValues];
    
    //Update Side Menu
    [[self controller] updateSlideMenu];
}

- (void)updateUserStats:(UserProfile *)profile
{
    //Info
    [_lblProfileName setText:[profile getDisplayNameOrTurtleIdentifier]];
    [_lblUserLevel setText:[NSString stringWithFormat:LEVEL_PREFIX, (int)profile.level]];
    
    //Photo
    [_profileImage setImage:[profile avatar]];
    
    //Stats
    [_lblThumbsUp setText:[NSString stringWithFormat:FORMATTED_NUMBER, (long)[profile thumbsUps]]];
    [_lblThumbsDown setText:[NSString stringWithFormat:FORMATTED_NUMBER, (long)[profile thumbsDown]]];
    [_lblThanks setText:[NSString stringWithFormat:FORMATTED_NUMBER, (long)[profile thanks]]];
    
    [_lblTurtleBucks setText:[NSString stringWithFormat:FORMATTED_NUMBER, (long)[profile bucks]]];
    [_lblTurtlePoints setText:[NSString stringWithFormat:FORMATTED_NUMBER, (long)[profile points]]];
    [_btnUserMode setEnabled:![profile isUserPremium]];
}

- (void)updateViewForUserProfile:(UserProfile *)profile
{
    [self updateUserStats:profile];
    
    if([APP_DELEGATE showCheckForSubscriptionExpiray] && ![profile isUserPremium] && [[UserDefaults getLastLogin] compare:[profile subscriptionExpiray]] == NSOrderedAscending)
    {
        [APP_DELEGATE setShowCheckForSubscriptionExpiray:NO];
        
        [self showSubscriptionEndedAlert];
    }
}

- (void)subscriptionExpiredAlertButtonTapped:(NSInteger)buttonIndex
{
    if (buttonIndex == 2)
    {
        [self.controller showSubscriptionModel];
    }
}

- (void)showSubscriptionEndedAlert
{
    [_btnUserMode setEnabled:YES];
    
    [UIAlertController showAlertInViewController:[self controller]
                                       withTitle:TITLE_SUBSCRIPTION_EXPIRED
                                         message:MESSAGE_SUBSCRIPTION_EXPIRED
                               cancelButtonTitle:NULL
                          destructiveButtonTitle:NULL
                               otherButtonTitles:@[TITLE_BUTTON_RENEW, TITLE_BUTTON_NOT_NOW]
                                        tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex) {
                                            [self subscriptionExpiredAlertButtonTapped:buttonIndex];
                                        }];
}

@end
