//
//  SelectEducationLevelView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/11/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseProfileWizardView.h"

@interface SelectEducationLevelView : BaseProfileWizardView

- (IBAction)selectEducationLevel:(id)sender;
- (IBAction)submitWizard:(id)sender;

@end
