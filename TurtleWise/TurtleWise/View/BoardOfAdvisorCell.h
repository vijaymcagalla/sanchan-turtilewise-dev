//
//  BoardOfAdvisorCell.h
//  TurtleWise
//
//  Created by Vijay on 16/11/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BoardOfAdvisorCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *answeredLabel;
@property (weak, nonatomic) IBOutlet UILabel *thumbsUp;
@property (weak, nonatomic) IBOutlet UILabel *thumbsDown;

@end
