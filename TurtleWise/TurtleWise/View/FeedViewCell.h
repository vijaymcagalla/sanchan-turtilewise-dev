//
//  FeedViewCell.h
//  TurtleWise
//
//  Created by Vijay Bhaskar on 15/03/18.
//  Copyright © 2018 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedViewCell : UITableViewCell

@property(nonatomic,weak) IBOutlet UIImageView *iconImageView;
@property(nonatomic,weak) IBOutlet UILabel *titleLabel;
@property(nonatomic,weak) IBOutlet UILabel *descriptionLabel;

@end
