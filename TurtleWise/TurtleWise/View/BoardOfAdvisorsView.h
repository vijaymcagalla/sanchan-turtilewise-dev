//
//  BoardOfAdvisorsView.h
//  TurtleWise
//
//  Created by Vijay Bhaskar on 12/11/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"

@interface BoardOfAdvisorsView : BaseView<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *listofAdvisorsView;
@property (strong, nonatomic) NSArray *listofAdvisors;

-(void)getAdvisorsList;

@end
