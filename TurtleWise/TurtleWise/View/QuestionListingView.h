//
//  SeekerView.h
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"

@class TabbarView;

@interface QuestionListingView : BaseView

@property(nonatomic,weak) IBOutlet UILabel *errorLabel;
@property(nonatomic,weak) IBOutlet UIToolbar *toolbar;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *toolbarBottomMargin;
@property(nonatomic,weak) IBOutlet UIBarButtonItem *deleteBarBtn;
@property(nonatomic,weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property(nonatomic, weak) IBOutlet TabbarView *tabBar;
@property(nonatomic, weak) IBOutlet UITableView *tableView;

-(void)populateTableView:(NSArray *)array;
-(void)addTableFooterWithActivityIndicator;
-(void)hideTableFooter;
-(void)onEditBtnTap;
-(void)closeQuestionSuccess;
-(void)deleteQuestionsSuccess;
-(void)didSelectTabBarBtn:(TabbarViewType)tabViewType;
@end
