//
//  UnsubscribeEmailSettingsView.h
//  TurtleWise
//
//  Created by Usman Asif on 11/15/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"

@interface UnsubscribeEmailSettingsView : BaseView

- (void)updateDoneButtonTitle:(BOOL)isLoggedIn;
@end
