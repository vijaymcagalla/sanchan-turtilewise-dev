//
//  MCQOptionsView.m
//  TurtleWise
//
//  Created by Waleed Khan on 2/2/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "MCQOptionsController.h"
#import "MCQOptionsView.h"
#import "TWTableView.h"

#define MESSAGE_MIN_CHOICES @"Must have atleast 2 MCQ Options."
#define MESSAGE_UNIQUE_CHOICES @"MCQ Options must be Unique."
#define MESSAGE_CHOICE_TEXT_LIMIT @"MCQ Option text should be between 2 to 160 characters."

@interface MCQOptionsView ()

@property(nonatomic, strong) MCQOptionsController *controller;

- (BOOL)areChoicesInLimit;
- (BOOL)areChoicesUnique;

@end

@implementation MCQOptionsView
@dynamic controller;

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_tableView setMinRowsVisible:2];
    [_tableView setMaxRowsAllowed:4];
    [_tableView setAddMoreButton:_btnAddMore];
    
    [_tableView prepareDataSet:[NSArray new]];
}

- (NSArray *)getChoices
{
    return [_tableView getDataSetAsArray];
}

- (void)clearOptions
{
    [_tableView reset];
}

#pragma mark - Validations

- (BOOL)areChoicesInLimit
{
    NSArray *choices = [self getChoices];
    
    if ([choices count] < 2)
    {
        [Alert show:@"Error!" andMessage:MESSAGE_MIN_CHOICES];
        
        return NO;
    }

    for (NSString *choice in choices)
    {
        if ([choice length] < 2 || [choice length] > 160)
        {
            [Alert show:@"Error!" andMessage:MESSAGE_CHOICE_TEXT_LIMIT];
            
            return NO;
        }
    }
    return YES;
}

- (BOOL)areChoicesUnique
{
    NSCountedSet *countedSet = [[NSCountedSet alloc] initWithArray:[self getChoices]];
    
    for (NSString *choice in [self getChoices])
    {
        if([countedSet countForObject:choice] > 1)
        {
            [Alert show:@"Error!" andMessage:MESSAGE_UNIQUE_CHOICES];
            
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - IBActions

- (IBAction)submitChoices:(id)sender
{
    if (![self areChoicesInLimit])
    {
        return;
    }
    
    if (![self areChoicesUnique])
    {
        return;
    }
    
    [[self controller] backToAskQuestionWithChoices:[self getChoices]];
}

- (IBAction)addMore:(id)sender
{
    [_tableView addRow];
}


@end
