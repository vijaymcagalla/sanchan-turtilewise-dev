//
//  TurtleswagView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/23/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "TurtleswagController.h"
//#import "AsyncImageView.h"
#import "TurtleswagCell.h"
#import "TurtleswagView.h"
#import "UserDefaults.h"
#import "UserProfile.h"
#import "Constant.h"
#import "Utility.h"
#import "Charity.h"
#import "Font.h"

#define BOLD_TEXT @"80"
#define TABLE_CELL_HEIGHT 170.0

#define CELL_NIB_NAME @"TurtleswagCell"

#define HEADING_TURTLE_SWAG @"Amazon Gift Card"
#define HEADING_DONATE @"Donate to a Charity"

@interface TurtleswagView ()

- (void)setupTableView;
- (void)setAttributedTextOnHeading;

@end

@implementation TurtleswagView

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    [self setupTableView];
    [self setAttributedTextOnHeading];
}

- (void)setupTableView
{
    [_turtleswagList registerNib:[UINib nibWithNibName:CELL_NIB_NAME bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CELL_REUSE_IDENTIFIER];
    [_turtleswagList setContentInset:UIEdgeInsetsMake(0, 0, 10, 0)];
}

- (void)setAttributedTextOnHeading
{
    NSDictionary *attributes = @{
                                 NSFontAttributeName            : [Font lightFontWithSize:15],
                                 NSForegroundColorAttributeName : [_lblHeading textColor]
                                 };
    
    NSDictionary *boldAttributes = @{
                                     NSFontAttributeName            : [Font mediumFontWithSize:15],
                                     NSForegroundColorAttributeName : [UIColor blackColor]
                                     };
    NSInteger bucks = [[UserDefaults getUserProfile] bucks];
    
    [_lblHeading setAttributedText:[Utility applyAttributes:boldAttributes
                                                onSubstring:[NSString stringWithFormat:@"%ld", (long)bucks]
                                           withParentString:[NSString stringWithFormat:@"You have %ld TurtleBucks", (long)bucks]
                                              andAttributes:attributes]];
}

- (void)populateCharityList:(NSArray *)charityList andGiftCard:(NSArray *)giftCardList
{
    _beneficiaryList = charityList;
    _catalogItems = giftCardList;
    
    [_turtleswagList reloadData];
}

#pragma mark - UITableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (section == 0) ? [_catalogItems count] : [_beneficiaryList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    TurtleswagCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_REUSE_IDENTIFIER];
    
    [cell prepareForReuse];
    
    Charity *catalogItem =  ([indexPath section] == 0) ? [_catalogItems objectAtIndex:[indexPath row]] :
                                                         [_beneficiaryList objectAtIndex:[indexPath row]];
    [cell set:catalogItem];

    return cell;
}

#pragma mark - UITableView Delegate Methods

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [UIView new];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 10, [self bounds].size.width, 41)];
    
    [label setText:(section == 0) ? HEADING_TURTLE_SWAG : HEADING_DONATE];
    [label setFont:[Font lightFontWithSize:31.0]];
    [label setTextColor:[UIColor darkGrayColor]];
    
    [headerView setBackgroundColor:[UIColor whiteColor]];
    [headerView addSubview:label];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TABLE_CELL_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 80.0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Charity *catalogItem =  ([indexPath section] == 0) ? [_catalogItems objectAtIndex:[indexPath row]] :
                                                         [_beneficiaryList objectAtIndex:[indexPath row]];
    
    [(TurtleswagController *)self.controller showTurtleSwagDetails:catalogItem];
    
    //deselect row
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
