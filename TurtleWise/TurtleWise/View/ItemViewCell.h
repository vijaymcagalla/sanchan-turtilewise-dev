//
//  ItemViewCell.h
//  TurtleWise
//
//  Created by Vijay Bhaskar on 08/07/18.
//  Copyright © 2018 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemViewCell : UITableViewCell

@property(nonatomic,strong)IBOutlet UILabel *label;

@end
