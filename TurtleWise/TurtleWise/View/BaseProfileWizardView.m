//
//  BaseProfileWizardView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/15/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardController.h"
#import "BaseProfileWizardView.h"
#import "TWWizardManager.h"

@implementation BaseProfileWizardView

#pragma mark - Life Cycle Methods

-(void)viewDidLoad
{
    [self setWizardSwipeGestures];
}

 - (void)viewWillAppear
{
    [self updateData:[[(BaseProfileWizardController *)self.controller profileWizardManager] profile]];
}

#pragma mark - Wizard Handling

- (void)completeWizard
{
    [self saveWizardData:[[(BaseProfileWizardController *)self.controller profileWizardManager] profile]];
    [(BaseProfileWizardController *)self.controller completeWizard];
}

#pragma mark - Save/Update Wizard Data on View

- (void)saveWizardData:(BaseProfile *)profile
{
    //Will be Implemented by the Sub Class
}

- (void)updateData:(BaseProfile *)profile
{
    //Will Be Implemented by the Sub Class
}

- (void)addPage {
    [self saveWizardDataPage:[[(BaseProfileWizardController *)self.controller profileWizardManager] createBrandedPage]];
    [(BaseProfileWizardController *)self.controller completeWizard];
}

- (void)saveWizardDataPage:(NSMutableDictionary *)page
{
    //Will be Implemented by only branded page class
}

@end
