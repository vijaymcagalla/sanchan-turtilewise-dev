//
//  FollowersView.m
//  TurtleWise
//
//  Created by Sunflower on 9/7/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "FollowersView.h"
#import "FollowersCell.h"
#import "UserDefaults.h"
#import "StringUtils.h"
#import "ManageBrandedPageView.h"

@interface FollowersView (){
    NSArray *followers;
}
@end

@implementation FollowersView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        UINib *nib = [UINib nibWithNibName:@"FollowersView" bundle:nil];
        NSArray *array = [nib instantiateWithOwner:nil options:nil];
        self = [array lastObject];
        self.frame = frame;
    }
    return self;
}

- (void) getData:(NSArray*)req {
    followers = req;
    [self setupTableView];
}

-(void)setupTableView{
    [self.followersTableView setEstimatedRowHeight:93.0];
    [self.followersTableView setRowHeight:UITableViewAutomaticDimension];
    [self.followersTableView registerNib:[UINib nibWithNibName:[FollowersCell cellName]bundle:[NSBundle mainBundle]] forCellReuseIdentifier:[FollowersCell cellName]];
    [_followersTableView reloadData];
}

#pragma mark - UITableView Delegate/DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return followers.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FollowersCell *cell = [tableView dequeueReusableCellWithIdentifier:[FollowersCell cellName]];
    NSDictionary* attributes = [ParserUtils object:[followers objectAtIndex:indexPath.row] key:KEY_ATTRIBUTES];
    NSString *firstName = [StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_FIRST_NAME]];
    NSString *lastName = [StringUtils sentenceCapitalizedString:[ParserUtils stringValue:attributes key:KEY_LAST_NAME]];
    
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    [cell set:fullName];
    
    cell.removeButton.tag = (NSInteger)[ParserUtils stringValue:[followers objectAtIndex:indexPath.row] key:KEY_USER_ID];
    
    [cell.removeButton addTarget:(ManageBrandedPageView*)self.superview action:NSSelectorFromString(@"removeButtonTapped:") forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 5)];
    header.backgroundColor = [UIColor whiteColor];
    return header;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (IBAction)closeTapped:(id)sender {
    [self removeFromSuperview];
}

@end
