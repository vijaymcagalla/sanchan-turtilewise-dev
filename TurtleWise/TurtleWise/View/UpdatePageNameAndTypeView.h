//
//  UpdatePageNameAndTypeView.h
//  TurtleWise
//
//  Created by Sunflower on 8/25/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"
#import "UpdatePageNameAndTypeController.h"
@class TextField;

@interface UpdatePageNameAndTypeView : BaseView
@property (weak, nonatomic) IBOutlet UIButton *btnInfluencer;
@property (weak, nonatomic) IBOutlet UIButton *btnOrganisation;
@property (weak, nonatomic) IBOutlet TextField *txtName;

- (void)updateData:(BrandedPage *)pageDetail;
- (void)saveWizardData:(BrandedPage *)pageDetail;
@end
