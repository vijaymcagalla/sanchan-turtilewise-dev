//
//  EditProfileView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/17/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "EditProfileController.h"
#import "EditProfileView.h"
#import "EditProfileCell.h"
#import "ProfileCell.h"
#import "UserProfile.h"
#import "Enum.h"

#define CELL_IDENTIFIER @"EditProfileCell"

#define ARRAY_INDEX_GURU_TAGS 19
#define ARRAY_INDEX_EXPLORER_TAGS 20

@interface EditProfileView () < UITableViewDataSource, UITableViewDelegate >

@property(nonatomic, strong) NSArray *arrProfileData;
@property(nonatomic, readonly) EditProfileController *controller;

- (void)setupUI;

@end

@implementation EditProfileView

@dynamic controller;

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    _arrProfileData = [NSArray new];
    
    [self setupUI];
}

- (void)viewWillDisappear
{
    [self updatePrivacy];
}

- (void)setupUI
{
    [[_imgUserProfile layer] setCornerRadius:CGRectGetWidth([_imgUserProfile frame])/2.0f];
    [_imgUserProfile setClipsToBounds:YES];
    
    [_tableView setEstimatedRowHeight:115.0];
    [_tableView setTableHeaderView:_headerView];
    [_tableView setRowHeight:UITableViewAutomaticDimension];
}

#pragma mark - Helper Methods

- (void)setProfile:(UserProfile *)profile
{
    if (!profile)
    {
        return;
    }
    
    [_imgUserProfile setImage:[profile avatar]];
    [_lblFullName setText:[profile fullName]];
    [_lblUserLevel setText:[NSString stringWithFormat:LEVEL_PREFIX,(int)[profile level]]];
    [_switchPrivacy setOn:[profile isProfilePublic] animated:YES];
    [_forPrivacyLabel setSelected:![profile isProfilePublic]];
    
    _arrProfileData = [profile getAsArrayWithEmptyValues:YES];
    
    [_tableView setHidden:NO];
    [_tableView reloadData];
}

- (void)updatePrivacy
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.profileAttributeAccess == %@", @(ProfileAttributeAccessPrivate)];
    NSArray *cells = [_arrProfileData filteredArrayUsingPredicate:predicate];
    NSMutableSet *privateAttributes = [NSMutableSet setWithArray:[cells valueForKeyPath:@"responseKey"]];
    
    [self.controller updatePrivateAttributes:privateAttributes andOverallPrivacy:[_switchPrivacy isOn]];
}

#pragma mark - IBActions

- (IBAction)changePrivacy:(id)sender
{
    BOOL isProfilePublic = [_switchPrivacy isOn];
    
    [_arrProfileData enumerateObjectsUsingBlock:^(ProfileCell *profileCell, NSUInteger idx, BOOL * _Nonnull stop)
    {
        if (idx == ARRAY_INDEX_GURU_TAGS || idx == ARRAY_INDEX_EXPLORER_TAGS)
        {
            return;
        }
        
        ProfileAttributeAccess access = (isProfilePublic) ? ProfileAttributeAccessPublic : ProfileAttributeAccessNotApplicable;
        
        [profileCell setProfileAttributeAccess:access];
    }];
    
    [_tableView reloadData];
    
    [_forPrivacyLabel setSelected:!isProfilePublic];
}

- (IBAction)onClickProfileImage {
    [(EditProfileController *)self.controller startProfileWizardFromStep:0];
}

#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_arrProfileData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EditProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    
    if (!cell)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:CELL_IDENTIFIER owner:self options:nil];
        
        cell = (EditProfileCell *) [topLevelObjects objectAtIndex:0];
    }
    
    [cell enablePrivacyOptions];
    [cell set:[_arrProfileData objectAtIndex:[indexPath row]]];
    
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [(EditProfileController *)self.controller startProfileWizardFromStep:[(ProfileCell *)[_arrProfileData objectAtIndex:[indexPath row]] stepInWizard]];
}

@end
