//
//  ChatRequestView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/22/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "ChatRequestCell.h"
#import "ChatRequestView.h"
#import "TabbarView.h"
#import "Constant.h"
#import "Enum.h"


#define TABLE_CELL_HEIGHT 55.0
#define CELL_NIB_NAME @"ChatRequestCell"

@interface ChatRequestView ()

@property(nonatomic, strong) NSArray *advisors;

- (void)setupTableView;

@end

@implementation ChatRequestView

#pragma mark - UIView Life Cycle

- (void)viewDidLoad
{
    [self setupTableView];
}

- (void)populateAdvisorsList:(NSArray *)advisors
{
    _advisors = [[NSArray alloc] initWithArray:advisors];
    [_chatRequestList reloadData];
}

#pragma mark - Set Up View Methods

- (void)setupTableView
{
    [_chatRequestList registerNib:[UINib nibWithNibName:CELL_NIB_NAME bundle:nil] forCellReuseIdentifier:CELL_REUSE_IDENTIFIER];
}

#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _advisors.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatRequestCell *chatRequestCell = [tableView dequeueReusableCellWithIdentifier:CELL_REUSE_IDENTIFIER];

    [chatRequestCell setData:[_advisors objectAtIndex:indexPath.row]];
    
    return chatRequestCell;
}

#pragma mark - UITableView Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TABLE_CELL_HEIGHT;
}

@end
