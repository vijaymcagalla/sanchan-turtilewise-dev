//
//  AboutMeView.m
//  TurtleWise
//
//  Created by Irfan Gul on 26/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "TWWizardManager.h"
#import "AboutMeController.h"
#import "AboutMeView.h"
#import "UserProfile.h"
#import "Constant.h"

#define QUESTION_TEXT_LIMIT 255

#define VALIDATION_TITLE @"Limit Exceeds!"
#define VALIDATION_MESSAGE @"About me shouldn't exceed 255 characters."

@interface AboutMeView () < UITextViewDelegate >

@property(nonatomic, strong) AboutMeController *controller;
@property(nonatomic, assign) AboutMeViewFlowType flowType;

- (void)saveAboutMeText;

@end

@implementation AboutMeView
@dynamic controller;

#pragma mark - Life Cycle Methods
- (void)viewDidLoad
{
    [self setupUI];
}

- (void)viewWillAppear
{
    [_textViewAboutMe setText:[(UserProfile *)[[[self controller] profileWizardManager] profile] about]];
    _lblCharCount.text = [NSString stringWithFormat:@"%lu/%d char left", QUESTION_TEXT_LIMIT - _textViewAboutMe.text.length, QUESTION_TEXT_LIMIT];
}

- (void)viewWillDisappear
{
    [(UserProfile *)[[[self controller] profileWizardManager] profile] setAbout:[_textViewAboutMe text]];
}

-(void)setupUI
{
    _btnSubmit.layer.cornerRadius = floor(_btnSubmit.frame.size.height/4);
    _btnAttributes.layer.cornerRadius = floor(_btnAttributes.frame.size.height/4);

    _btnSubmit.clipsToBounds = YES;
    _btnAttributes.clipsToBounds = YES;
    
    [self setSwipeToBackGesture];
}

#pragma mark - Helper Methods

- (void)adjustViewForFlow:(AboutMeViewFlowType)flowType
{
    _flowType = flowType;
    
    if (_flowType == AboutMeViewFlowTypeEditProfile)
    {
        [_btnSubmit setTitle:SUBMIT_BUTTON_TITLE forState:UIControlStateNormal];
        [_btnSubmit removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
        [_btnSubmit addTarget:self action:@selector(onSubmit:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)saveAboutMeText
{
    if (![self validate])
    {
        return;
    }
    
    [(UserProfile *)[[[self controller] profileWizardManager] profile] setAbout:[_textViewAboutMe text]];
}

#pragma mark - IBActions

- (IBAction)onSubmit:(id)sender //Action is set to Submit Button Programatically
{
    [self saveAboutMeText];
    
    if (_flowType == AboutMeViewFlowTypeEditProfile)
    {
        [(AboutMeController *)self.controller showEditProfileController];
    }
}

- (IBAction)onChooseAttributes:(id)sender
{
    [self saveAboutMeText];
    [(AboutMeController *)self.controller startProfileWizard];
}

#pragma mark - Validations

- (BOOL)validate
{
    BOOL didValidate = [[_textViewAboutMe text] length] <= QUESTION_TEXT_LIMIT;
    
    if (!didValidate)
    {
        [Alert show:VALIDATION_TITLE andMessage:VALIDATION_MESSAGE];
    }
    
    return didValidate;
}

#pragma mark - Textview Delegate

-(void) textViewDidChange:(UITextView *)textView {
    
    _lblCharCount.text = [NSString stringWithFormat:@"%lu/%d char left", QUESTION_TEXT_LIMIT - textView.text.length, QUESTION_TEXT_LIMIT];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if(text.length > QUESTION_TEXT_LIMIT && textView.text.length == 0) {
        textView.text = [text substringToIndex:QUESTION_TEXT_LIMIT];
        _lblCharCount.text = [NSString stringWithFormat:@"%lu/%d char left", QUESTION_TEXT_LIMIT - textView.text.length, QUESTION_TEXT_LIMIT];
        return NO;
    }
    
    return textView.text.length + (text.length - range.length) <= QUESTION_TEXT_LIMIT;
}

@end
