//
//  FeedView.h
//  TurtleWise
//
//  Created by Vijay Bhaskar on 15/03/18.
//  Copyright © 2018 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"

@interface FeedView : BaseView<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *feedsListView;
@property (strong, nonatomic) NSArray *feedList;

-(void)getFeedList;


@end
