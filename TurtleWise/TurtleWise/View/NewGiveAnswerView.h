//
//  NewGiveAnswerView.h
//  TurtleWise
//
//  Created by Ajdal on 1/22/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"
#import "Question.h"
#import "TabbarView.h"
#import "MZTimerLabel.h"
#import "Label.h"
#import "TPKeyboardAvoidingScrollView.h"

@class Answer;

typedef NS_ENUM(NSUInteger, AnswerViewState) {
    AnswerViewStateChoice = 0,
    AnswerViewStateChat
};


@interface NewGiveAnswerView : BaseView

@property (nonatomic,assign) AnswerViewState answerState;

@property(weak ,nonatomic) IBOutlet TabbarView *tabBar;

@property(nonatomic,weak) IBOutlet UIView *contentView;

#pragma mark - Question View
@property(nonatomic,weak) IBOutlet UIImageView *questionImage;
@property(nonatomic,weak) IBOutlet Label *questionTitle;
@property(nonatomic,weak) IBOutlet MZTimerLabel *questionTime;
@property(nonatomic,weak) IBOutlet UILabel *lblRemaining;
@property(nonatomic,weak) IBOutlet UILabel *lblCurrentPeriod;
@property(nonatomic,weak) IBOutlet UIButton *seekerNameBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;

#pragma mark - Input Fields
@property(nonatomic,weak) IBOutlet UITextView *complexAnswerTextView;
@property(nonatomic,weak) IBOutlet UITextView *reasonTextView;
@property(nonatomic,weak) IBOutlet UITextView *whyChooseMeTextView;

#pragma mark --
@property(nonatomic,weak) IBOutlet UIView *answerViewMCQ;
@property(nonatomic,weak) IBOutlet UIView *answerViewComplex;
@property(nonatomic,weak) IBOutlet UIButton *chatAvailabilityYes;
@property(nonatomic,weak) IBOutlet UIButton *chatAvailabilityNo;
@property(nonatomic,weak) IBOutlet UILabel *chatRewardLabel;
@property(nonatomic,weak) IBOutlet UILabel *premiumFeatureStaticLabel;
@property(nonatomic,weak) IBOutlet UITableView *tableViewAnswerOptionDetails;
@property(strong,nonatomic) IBOutletCollection(UIButton) NSArray *answerOptionButtons;
@property(strong,nonatomic) IBOutletCollection(UIButton) NSArray *feedBackButtons;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIView *viewWhyChat;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIView *viewBottom;
@property (weak, nonatomic) IBOutlet UIView *viewChatAvailability;
@property (weak, nonatomic) IBOutlet UIView *viewReason;
@property (strong, nonatomic) IBOutlet UILabel *lblCharCount;

@property(nonatomic,weak) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *tableViewBottomMarginConstraint;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reasonViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseMeViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *questionViewHeightConstraint;

-(void)onBackNavBtnTap;
-(void)onNextNavBtnTap;

-(IBAction)onAnswerChoiceTap:(id)sender;
-(IBAction)onChatAvailabilitySelection:(id)sender;
-(IBAction)onSubmitAnswerBtnTap;
-(IBAction)onFeedbackOptionSelect:(id)sender;
-(IBAction)onSeekerNameBtnTap:(id)sender;

-(void)setQuestion:(Question*)question;
-(void)setupUIForAdvisorAnswerView:(Answer *)answer;
-(void)hideQuestionTimer;
-(void)preparePostAnswerCall;

@end
