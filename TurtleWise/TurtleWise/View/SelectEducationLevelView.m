//
//  SelectEducationLevelView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/11/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SelectEducationLevelController.h"
#import "SelectEducationLevelView.h"
#import "StringUtils.h"
#import "UserProfile.h"
#import "TextField.h"
#import "Constant.h"

#define OPTION_BUTTONS_START_TAG 100
#define OPTION_BUTTONS_END_TAG 103

@interface SelectEducationLevelView ()

@property (nonatomic, strong) NSString *educationLevelTag;

@end

@implementation SelectEducationLevelView

#pragma mark - UIView Life Cycle

-(void)viewDidLoad
{
    [super viewDidLoad];

    _educationLevelTag = EMPTY_STRING;
}

#pragma mark - Override Methods

- (void)updateData:(UserProfile *)profile
{
    _educationLevelTag = [profile educationLevel];
    
    for (int i = OPTION_BUTTONS_START_TAG; i <= OPTION_BUTTONS_END_TAG; i++)
    {
        UIButton *buttonToSelect = (UIButton *)[self viewWithTag:i];
        
        if ([StringUtils compareString:[[buttonToSelect titleLabel] text] withString:[profile educationLevel]])
        {
            [buttonToSelect setSelected:YES];
        }
    }
}

- (void)saveWizardData:(UserProfile *)profile
{
    [profile setEducationLevel:_educationLevelTag];
}

#pragma mark - IBActions

- (IBAction)selectEducationLevel:(id)sender
{
    for (int i = OPTION_BUTTONS_START_TAG; i <= OPTION_BUTTONS_END_TAG; i++)
    {
        if (i == [sender tag])
        {
            [sender setSelected:![sender isSelected]];
            
            _educationLevelTag = ([sender isSelected]) ? [[sender titleLabel] text] : EMPTY_STRING;
            
            continue;
        }
        
        UIButton *unselectedButtons = (UIButton *)[self viewWithTag:i];
        [unselectedButtons setSelected:NO];
    }
}

- (IBAction)submitWizard:(id)sender
{
    [self completeWizard];
}

@end
