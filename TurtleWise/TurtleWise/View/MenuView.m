//
//  MenuView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/12/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SocialLoginController.h"
#import "UserProfileController.h"
#import "TurtleswagController.h"
#import "SettingsController.h"
#import "MenuController.h"
#import "FAQController.h"
#import "HomeController.h"
#import "UserDefaults.h"
#import "MenuView.h"
#import "Utility.h"
#import "Enum.h"
#import "ManageBrandedPageController.h"
#import "OrganisationAndInfluencerController.h"
#import "BrandedPageManager.h"
#import "QuestionListingController.h"
#import "BoardOfAdvisorsController.h"
#import "FeedController.h"

#define TEXT_USER_ROLE_SEEKER @"Switch to Explorer Role"
#define TEXT_USER_ROLE_ADVISOR @"Switch to Guru Role"

@interface MenuView ()

@end

@implementation MenuView

#pragma mark - UIView Life Cycle

- (void)viewDidLoad
{
    _userRole = [UserDefaults getUserRole];
    [_btnSwitchRoles setSelected:_userRole];
}

#pragma mark - Public Method

- (void)updateUserRole
{
    [_lblUserRole setText:(_userRole == UserRoleSeeker) ? TEXT_USER_ROLE_ADVISOR : TEXT_USER_ROLE_SEEKER];
}

-(void)viewWillAppear {
    [super viewWillAppear];
}

-(void)updateBrandedPage {
    if ([[UserDefaults getPageID] length] == 0) {
        _manageBrandedPageHeight.constant = 0.0;
        _lblManagePage.hidden = TRUE;
    } else {
        _manageBrandedPageHeight.constant = 40.0;
        _lblManagePage.hidden = FALSE;
    }
}


#pragma mark - IBActions

- (IBAction)switchUserRole:(id)sender
{
    _userRole = ((_userRole == UserRoleAdvisor) ? UserRoleSeeker : UserRoleAdvisor);
    
    [self updateUserRole];
    [UserDefaults setUserRole:_userRole];

    [(MenuController *)[self controller] pushController:[HomeController new]];
}

- (IBAction)showUserProfile:(id)sender
{
    [(MenuController *)[self controller] pushController:[UserProfileController new]];
}

- (IBAction)showTurtleSwag:(id)sender
{
//    [(MenuController *)[self controller] pushController:[TurtleswagController new]];
    [(MenuController *)[self controller] openRedeemInSafari];
}

- (IBAction)showHelp:(id)sender
{
    [self notImplemented];
}

- (IBAction)logout:(id)sender
{    
    [(MenuController *)[self controller] userLogOut];
}

- (IBAction)showSettings:(id)sender
{
    [(MenuController *)[self controller] pushController:[SettingsController new]];
}
-(IBAction)showArchivedQuestions:(id)sender
{
    QuestionListingController *questionList = [QuestionListingController new];
    questionList.isArchived = YES;
    [(MenuController *)[self controller] pushController:questionList];
}

- (IBAction)showFAQ:(id)sender {
    [(MenuController *)[self controller] pushController:[FAQController new]];
}
- (IBAction)showManageBrandedPage:(id)sender {
    [[BrandedPageManager sharedManager] clearPageId];
    [(MenuController *)[self controller] pushController:[ManageBrandedPageController new]];
}

- (IBAction)showBoardofAdvisors:(id)sender {
    [(MenuController*)[self controller] pushController:[BoardOfAdvisorsController new]];
}

- (IBAction)whoIAdvise:(id)sender {
}
- (IBAction)showViewOrganisationAndInfluencer:(id)sender {
    [(MenuController *)[self controller] pushController:[OrganisationAndInfluencerController new]];
}
- (IBAction)feed:(id)sender {
    
    [(MenuController*)[self controller] pushController:[FeedController new]];
}


@end
