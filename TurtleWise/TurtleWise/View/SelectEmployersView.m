//
//  SelectEmployersView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/11/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SelectEmployersView.h"
#import "TWTableView.h"
#import "UserProfile.h"

@interface SelectEmployersView ()

@end

@implementation SelectEmployersView

#pragma mark - UIView Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_tableView setAddMoreButton:_btnAddMore];
}

#pragma mark - Override Methods

- (void)updateData:(UserProfile *)profile
{
    [_tableView prepareDataSet:[profile employers]];
}

- (void)saveWizardData:(UserProfile *)profile
{
    [profile setEmployers:[_tableView getDataSetAsArray]];
}

#pragma mark - IBActions

- (IBAction)submitWizard:(id)sender
{
    [self completeWizard];
}

- (IBAction)addMore:(id)sender
{
    [_tableView addRow];
}

@end
