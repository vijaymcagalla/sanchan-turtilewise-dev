//
//  SelectHobbiesView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/8/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "SelectHobbiesView.h"
#import "TWTableView.h"
#import "UserProfile.h"
#import "SelectHobbiesController.h"
#import "ItemListView.h"

@interface SelectHobbiesView ()<SelectedItemDelegate>
{
    NSArray *list;
    ItemListView *itemView;
}

@end

@implementation SelectHobbiesView

#pragma mark - UIView Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_tableView setAddMoreButton:_btnAddMore];
}

#pragma mark - Override Methods

- (void)updateData:(UserProfile *)profile
{
    [_tableView prepareDataSet:[profile hobbies]];
}

- (void)saveWizardData:(UserProfile *)profile
{
    [profile setHobbies:[_tableView getDataSetAsArray]];
}

#pragma mark - IBActions

- (IBAction)submitWizard:(id)sender
{
    [self completeWizard];
}

-(IBAction)selectHobbies:(id)sender{
    
//    [self showPickerWith:@[@"Reading",@"Writing",@"Singing",@"Browsing",@"Chatting"]];
    [(SelectHobbiesController*)[self controller] getHobbiesListwith:^(id responseObject) {
        self->list = responseObject[@"data"];
        [self showPickerWith:list];
        [self.tableView setMaxRowsAllowed:list.count];
    }];
}
-(void)showPickerWith:(NSArray*)list
{
    [UIView transitionWithView:self duration:0.5 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        
        itemView =  (ItemListView*)[[NSBundle mainBundle]loadNibNamed:@"ItemListView" owner:self options:nil].firstObject;
        
        itemView.frame = self.superview.bounds;
        itemView.delegate = self;
        [itemView setList:list];
        [self addSubview:itemView];
    } completion:^(BOOL finished) {
        
    }];
    
}
-(void)selectedItem:(NSArray *)selectedList{
    [itemView removeFromSuperview];
    itemView = nil;
    [self.tableView prepareDataSet:selectedList];
}
- (IBAction)addMore:(id)sender
{
    [_tableView addRow];
}


@end

