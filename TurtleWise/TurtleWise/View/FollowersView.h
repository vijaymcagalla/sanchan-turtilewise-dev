//
//  FollowersView.h
//  TurtleWise
//
//  Created by Sunflower on 9/7/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowersView : UIView<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *followersTableView;
- (void) getData:(NSArray*)req;
@end
