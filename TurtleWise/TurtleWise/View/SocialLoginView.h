//
//  SocialLoginView.h
//  TurtleWise
//
//  Created by Irfan Gul on 02/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"

@interface SocialLoginView : BaseView

- (IBAction)onLoginIn:(id)sender;
- (IBAction)onSignupEmail:(id)sender;
- (IBAction)signInWithFacebook:(id)sender;
- (IBAction)signInWithLinkedIn:(id)sender;

@end
