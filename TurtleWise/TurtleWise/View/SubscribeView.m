//
//  SubscribeView.m
//  TurtleWise
//
//  Created by Irfan Gul on 09/07/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SubscribeController.h"
#import "SubscribeView.h"

@implementation SubscribeView

#pragma mark - IBActions

- (IBAction)buyMonthlySubscription:(id)sender
{
    [(SubscribeController *)self.controller buyMonthlySubscription];
}
/*
- (IBAction)buyThisQuestionSubscription:(id)sender
{
    [(SubscribeController *)self.controller buyThisQuestionSubscription];
}*/

-(IBAction)buyHalfYearlySubscription:(id)sender{
    
    [(SubscribeController*)self.controller buyHalfYearlySubscription];
}
-(IBAction)buyAnnualSubscription:(id)sender{
    [(SubscribeController*)self.controller buyAnnualSubscription];
}

#pragma mark - Helper Methods
/*
- (void)hideSingleQuestionSubscription
{
    [_btnSingleQuestionSubscription setHidden:NO];
}*/
- (IBAction)applyCode:(id)sender {
    
    [(SubscribeController*)self.controller applyForCoupon:_couponField.text];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
