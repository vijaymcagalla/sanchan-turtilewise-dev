//
//  AskQuestionView.m
//  TurtleWise
//
//  Created by Irfan Gul on 05/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "AskQuestionController.h"
#import "AskQuestionView.h"
#import "UserDefaults.h"
#import "TextField.h"

#define QUESTION_TEXT_LIMIT 255
#define QUESTION_TEXT_MIN_LIMIT 2

#define MESSAGE_ERROR @"Question should be between 2 to 255 characters."

@interface AskQuestionView ()<UITextViewDelegate>


@end

@implementation AskQuestionView

#pragma mark - Life Cycle Methods

- (void)viewDidAppear
{
     [_inputTextView becomeFirstResponder];
}

- (BOOL)validate
{
    NSInteger textLenght = [[_inputTextView text] length];
    
    if (textLenght < QUESTION_TEXT_MIN_LIMIT || textLenght > QUESTION_TEXT_LIMIT)
    {
        [Alert show:TITLE_ALERT_ERROR andMessage:MESSAGE_ERROR];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - Actions

- (IBAction)onNext:(id)sender
{
    if ([self validate])
    {
       [(AskQuestionController *)self.controller startAdvisorSelectionForQuestion:[_inputTextView text]];
    }
}

-(void) clearQuestion {
    _inputTextView.text = @"";
}

#pragma mark - TextView Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void) textViewDidChange:(UITextView *)textView
{
    [_nextBtn setEnabled:([textView hasText])];
}

@end
