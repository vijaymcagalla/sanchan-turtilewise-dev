//
//  SelectSignUpAsView.h
//  TurtleWise
//
//  Created by Hardeep Kaur on 03/06/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//


#import "BaseProfileWizardView.h"
#import "TextField.h"
#import "TWWizardManager.h"


@interface SelectSignUpAsView : BaseProfileWizardView

@property (weak, nonatomic) IBOutlet TextField *fieldName;

- (IBAction)selectSignUpAs:(id)sender;
- (IBAction)submitWizard:(id)sender;
- (IBAction)skipThisStep:(id)sender;

@end
