//
//  ItemViewCell.m
//  TurtleWise
//
//  Created by Vijay Bhaskar on 08/07/18.
//  Copyright © 2018 Waaleed Khan. All rights reserved.
//

#import "ItemViewCell.h"

@implementation ItemViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
