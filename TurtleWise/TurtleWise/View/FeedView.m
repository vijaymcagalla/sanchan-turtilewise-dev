//
//  FeedView.m
//  TurtleWise
//
//  Created by Vijay Bhaskar on 15/03/18.
//  Copyright © 2018 Waaleed Khan. All rights reserved.
//

#import "FeedView.h"
#import "FeedController.h"
#import "FeedViewCell.h"

@implementation FeedView

-(void)getFeedList{
    
    [(FeedController*)[self controller] getTurtleWisePages];
    [self.feedsListView registerNib:[UINib nibWithNibName:@"FeedViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1.0f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _feedList.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FeedViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dict = _feedList[indexPath.row];
    cell.titleLabel.text = [dict objectForKey:@"title"];
    cell.descriptionLabel.text = [dict objectForKey:@"summary"];
    NSURL *url = [NSURL URLWithString:[dict objectForKey:@"image"]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *image = [UIImage imageWithData:data];
    cell.iconImageView.image = image;
    return cell;
}

@end
