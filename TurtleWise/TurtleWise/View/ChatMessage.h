//
//  ChatMessage.h
//  TurtleWise
//
//  Created by Irfan Gul on 16/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseEntity.h"
#import "MessageSender.h"
#import "Enum.h"

@interface ChatMessage : BaseEntity

@property(nonatomic, strong) MessageSender *messageSender;

@property(nonatomic, strong) NSString *message;
@property(nonatomic, strong) NSString *conversationId;
@property(nonatomic, strong) NSString *messageIdBeforeSending;

@property(nonatomic, strong) NSDate *messageDate;

@property(nonatomic) BOOL isMyMessage;
@property(nonatomic) ChatMessageStatus messageStatus;

- (void)set:(NSDictionary *)input withUserId:(NSString *)userId;
- (void)setForNotificationPayload:(NSDictionary *)input;

- (NSString *)messageTimeString;

@end
