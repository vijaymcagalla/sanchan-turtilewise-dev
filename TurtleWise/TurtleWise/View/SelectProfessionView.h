//
//  SelectProfessionView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/11/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseProfileWizardView.h"
#import "BaseView.h"


@class TWTableView;

@interface SelectProfessionView : BaseProfileWizardView

//Outlets

@property (weak, nonatomic) IBOutlet TWTableView *tableViewProfession;
@property (weak, nonatomic) IBOutlet TWTableView *tableViewEmployer;
@property (weak, nonatomic) IBOutlet UIButton *btnProfessionAddMore;
@property (weak, nonatomic) IBOutlet UIButton *btnEmployerAddMore;

//Actions
- (IBAction)submitWizard:(id)sender;
- (IBAction)addMore:(id)sender;

@end
