//
//  ViewOrganisationAndInfluencerView.h
//  TurtleWise
//
//  Created by Sunflower on 8/31/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"

@interface OrganisationAndInfluencerView : BaseView {
    NSString * selectedView;
    NSMutableArray * influencerArray;
    NSMutableArray * organisationArray;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionview;

@property (weak, nonatomic) IBOutlet UILabel *lblInfluencer;
@property (weak, nonatomic) IBOutlet UILabel *lblInfluencerSelected;
@property (weak, nonatomic) IBOutlet UILabel *lblOrganisation;
@property (weak, nonatomic) IBOutlet UILabel *lblOrganisationSelected;

-(void) viewWithArray:(NSArray*)array;
@end
