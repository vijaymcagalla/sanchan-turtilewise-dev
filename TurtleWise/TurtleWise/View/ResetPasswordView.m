//
//  ResetPasswordView.m
//  TurtleWise
//
//  Created by Usman Asif on 11/8/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "ResetPasswordController.h"
#import "ResetPasswordView.h"
#import "StringUtils.h"
#import "TextField.h"
#import "Constant.h"

@interface ResetPasswordView()

@property (weak, nonatomic) IBOutlet TextField *txtFieldNewPassword;
@property (weak, nonatomic) IBOutlet TextField *txtFieldConfirmPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@end

@implementation ResetPasswordView

-(void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}

#pragma mark - Private Methods

- (void)initUI {
    _btnSave.layer.cornerRadius = floor(_btnSave.frame.size.height/4);
    _btnSave.clipsToBounds = YES;
    
    [super setSwipeToBackGesture];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - IBActions

- (IBAction)onClickSave {
    if ([self validateFields]) {
        [(ResetPasswordController *)self.controller forgotResetPassword:_txtFieldNewPassword.text];
    }
}

#pragma -  Validators

- (BOOL)validateFields {
    NSString *newPassword = [_txtFieldNewPassword text];
    NSString *confirmPassword = [_txtFieldConfirmPassword text];
    
    if ([StringUtils isEmptyOrNull:newPassword])
    {
        [Alert show:TITLE_ALERT_EMPTY_FIELD andMessage:TITLE_ALERT_INVALID_NEW_PASSWORD];
        return NO;
    }
    
    if ([StringUtils isEmptyOrNull:confirmPassword])
    {
        [Alert show:TITLE_ALERT_EMPTY_FIELD andMessage:TITLE_ALERT_INVALID_CONFIRM_PASSWORD];
        return NO;
    }
    
    if (![StringUtils compareString:newPassword withString:confirmPassword])
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:ERROR_MESSAGE_PASSWORD_DONOT_MATCH];
        return NO;
    }
    
    if ([newPassword length] < 6 || [newPassword length] > 32)
    {
        [Alert show:TITLE_ALERT_INVALID_FIELD andMessage:ERROR_MESSAGE_IN_VALID_PASSWORD];
        return NO;
    }
    
    return YES;
}

@end