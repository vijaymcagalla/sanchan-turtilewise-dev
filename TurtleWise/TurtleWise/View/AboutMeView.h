//
//  AboutMeView.h
//  TurtleWise
//
//  Created by Irfan Gul on 26/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"
#import "Enum.h"

@interface AboutMeView : BaseView
@property (weak, nonatomic) IBOutlet UITextView *textViewAboutMe;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnAttributes;
@property (strong, nonatomic) IBOutlet UILabel *lblCharCount;

- (IBAction)onSubmit:(id)sender;
- (IBAction)onChooseAttributes:(id)sender;

- (void)adjustViewForFlow:(AboutMeViewFlowType)flowType;

@end
