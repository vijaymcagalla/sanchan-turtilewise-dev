//
//  EditProfileView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/17/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"

@class UserProfile;

@interface EditProfileView : BaseView

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIImageView *imgUserProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblFullName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserLevel;

@property (weak, nonatomic) IBOutlet UISwitch *switchPrivacy;
@property (weak, nonatomic) IBOutlet UIButton *forPrivacyLabel;

- (void)setProfile:(UserProfile *)profile;
- (void)updatePrivacy;

- (IBAction)changePrivacy:(id)sender;

@end
