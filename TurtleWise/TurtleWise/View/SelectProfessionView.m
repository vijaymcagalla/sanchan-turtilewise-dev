//
//  SelectProfessionView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/11/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SelectProfessionView.h"
#import "TWTableView.h"
#import "UserProfile.h"

#define TAG_ADD_PROFESSION 101

@interface SelectProfessionView ()

@end

@implementation SelectProfessionView

#pragma mark - UIView Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_tableViewProfession setAddMoreButton:_btnProfessionAddMore];
    [_tableViewEmployer setAddMoreButton:_btnEmployerAddMore];
}

#pragma mark - Override Methods

- (void)updateData:(UserProfile *)profile
{
    [_tableViewProfession prepareDataSet:[profile professions]];
    [_tableViewEmployer prepareDataSet:[profile employers]];
}

- (void)saveWizardData:(UserProfile *)profile
{
    [profile setProfessions:[_tableViewProfession getDataSetAsArray]];
    [profile setEmployers:[_tableViewEmployer getDataSetAsArray]];
}

#pragma mark - IBActions

- (IBAction)submitWizard:(id)sender
{
    [self completeWizard];
}

- (IBAction)addMore:(id)sender
{
    if([sender tag] == TAG_ADD_PROFESSION)
        [_tableViewProfession addRow];
    else
        [_tableViewEmployer addRow];
}

@end
