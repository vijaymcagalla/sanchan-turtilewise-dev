//
//  UserProfileView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/9/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "UserProfileView.h"
#import "PercentFillView.h"
#import "UserProfileCell.h"
#import "EditProfileCell.h"
#import "ProfileCell.h"
#import "UserProfile.h"
#import "Constant.h"
#import "Keys.h"

#define NORMAL_CELL_IDENTIFIER @"UserProfileCell"
#define ABOUT_ME_CELL_IDENTIFIER @"EditProfileCell"

@interface UserProfileView () < UITableViewDataSource, UITableViewDelegate >

@property(nonatomic, strong) NSArray *arrProfileData;

- (void)setupUI;

@end

@implementation UserProfileView

#pragma mark - Life Cycle

- (void)viewDidLoad
{
    [self setupUI];
}

- (void)setupUI
{
    [[_imgUserProfile layer] setCornerRadius:CGRectGetWidth([_imgUserProfile frame])/2.0f];
    [_imgUserProfile setClipsToBounds:YES];
    
    [_tableView setEstimatedRowHeight:146.0];
    [_tableView setTableHeaderView:_headerView];
    [_tableView setRowHeight:UITableViewAutomaticDimension];
}

- (void)updateUserProfile:(UserProfile *)profile
{
    _arrProfileData = [profile getAsArrayWithEmptyValues:NO];
    
    [_tableView setHidden:NO];
    [_tableView  reloadData];
    
    [_imgUserProfile setImage:[profile avatar]];
    [_lblFullName setText:[profile fullName]];
    [_lblUserLevel setText:[NSString stringWithFormat:LEVEL_PREFIX,(int)[profile level]]];
    [_lblProfileComplete setText:[profile profilePercentage]];
    
    [_viewProfileComplete setFillPercentage:[[profile profilePercentage] floatValue]];
    [_viewProfileComplete setNeedsDisplay];
}


#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_arrProfileData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = NORMAL_CELL_IDENTIFIER;
    UserProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if ([indexPath row] == 0 && [[(ProfileCell *)[_arrProfileData firstObject] responseKey] isEqualToString:KEY_ABOUT])
    {
        cellIdentifier = ABOUT_ME_CELL_IDENTIFIER;
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    if (!cell)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    [cell set:[_arrProfileData objectAtIndex:[indexPath row]]];
    
    return cell;
}


@end
