//
//  SelectSiblingsView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/9/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SelectSiblingsController.h"
#import "SelectSiblingsView.h"
#import "BaseController.h"
#import "StringUtils.h"
#import "UserProfile.h"
#import "TextField.h"
#import "Constant.h"

#define TITLE_VALIDATION @"Limit exceeds!"

#define MESSAGE_ALERT_KIDS_VALIDATION @"Kids should be between 0 to 20."
#define MESSAGE_ALERT_SIBLINGS_VALIDATION @"Siblings should be 0 to 20."

#define PREFIX_KIDS_TAG @"Kids:%@"
#define PREFIX_SIBLINGS_TAG @"Siblings:%@"

@interface SelectSiblingsView ()

@end

@implementation SelectSiblingsView

#pragma mark - Override Methods

- (void)updateData:(UserProfile *)profile
{
    [_fieldKids setText:[[profile kids] stringValue]];
    [_fieldSiblings setText:[[profile siblings] stringValue]];
    
    [_stepperKids setValue:[[profile kids] intValue]];
    [_stepperSiblings setValue:[[profile siblings] intValue]];
}

- (void)saveWizardData:(UserProfile *)profile
{
    NSString *numberOfKids = [_fieldKids text];
    NSString *numberOfSiblings = [_fieldSiblings text];
    
    if (![StringUtils isEmptyOrNull:numberOfKids])
    {
        [profile setKids:[NSNumber numberWithInt:[numberOfKids intValue]]];
    }
    
    if (![StringUtils isEmptyOrNull:numberOfSiblings])
    {
        [profile setSiblings:[NSNumber numberWithInt:[numberOfSiblings intValue]]];
    }
}

#pragma mark - Public/Private Methods

- (void)resignFieldsOnView
{
    [_fieldKids resignFirstResponder];
    [_fieldSiblings resignFirstResponder];
}

- (BOOL)areFieldsValid
{
    if ([_fieldKids isValid] && ![_fieldKids isValueInRangeOf:0 and:20])
    {
        [Alert show:TITLE_VALIDATION andMessage:MESSAGE_ALERT_KIDS_VALIDATION];
        
        return NO;
    }
    
    if ([_fieldSiblings isValid] && ![_fieldSiblings isValueInRangeOf:0 and:20])
    {
        [Alert show:TITLE_VALIDATION andMessage:MESSAGE_ALERT_SIBLINGS_VALIDATION];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - IBActions

- (IBAction)submitWizard:(id)sender
{
    if (![self areFieldsValid])
    {
        return;
    }
    
    [self completeWizard];
}

- (IBAction)onSteppingKids:(UIStepper *)sender
{
    [_fieldKids setText:[NSString stringWithFormat:@"%i", (int)sender.value]];
}

- (IBAction)onSteppingSiblings:(UIStepper *)sender
{
    [_fieldSiblings setText:[NSString stringWithFormat:@"%i", (int)sender.value]];
}

@end
