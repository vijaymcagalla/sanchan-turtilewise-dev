//
//  UserLocationView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/7/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseProfileWizardView.h"

@class TextField;
@class Button;

@interface UserLocationView : BaseProfileWizardView

//Outlets
@property (weak, nonatomic) IBOutlet TextField *txtFieldCity;
@property (weak, nonatomic) IBOutlet Button *btnCountry;
@property (weak, nonatomic) IBOutlet Button *btnState;

//Actions
- (IBAction)submitWizard:(id)sender;
- (IBAction)openCountryPicker:(Button *)sender;
- (IBAction)openStatePicker:(Button *)sender;

@end
