//
//  ManageBrandedPageView.h
//  TurtleWise
//
//  Created by Sunflower on 8/22/17.
//  Copyright © 2017 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"
#import "FZAccordionTableView.h"
#import "AccordionHeaderView.h"
@class BrandedPage;


@interface ManageBrandedPageView : BaseView {
    BrandedPage * pageBranded;
}
@property (weak, nonatomic) IBOutlet FZAccordionTableView *tableview;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imgCoverpic;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblType;
- (void) brandedPageWith:(BrandedPage *) pageDetail;
- (void) getPrivacy:(BrandedPage *)pageDetail;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;
@property (weak, nonatomic) IBOutlet UIButton *pictureEditButton;
@property (weak, nonatomic) IBOutlet UIButton *nameEditButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *privacySegment;
@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *followersHeight;
@property (weak, nonatomic) IBOutlet UIButton *followersButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *privacySegmentHeight;


- (void) showRequestView:(NSArray*)array;
- (void) showFollowersView:(NSArray*)array;
-(void)removesubview;
@end
