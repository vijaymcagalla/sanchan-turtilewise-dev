//
//  QuestionTypeView.h
//  TurtleWise
//
//  Created by Irfan Gul on 05/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"

@class TLTagsControl;

@interface AdvisorSelectionView : BaseView {
    BOOL isOwner;
}

@property (weak, nonatomic) IBOutlet TLTagsControl *attributtesTagView;
@property (weak, nonatomic) IBOutlet TLTagsControl *guruTagsView;

- (IBAction)setAdvisorType:(id)sender;
- (IBAction)selectQuestionType:(id)sender;
- (IBAction)addGuruTagButton:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewToQue;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *seekToQue;
@property (weak, nonatomic) IBOutlet UIView *guruView;
- (void)setupHiddenUI;
- (void)setupShowUI;
@end
