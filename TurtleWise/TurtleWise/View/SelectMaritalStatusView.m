//
//  SelectMaritialStatusView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/9/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "SelectMaritalStatusController.h"
#import "SelectMaritalStatusView.h"
#import "StringUtils.h"
#import "Constant.h"
#import "UserProfile.h"

#define OPTION_BUTTONS_START_TAG 100
#define OPTION_BUTTONS_END_TAG 104

#define TITLE_VALIDATION @"Limit exceeds!"

#define MESSAGE_ALERT_KIDS_VALIDATION @"Kids should be between 0 to 20."
#define MESSAGE_ALERT_SIBLINGS_VALIDATION @"Siblings should be 0 to 20."

#define PREFIX_KIDS_TAG @"Kids:%@"
#define PREFIX_SIBLINGS_TAG @"Siblings:%@"

@interface SelectMaritalStatusView ()

@property(nonatomic, strong) NSString *maritalStatusTag;

@end

@implementation SelectMaritalStatusView

#pragma mark - UIView Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _maritalStatusTag = EMPTY_STRING;
}

#pragma mark - Override Methods

- (void)updateData:(UserProfile *)profile
{
    _maritalStatusTag = [profile maritalStatus];
    
    for (int i = OPTION_BUTTONS_START_TAG; i <= OPTION_BUTTONS_END_TAG; i++)
    {
        UIButton *buttonToSelect = (UIButton *)[self viewWithTag:i];
        
        if ([StringUtils compareString:[[buttonToSelect titleLabel] text] withString:[profile maritalStatus]])
        {
            [buttonToSelect setSelected:YES];
        }
    }
    
    [_fieldKids setText:[[profile kids] stringValue]];
    [_fieldSiblings setText:[[profile siblings] stringValue]];
    
    [_stepperKids setValue:[[profile kids] intValue]];
    [_stepperSiblings setValue:[[profile siblings] intValue]];
}

- (void)saveWizardData:(UserProfile *)profile
{
    [profile setMaritalStatus:_maritalStatusTag];
    
    NSString *numberOfKids = [_fieldKids text];
    NSString *numberOfSiblings = [_fieldSiblings text];
    
    if (![StringUtils isEmptyOrNull:numberOfKids])
    {
        [profile setKids:[NSNumber numberWithInt:[numberOfKids intValue]]];
    }
    
    if (![StringUtils isEmptyOrNull:numberOfSiblings])
    {
        [profile setSiblings:[NSNumber numberWithInt:[numberOfSiblings intValue]]];
    }

}

#pragma mark - IBActions

- (IBAction)setMaritalStatus:(id)sender
{
    for (int i = OPTION_BUTTONS_START_TAG; i <= OPTION_BUTTONS_END_TAG; i++)
    {
        if (i == [sender tag])
        {
            [sender setSelected:![sender isSelected]];
            
            _maritalStatusTag = ([sender isSelected]) ? [[sender titleLabel] text] : EMPTY_STRING;
            continue;
        }
        
        UIButton *unselectedButtons = (UIButton *)[self viewWithTag:i];
        [unselectedButtons setSelected:NO];
    }
}

- (IBAction)submitWizard:(id)sender
{
    if (![self areFieldsValid])
    {
        return;
    }
    
    [self completeWizard];
}

- (IBAction)onSteppingKids:(UIStepper *)sender
{
    [_fieldKids setText:[NSString stringWithFormat:@"%i", (int)sender.value]];
}

- (IBAction)onSteppingSiblings:(UIStepper *)sender
{
    [_fieldSiblings setText:[NSString stringWithFormat:@"%i", (int)sender.value]];
}

#pragma mark - Public/Private Methods

- (void)resignFieldsOnView
{
    [_fieldKids resignFirstResponder];
    [_fieldSiblings resignFirstResponder];
}

- (BOOL)areFieldsValid
{
    if ([_fieldKids isValid] && ![_fieldKids isValueInRangeOf:0 and:20])
    {
        [Alert show:TITLE_VALIDATION andMessage:MESSAGE_ALERT_KIDS_VALIDATION];
        
        return NO;
    }
    
    if ([_fieldSiblings isValid] && ![_fieldSiblings isValueInRangeOf:0 and:20])
    {
        [Alert show:TITLE_VALIDATION andMessage:MESSAGE_ALERT_SIBLINGS_VALIDATION];
        
        return NO;
    }
    
    return YES;
}

@end
