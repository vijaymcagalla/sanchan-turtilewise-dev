//
//  TabbarView.m
//  TurtleWise
//
//  Created by Irfan Gul on 08/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "UserDefaults.h"
#import "TabbarView.h"
#import "UserStats.h"

#define TAB_NORMAL_BG_COLOR [UIColor blackColor]
#define TAB_SELECTED_BG_COLOR [UIColor colorWithRed:(64/255.0) green:(64/255.0) blue:(64/255.0) alpha:1.0]

@interface TabbarView ()

@end


@implementation TabbarView

-(void)awakeFromNib
{
    [self setupUI];
}

-(void)setupUI
{
    _lblQuestionBubble.layer.cornerRadius = 14.0f;
    _lblAnswerBubble.layer.cornerRadius = 14.0f;
    _lblChatBubble.layer.cornerRadius = 14.0f;
    
    _lblQuestionBubble.clipsToBounds = YES;
    _lblAnswerBubble.clipsToBounds = YES;
    _lblChatBubble.clipsToBounds = YES;
    
    _btnQuestion.selected = YES;
    
    _separatorLeft.hidden = YES;
    _separatorRight.hidden = NO;
    
    [_btnQuestion setBackgroundColor:TAB_SELECTED_BG_COLOR];
    [_btnChat setBackgroundColor:TAB_NORMAL_BG_COLOR];
    [_btnAnswer setBackgroundColor:TAB_NORMAL_BG_COLOR];
    
    [self updateNotificationValues:[UserDefaults getUserStats]];
}

#pragma mark - Notification Bubble Work

- (void)updateNotificationValues:(UserStats *)userStats
{
    UserRole userRole = [UserDefaults getUserRole];
    
    long questionValue = [userStats advisorQuestions];
    long answerValue = 0;
    
    if (userRole == UserRoleSeeker)
    {
        questionValue = [userStats seekerQuestions];
        answerValue = [userStats seekerAnswers];
    }
    
    [self setNotificationCountOn:_lblQuestionBubble withValue:questionValue];
    [self setNotificationCountOn:_lblAnswerBubble withValue:answerValue];
    [self setNotificationCountOn:_lblChatBubble withValue:[userStats unreadChats]];
}

- (void)setNotificationCountOn:(UILabel *)lblBubble withValue:(long)bubbleValue
{
    if(bubbleValue == 0)
    {
        [lblBubble setHidden:YES];
        
        return;
    }
    
    [lblBubble setHidden:NO];
    [lblBubble setText:[NSString stringWithFormat:@"%ld", bubbleValue]];
}

-(IBAction)onTabButton:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    _selectedTab = btn.tag;
    NSLog(@"Selected Tab: %lu",(unsigned long)_selectedTab);
    
    switch (btn.tag) {
        case 0:
        {
            [_btnQuestion setBackgroundColor:TAB_SELECTED_BG_COLOR];
            [_btnChat setBackgroundColor:TAB_NORMAL_BG_COLOR];
            [_btnAnswer setBackgroundColor:TAB_NORMAL_BG_COLOR];
            _separatorLeft.hidden = YES;
            _separatorRight.hidden = NO;

        }
            break;
            case 1:
        {
            [_btnQuestion setBackgroundColor:TAB_NORMAL_BG_COLOR];
            [_btnAnswer setBackgroundColor:TAB_SELECTED_BG_COLOR];
            [_btnChat setBackgroundColor:TAB_NORMAL_BG_COLOR];
            _separatorLeft.hidden = YES;
            _separatorRight.hidden = YES;

        }
            break;
            case 2:
        {
            [_btnQuestion setBackgroundColor:TAB_NORMAL_BG_COLOR];
            [_btnAnswer setBackgroundColor:TAB_NORMAL_BG_COLOR];
            [_btnChat setBackgroundColor:TAB_SELECTED_BG_COLOR];
            _separatorRight.hidden = YES;
            _separatorLeft.hidden = NO;
        }
            break;
            
        default:
            break;
    }
    
    if(_delegate && [_delegate respondsToSelector:@selector(tabbar:didSelectTab:)])
        [_delegate tabbar:self didSelectTab:btn.tag];
}
-(void)setSelectedTab:(TabbarViewType)selectedTab
{
    switch (selectedTab) {
        case TabbarViewTypeQuestion:
            [self onTabButton:_btnQuestion];
            break;
        case TabbarViewTypeAnswer:
            [self onTabButton:_btnAnswer];
            break;
        case TabbarViewTypeChat:
            [self onTabButton:_btnChat];
            break;
            
        default:
            break;
    }
}


-(void)enableTab:(BOOL)enabled tabbarType:(TabbarViewType) type
{
    switch (type) {
        case TabbarViewTypeQuestion:
        {
            [_btnQuestion setUserInteractionEnabled:enabled];
        }
            break;
        case TabbarViewTypeAnswer:
        {
            [_btnAnswer setUserInteractionEnabled:enabled];
        }
            break;
        case TabbarViewTypeChat:
        {
            [_btnChat setUserInteractionEnabled:enabled];
        }
            break;
            
        default:
            break;
    }
}

@end
