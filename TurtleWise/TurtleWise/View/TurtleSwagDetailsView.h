//
//  TurtleSwagDetailsView.h
//  TurtleWise
//
//  Created by Waleed Khan on 7/12/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "BaseView.h"

@class Label;
@class Charity;
@class TextField;
@class AsyncImageView;

@interface TurtleSwagDetailsView : BaseView

@property (weak, nonatomic) IBOutlet AsyncImageView *displayImage;
@property (weak, nonatomic) IBOutlet Label *title;
@property (weak, nonatomic) IBOutlet UILabel *details;
@property (weak, nonatomic) IBOutlet TextField *fieldBucks;
@property (weak, nonatomic) IBOutlet UIButton *btnDollarAmount;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailsLblTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dollarBtnTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewHeightConstraint;

- (void)populateDetails:(Charity *)charityDetails;
- (void)redeemSucessfull;

@end
