//
//  NewUserInfoView.m
//  TurtleWise
//
//  Created by Usman Asif on 1/22/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "NewUserInfoView.h"
#import "UserProfileCell.h"
#import "ProfileCell.h"
#import "UserProfile.h"
#import "Constant.h"
#import "Utility.h"
#import "Font.h"
#import "Keys.h"

#define HEADING_BIO @"Bio:"
#define HEADING_ATTRIBUTES @"Attributes:"
#define CHAT_FEE_PREFIX @"Chat Fee:"
#define THANKS_POSTFIX @"%ld Thanks"
#define FORMATTED_NUMBER @"%ld"

#define NORMAL_CELL_IDENTIFIER @"UserProfileCell"
#define ABOUT_ME_CELL_IDENTIFIER @"EditProfileCell"

@interface NewUserInfoView()
{
    NSArray *arrProfileData;
}

@end

@implementation NewUserInfoView

#pragma mark - UIView Life Cycle

- (void)viewDidLoad
{
    [self setupUI];
}

- (void)setupUI
{
    [[_imgViewUserDp layer] setCornerRadius:CGRectGetWidth([_imgViewUserDp frame])/2.0f];
    [_imgViewUserDp setClipsToBounds:YES];
    
    [_tableView setEstimatedRowHeight:146.0];
    [_tableView setTableHeaderView:_headerView];
    [_tableView setRowHeight:UITableViewAutomaticDimension];
}


#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrProfileData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = NORMAL_CELL_IDENTIFIER;
    UserProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if ([indexPath row] == 0 && [[(ProfileCell *)[arrProfileData firstObject] responseKey] isEqualToString:KEY_ABOUT])
    {
        cellIdentifier = ABOUT_ME_CELL_IDENTIFIER;
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    if (!cell)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        
        cell = [topLevelObjects objectAtIndex:0];
    }

    [cell set:[arrProfileData objectAtIndex:[indexPath row]]];
    
    return cell;
}

- (void)setUser:(UserProfile *)profile
{
    [_imgViewUserDp setImage:[profile avatar]];
    [_lblUsername setText:[profile fullName]];
    [_lblLevel setText:[NSString stringWithFormat:LEVEL_PREFIX,(int)[profile level]]];
    
    [_btnThumbsUp setUserInteractionEnabled:NO];
    [_lblThumbsUp setText:[NSString stringWithFormat:FORMATTED_NUMBER, (long)[profile thumbsUps]]];
    [_btnThumbsDown setUserInteractionEnabled:NO];
    [_lblThumbsDown setText:[NSString stringWithFormat:FORMATTED_NUMBER, (long)[profile thumbsDown]]];
    [_lblThanks setText:[NSString stringWithFormat:THANKS_POSTFIX, (long)[profile thanks]]];
    
    [_lblQuestionsAnswered setText:[NSString stringWithFormat:FORMATTED_NUMBER, (long)[profile questionsAnswered]]];
    [_lblChats setText:[NSString stringWithFormat:FORMATTED_NUMBER, (long)[profile chats]]];
    [_lblMemberSince setText:[profile getMemberSince]];
    
    arrProfileData = [profile getAsArrayWithEmptyValues:NO];
    
    [_tableView setHidden:NO];
    [_tableView  reloadData];
}

@end
