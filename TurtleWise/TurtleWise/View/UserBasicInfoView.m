//
//  UserBasicInfoView.m
//  TurtleWise
//
//  Created by Waaleed Khan on 12/4/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "UserBasicInfoController.h"
#import "ActionSheetDatePicker.h"
#import "UserBasicInfoView.h"
#import "StringUtils.h"
#import "UserProfile.h"
#import "TextField.h"
#import "DateUtils.h"
#import "Constant.h"
#import "Button.h"

#define TITLE_ALERT_NAME_VALIDATION @"Invalid Name!"

#define MESSAGE_FIRST_NAME_VALIDATION @"First Name should be between 3 to 35 characters."
#define MESSAGE_LAST_NAME_VALIDATION @"Last Name should be between 3 to 35 characters."

#define PICKER_TITLE_DOB @"Date of Birth"
#define TAG_MALE_BUTTON 200
#define TAG_FEMALE_BUTTON 201
#define TAG_UNKNOWN_BUTTON 202

#define TEXT_MALE @"Male"
#define TEXT_FEMALE @"Female"
#define TEXT_UNKNOWN_GENDER @"Prefer not to answer"

@interface UserBasicInfoView () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property(nonatomic, strong) NSString *genderSelected;
@property(nonatomic, strong) UIImagePickerController *imagePickerController;
@property(nonatomic) BOOL ignorUpdateInfo;

- (void)setDateOfBirth:(NSDate *)selectedDate;
- (void)setBasicInformation:(UserProfile *)profile;
- (void)setGender:(NSString *)gender;

@end

@implementation UserBasicInfoView

#pragma mark - Override Methods

- (void)updateData:(UserProfile *)profile
{
    if (_ignorUpdateInfo)
        return;
    
    [self setBasicInformation:profile];
    [self setGender:[profile gender]];
}

- (void)saveWizardData:(UserProfile *)profile
{
    if (_ignorUpdateInfo)
        return;
    
    [profile setFirstName:[[_txtFieldFirstName text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    [profile setLastName:[[_txtFieldLastName text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    [profile setDateOfBirth:[DateUtils dateFromString:[[_btnDOB titleLabel] text]]];
    [profile setGender:_genderSelected];
    [profile setAvatar:[_imageUserProfile imageForState:UIControlStateNormal]];
}

#pragma mark - Update View Methods

- (void)setBasicInformation:(UserProfile *)profile
{
    [_imageUserProfile setImage:[profile avatar] forState:UIControlStateNormal];
    [_txtFieldFirstName setText:[profile firstName]];
    [_txtFieldLastName setText:[profile lastName]];
    [_btnDOB setTitle:[DateUtils stringFromDate:[profile dateOfBirth]] forState:UIControlStateNormal];
}

- (void)setGender:(NSString *)gender
{
    _genderSelected = gender;
    
    int tag = 0;
    
    if ([StringUtils compareString:gender withString:TEXT_MALE])
    {
        tag = TAG_MALE_BUTTON;
    }
    
    if ([StringUtils compareString:gender withString:TEXT_FEMALE])
    {
        tag = TAG_FEMALE_BUTTON;
    }
    
    if ([StringUtils compareString:gender withString:TEXT_UNKNOWN_GENDER])
    {
        tag = TAG_UNKNOWN_BUTTON;
    }
    
    if (tag)
    {
        [(UIButton *)[self viewWithTag:tag] setSelected:YES];
    }
}

- (void)showImagePickerForSource:(UIImagePickerControllerSourceType)source
{
    _imagePickerController = [[UIImagePickerController alloc] init];
    _imagePickerController.sourceType = source;
    _imagePickerController.delegate = self;
    _imagePickerController.allowsEditing = YES;
    
    [self.controller presentViewController:_imagePickerController animated:YES completion:nil];
}

- (void)resignFieldsOnView
{
    [_txtFieldLastName resignFirstResponder];
    [_txtFieldLastName resignFirstResponder];
}

#pragma mark - IBActions

- (IBAction)changeUserProfile:(id)sender
{
    _ignorUpdateInfo = YES;
    [self showImagePickerForSource:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (IBAction)setUserGender:(id)sender
{
    if ([sender isSelected])
    {
        [sender setSelected:NO];
        _genderSelected = @"";
        
        return;
    }
    
    [(UIButton *)[self viewWithTag:TAG_MALE_BUTTON] setSelected:NO];
    [(UIButton *)[self viewWithTag:TAG_FEMALE_BUTTON] setSelected:NO];
    [(UIButton *)[self viewWithTag:TAG_UNKNOWN_BUTTON] setSelected:NO];

    [sender setSelected:YES];
    
    switch ([sender tag]) {
        case TAG_MALE_BUTTON:
            _genderSelected = TEXT_MALE;
            break;
            
        case TAG_FEMALE_BUTTON:
            _genderSelected = TEXT_FEMALE;
            break;
            
        default:
            _genderSelected = TEXT_UNKNOWN_GENDER;
            break;
    }
}

- (IBAction)submitWizard:(id)sender
{
    if ([self areFieldsValid])
    {
        [self completeWizard];
    }
}

- (IBAction)openDatePicker:(id)sender
{
    [self endEditing:YES];
    [ActionSheetDatePicker showPickerWithTitle:PICKER_TITLE_DOB
                                datePickerMode:UIDatePickerModeDate
                                  selectedDate:[DateUtils dateFromString:[[_btnDOB titleLabel] text]]
                                   minimumDate:nil
                                   maximumDate:[NSDate date]
                                     doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin)
    {
        [self setDateOfBirth:selectedDate];
    }
                                   cancelBlock:NULL
                                        origin:self];
}

- (void)setDateOfBirth:(NSDate *)selectedDate;
{
    if ([selectedDate compare:[NSDate date]] == NSOrderedDescending)
    {
        [Alert show:@"" andMessage:@"Date of birth must not be future date."];
        return;
    }
    [_btnDOB setTitle:[DateUtils stringFromDate:selectedDate] forState:UIControlStateNormal];
}

# pragma mark -
# pragma mark UIImagePickerController Delegate Methods

- (void)hideImagePicker
{
    [_imagePickerController dismissViewControllerAnimated:YES completion:^{
        _ignorUpdateInfo = NO;
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *editedImage = info[UIImagePickerControllerEditedImage];
    if (!editedImage) {
        [self hideImagePicker];
        return;
    }
    
    editedImage = [(UserBasicInfoController *)self.controller getSquareImage:editedImage];
    
    [_imageUserProfile setImage:editedImage forState:UIControlStateNormal];
    [(UserBasicInfoController *)self.controller updateProfilePhoto:editedImage];
    
    [self hideImagePicker];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self hideImagePicker];
}

#pragma mark - Helper Methods

- (BOOL)areFieldsValid
{    
    if ([_txtFieldFirstName isValid] && ![_txtFieldFirstName isLenghtInRangeOf:3 and:35])
    {
        [Alert show:TITLE_ALERT_NAME_VALIDATION andMessage:MESSAGE_FIRST_NAME_VALIDATION];
        
        return NO;
    }
    
    if ([_txtFieldLastName isValid] && ![_txtFieldLastName isLenghtInRangeOf:3 and:35])
    {
        [Alert show:TITLE_ALERT_NAME_VALIDATION andMessage:MESSAGE_LAST_NAME_VALIDATION];
        
        return NO;
    }
    
    return YES;
}

@end
