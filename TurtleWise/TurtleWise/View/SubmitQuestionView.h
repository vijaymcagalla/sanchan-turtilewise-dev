//
//  SubmitQuestionView.h
//  TurtleWise
//
//  Created by Waaleed Khan on 6/11/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "BaseView.h"

@class Label;
@class TLTagsControl;

@interface SubmitQuestionView : BaseView

@property (weak, nonatomic) IBOutlet Label *lblQuestionText;

@property (weak, nonatomic) IBOutlet TLTagsControl *attributesTagsView;
@property (weak, nonatomic) IBOutlet TLTagsControl *guruTagsView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightForAttributesTagView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightForGuruTagView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightForContainerView;

- (void)setData:(SeekingAdvisor *)seekingAdvisor;
- (void)showAdvisorCountForConfirmation:(NSString *)advisorCount;

- (IBAction)submitQuestion:(id)sender;

@end
