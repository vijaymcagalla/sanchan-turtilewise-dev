//
//  SearchHelpsViewController.m
//  TurtleWise
//
//  Created by Vijay Bhaskar on 08/07/18.
//  Copyright © 2018 Waaleed Khan. All rights reserved.
//

#import "SearchHelpsViewController.h"

@interface SearchHelpsViewController ()

@end

@implementation SearchHelpsViewController


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",_list);
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.list.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = self.list[indexPath.row];
//    self.list[indexPath.row][@"title"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
@end
