//
//  NSArray+Utils.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/16/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Utils)

- (BOOL)isEmpty;

- (NSArray *)sortArray:(NSArray *)unsortedArray orderAscending:(BOOL)ascending;

@end
