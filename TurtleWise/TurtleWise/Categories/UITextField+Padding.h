//
//  UITextField+Padding.h
//  TurtleWise
//
//  Created by Irfan Gul on 04/06/2015.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Padding)

-(void)addLeftPadding:(CGFloat)padding;

@end
