//
//  ConfigResponse.m
//  TurtleWise
//
//  Created by Waleed Khan on 7/11/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "ConfigResponse.h"
#import "UserLevel.h"
#import "Charity.h"

#define KEY_LEVELS @"levels"
#define KEY_CHARITIES @"charity"
#define KEY_CONVERSATION_RATE @"conversionRate"
#define KEY_TURTLE_BUCK @"turtleBucks"
#define KEY_DOLLARS @"dollar"

@interface ConfigResponse ()

@property(nonatomic, strong) NSArray *levelsTable;
@property(nonatomic, strong) NSArray *charities;
@property(nonatomic, strong) NSArray *levelBucks;
@property(nonatomic, strong) NSArray *levelAmount;

- (void)parseUserLevelTable:(NSDictionary *)input;
- (void)parseCharityList:(NSDictionary *)input;

@end

@implementation ConfigResponse

#pragma mark - Response Setter

- (void)set:(NSDictionary *)input
{
    NSDictionary *data = [ParserUtils object:input key:KEY_API_DATA];
    
    [self parseUserLevelTable:data];
    [self parseCharityList:data];
    [self parseLevelBucks:data];
}

#pragma mark - Helper Methods

- (void)parseUserLevelTable:(NSDictionary *)input
{
    NSMutableArray *userLevels = [[NSMutableArray alloc] init];
    
    NSArray *levels = [ParserUtils array:input key:KEY_LEVELS];
    
    for(NSDictionary *level in levels)
    {
        UserLevel *lvl = [UserLevel new];
        [lvl set:level];
        
        [userLevels addObject:lvl];
    }
    
    _levelsTable = [[NSArray alloc] initWithArray:userLevels];
}

- (void)parseCharityList:(NSDictionary *)input
{
    NSMutableArray *charities = [[NSMutableArray alloc] init];
    
    NSArray *rawCharities = [ParserUtils array:input key:KEY_CHARITIES];
    
    for(NSDictionary *rawCharity in rawCharities)
    {
        Charity *charity = [Charity new];
        [charity set:rawCharity];
        
        [charities addObject:charity];
    }
    
    _charities = [[NSArray alloc] initWithArray:charities];
}

- (void)parseLevelBucks:(NSDictionary *)input
{
    NSMutableArray *levelBucks = [NSMutableArray new];
    NSMutableArray *levelAmount = [NSMutableArray new];
    
    NSArray *conversationRates = [ParserUtils array:input key:KEY_CONVERSATION_RATE];
    
    for(NSDictionary *conversationRate in conversationRates)
    {
        [levelBucks addObject:[NSString stringWithFormat:@"%@", [ParserUtils numberValue:conversationRate key:KEY_TURTLE_BUCK]]];
        [levelAmount addObject:[NSString stringWithFormat:@"%@", [ParserUtils numberValue:conversationRate key:KEY_DOLLARS]]];
    }
    
    _levelBucks = [[NSArray alloc] initWithArray:levelBucks];
    _levelAmount = [[NSArray alloc] initWithArray:levelAmount];
}

#pragma mark - Getters

- (NSArray *)levelsTable
{
    return _levelsTable;
}

- (NSArray *)charities
{
    return _charities;
}

- (NSArray *)levelBucks
{
    return _levelBucks;
}

- (NSArray *)levelAmount
{
    return _levelAmount;
}

@end
