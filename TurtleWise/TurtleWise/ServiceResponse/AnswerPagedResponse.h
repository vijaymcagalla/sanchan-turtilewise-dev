//
//  AnswerPagedResponse.h
//  TurtleWise
//
//  Created by Ajdal on 1/15/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "PagedResponse.h"

@interface AnswerPagedResponse : PagedResponse
@property(nonatomic,strong) NSString *questionID;
@property(nonatomic,strong) NSMutableArray *answerOptions;
@property(nonatomic,strong) NSString *activeChatId;
@property(nonatomic,assign) BOOL isUserSubscribed;

@end
