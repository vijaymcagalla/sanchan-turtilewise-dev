//
//  StringResponse.h
//  iOSTemplate
//
//  Created by mohsin on 11/5/14.
//  Copyright (c) 2014 mohsin. All rights reserved.
//

#import "BaseResponse.h"

@interface StringResponse : BaseResponse
-(id)init:(NSString*)responseKey;
-(id)get;

@end
