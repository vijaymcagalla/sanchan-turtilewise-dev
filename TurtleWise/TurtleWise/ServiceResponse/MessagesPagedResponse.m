//
//  MessagesPagedResponse.m
//  TurtleWise
//
//  Created by Waleed Khan on 4/26/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "MessagesPagedResponse.h"
#import "Conversation.h"
#import "UserDefaults.h"
#import "ChatMessage.h"
#import "Keys.h"

@interface MessagesPagedResponse ()

- (void)setupChatMessages:(NSArray *)rawDataArray;

@property(nonatomic, strong) Conversation *conversation;

@end

@implementation MessagesPagedResponse

#pragma mark - Life Cycle Methods

- (id)init
{
    if (self = [super init])
    {
        _messageOffset = [NSNumber new];
    }
    
    return self;
}

#pragma mark - Setter

- (void)set:(NSDictionary *)input
{
    if (![self dataArray])
    {
        [self setDataArray:[NSMutableArray new]];
    }
    
    NSArray *messages = [ParserUtils array:input key:KEY_API_DATA];
    
    if([self refreshDataset])
    {
        [super removeAllObjectsFromArray];
    }
    
    [self setSkip:[self skip] + (int)[messages count]];
    
    [self setupChatMessages:messages];
}

- (void)setupChatMessages:(NSArray *)rawDataArray
{
    if ([rawDataArray count] == 0)
    {
        [self setHasMoreData:NO];
        
        return;
    }
    
    __block NSString *userId = [UserDefaults getUserID];
    
    [rawDataArray enumerateObjectsUsingBlock:^(NSDictionary *rawMessage, NSUInteger idx, BOOL * _Nonnull stop)
    {
        ChatMessage *chatMessage = [ChatMessage new];
        
        [chatMessage set:rawMessage withUserId:userId];
        [[chatMessage messageSender] determineSenderRoleForConversation:_conversation];
        
        [[self dataArray] insertObject:chatMessage atIndex:0];
    }];
    
    _messageOffset = [[rawDataArray lastObject] objectForKey:KEY_CREATED_AT];
}

- (void)setConversation:(Conversation *)conversation
{
    _conversation = conversation;
}

@end
