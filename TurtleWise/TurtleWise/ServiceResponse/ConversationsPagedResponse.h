//
//  ConversationPagedResponse.h
//  TurtleWise
//
//  Created by Ajdal on 4/22/16.
//  Copyright © 2016 Waaleed Khan. All rights reserved.
//

#import "PagedResponse.h"

@interface ConversationsPagedResponse : PagedResponse

@end
