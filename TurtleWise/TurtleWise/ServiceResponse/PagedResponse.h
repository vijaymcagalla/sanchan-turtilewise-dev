//
//  PagedResponse.h
//  GuestLynx
//
//  Created by Ajdal on 10/5/15.
//  Copyright (c) 2015 10Pearls. All rights reserved.
//

#import "BaseResponse.h"
#import "ParserUtils.h"

#define KEY_SKIP @"skip"
#define KEY_PAGE_SIZE @"pageSize"
#define KEY_ANSWERED @"answered"
#define KEY_TOTAL @"total"


@interface PagedResponse : BaseResponse

@property(nonatomic,retain) NSMutableArray *dataArray;
@property(nonatomic) int skip;
@property(nonatomic) int pageSize;
@property(nonatomic) int totalCount;
@property(nonatomic) BOOL refreshDataset;
@property(nonatomic) BOOL isLoadingData;
@property(nonatomic) BOOL hasMoreData;


-(void)reset;
-(void)removeAllObjectsFromArray;
@end
