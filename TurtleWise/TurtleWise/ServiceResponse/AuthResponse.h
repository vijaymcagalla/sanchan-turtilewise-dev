//
//  AuthResponse.h
//  TurtleWise
//
//  Created by Waaleed Khan on 12/4/15.
//  Copyright © 2015 Waaleed Khan. All rights reserved.
//

#import "BaseResponse.h"

@interface AuthResponse : BaseResponse

@property(nonatomic, assign) BOOL isNewUser;

@end
