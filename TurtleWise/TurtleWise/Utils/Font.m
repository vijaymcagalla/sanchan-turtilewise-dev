//
//  Font.m
//  wusup
//
//  Created by mohsin on 3/11/14.
//  Copyright (c) 2014 SocialRadar. All rights reserved.
//

#import "Font.h"

#define REGULAR_FONT             @"ROBOTO-REGULAR"
#define MEDIUM_FONT              @"ROBOTO-MEDIUM"
#define BOLD_FONT                @"ROBOTO-BOLD"
#define LIGHT_FONT               @"ROBOTO-LIGHT"
#define ITALIC_FONT              @"ROBOTO-ITALIC"

#define NAV_BUTTON_FONT          @"HelveticaNeue-Light"
#define NAV_BUTTON_FONT_SIZE       17

@implementation Font

+(UIFont*)smallFont{
    return [UIFont fontWithName:LIGHT_FONT size:13];
}

+(UIFont*)lightFontWithSize:(int)size {
    return [UIFont fontWithName:LIGHT_FONT size:size];
}

+(UIFont*)regularFontWithSize:(int)size {
    return [UIFont fontWithName:REGULAR_FONT size:size];
}

+(UIFont*)boldFontWithSize:(int)size {
    return [UIFont fontWithName:BOLD_FONT size:size];
}

+(UIFont *)italicFontWithSize:(int)size
{
    return [UIFont fontWithName:ITALIC_FONT size:size];
}

+(UIFont *)navButtonFont {
    return [UIFont fontWithName:NAV_BUTTON_FONT size:NAV_BUTTON_FONT_SIZE];
    
}

+ (UIFont *)mediumFontWithSize:(int)size
{
    return [UIFont fontWithName:MEDIUM_FONT size:size];
}


+ (CGFloat)getHeight:(NSString*)text andFont:(UIFont *)font andWidth:(int)width {
    NSAttributedString *attributedText;
    CGSize boundingBox;
    
    CGSize maximumSize = CGSizeMake(width, 99999);
    
    attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName : font}];
    
    NSRange range = NSMakeRange(0, [attributedText length]);
    NSDictionary *attributes = [attributedText attributesAtIndex:0 effectiveRange:&range];
    
    boundingBox = [text boundingRectWithSize:maximumSize
                                     options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin
                                  attributes:attributes
                                     context:nil].size;
    
    CGSize size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;

}

@end
