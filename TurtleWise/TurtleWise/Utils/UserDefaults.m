//
//  UserDefaults.m
//  TurtleWise
//
//  Created by Waaleed Khan on 6/12/15.
//  Copyright (c) 2015 mohsin. All rights reserved.
//

#import "UserDefaults.h"
#import "StringUtils.h"
#import "UserProfile.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "Keys.h"


#define KEY_USER_PROFILE @"UserProfile"
#define KEY_SAVE_USER_PROFILE @"SaveUserProfile"
#define KEY_DID_USER_ASKED_QUESTION @"DidUserAskedQuestion"
#define KEY_IDEAL_ADVISOR_TEXT @"IdealAdvisorText"
#define KEY_DID_USER_ANSWERED_A_QUESTION @"DidUserAnsweredAQuestion"
#define KEY_SHOULD_SHOW_AD @"ShouldShowAdd"
#define KEY_SHOULD_SHOW_CHAT_NOTIFICATION @"ShouldShowChatNotification"
#define KEY_DID_SHOW_ANSWER_NOTIFICATION @"DidShowAnswerNotification"
#define KEY_DID_USER_SUBSCRIBED @"DidUserSubscribed"
#define KEY_QUESTION_TEXT @"QuestionText"
#define LOGIN_FLOW_TYPE @"LoginFlowType"
#define BG_IMAGE_INDEX @"BgImageIndex"
#define KEY_SAVE_USER_STATS @"SaveUserStats"
#define KEY_PUSH_TOKEN @"SavePushToken"
#define KEY_CHECK_UNRATED_CHAT @"CheckUnratedChats"
#define KEY_LEVELS_TABLE @"levels"
#define KEY_CHARITIES @"Charities"
#define KEY_LEVEL_AMOUNT @"LevelAmount"
#define KEY_LEVEL_BUCKS @"LevelBucks"
#define KEY_LAST_LOGIN @"LastLogin"
#define KEY_COUPON @"coupon"

#define KEY_LOGIN_EMAIL @"email"
#define KEY_LOGIN_PASSWORD @"password"

@interface UserDefaults ()

+ (void)saveInteger:(NSInteger)value forKey:(NSString *)key;
+ (void)saveBool:(BOOL)value forKey:(NSString *)key;

@end


@implementation UserDefaults

+ (void)setUserProfile:(UserProfile *)profile
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:profile];
    
    [defaults setObject:data forKey:KEY_SAVE_USER_PROFILE];
    [defaults synchronize];
}

+ (UserProfile *)getUserProfile
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:KEY_SAVE_USER_PROFILE])
    {
        return [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:KEY_SAVE_USER_PROFILE]];
    }
    
    return nil;
}

+ (void)setLevelTable:(NSArray *)levels
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:levels];
    
    [defaults setObject:data forKey:KEY_LEVELS_TABLE];
    [defaults synchronize];
}
+ (void)setCoupon:(NSString *)Coupon
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *data = Coupon;
    [defaults setObject:data forKey:KEY_COUPON];
    [defaults synchronize];
}
+ (NSArray *)getLevelTable
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:KEY_LEVELS_TABLE])
    {
        return [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:KEY_LEVELS_TABLE]];
    }
    
    return nil;
}

+ (UserRole)getUserRole
{
    return (UserRole)[[NSUserDefaults standardUserDefaults] integerForKey:KEY_USER_ROLE];
}

+ (void)setUserRole:(UserRole)userRole
{
    [UserDefaults saveInteger:userRole forKey:KEY_USER_ROLE];
}

+ (LoginFlowType)loginFlowType
{
    return (LoginFlowType)[[NSUserDefaults standardUserDefaults] integerForKey:LOGIN_FLOW_TYPE];
}

+ (void)setLoginFlowType:(LoginFlowType)loginFlow
{
    [UserDefaults saveInteger:loginFlow forKey:LOGIN_FLOW_TYPE];
}

+ (NSString *)getAccessToken
{
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_ACCESS_TOKEN];
    
    if ([StringUtils isEmptyOrNull:accessToken])
    {
        return @"";
    }
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:KEY_ACCESS_TOKEN];
}

+ (void)setAccessToken:(NSString *)accessToken
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:accessToken forKey:KEY_ACCESS_TOKEN];
    [defaults synchronize];
}

+ (NSString *)getUserID
{
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_USER_ID];
    NSLog(@"userid:%@",userId);
    if ([StringUtils isEmptyOrNull:userId])
    {
        return @"";
    }
    return [[NSUserDefaults standardUserDefaults] objectForKey:KEY_USER_ID];
}

+ (void)setUserID:(NSString *)userID
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:userID forKey:KEY_USER_ID];
    NSLog(@"userid:%@",userID);
    [defaults synchronize];
}

+ (NSString *)getPageID
{
    NSString *pageId = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PAGE_ID];
    
    if ([StringUtils isEmptyOrNull:pageId])
    {
        return @"";
    }
    return [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PAGE_ID];
}

+ (void)setPageID:(NSString *)pageID
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:pageID forKey:KEY_PAGE_ID];
    [defaults synchronize];

}

+ (BOOL)isUserLogin
{
    return !([StringUtils isEmptyOrNull:[UserDefaults getAccessToken]]);
}

+ (UserStats *)getUserStats
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:KEY_SAVE_USER_STATS])
    {
        return [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:KEY_SAVE_USER_STATS]];
    }
    
    return nil;
}

+ (void)setUserStats:(UserStats *)stats
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:stats];
    
    [defaults setObject:data forKey:KEY_SAVE_USER_STATS];
    [defaults synchronize];
}

+ (void)clearDefaults
{
    [UserDefaults setLoginFlowType:LoginFlowTypeEmail];
    [UserDefaults setAccessToken:NULL];
    [UserDefaults setUserID:NULL];
    [UserDefaults setShouldCheckForUnratedChats:NO];
    [UserDefaults setUserProfile:NULL];
    [UserDefaults setPageID:NULL];
    
    [APP_DELEGATE setShowCheckForSubscriptionExpiray:YES];
}

+ (NSString *)getPushToken
{
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PUSH_TOKEN];
    
    if ([StringUtils isEmptyOrNull:userId])
    {
        return @"0ae44bf701af8668a959265e059dbad455524160732310a08153878847c7a3a5";
    }
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PUSH_TOKEN];
}

+ (void)savePushToken:(NSString *)pushToken
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:pushToken forKey:KEY_PUSH_TOKEN];
    [defaults synchronize];
}

+ (BOOL)shouldCheckForUnratedChats
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:KEY_CHECK_UNRATED_CHAT];
}

+ (void)setShouldCheckForUnratedChats:(BOOL)shouldCheck
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setBool:shouldCheck forKey:KEY_CHECK_UNRATED_CHAT];
    [defaults synchronize];
}

+ (BOOL)isPremiumUser
{
    UserProfile *userProfile = [UserDefaults getUserProfile];
    
    return [userProfile isUserPremium];
}

+ (void)setCharities:(NSArray *)charities
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:charities];
    
    [defaults setObject:data forKey:KEY_CHARITIES];
    [defaults synchronize];
}

+ (NSArray *)getCharities
{
    return [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:KEY_CHARITIES]];
}

+ (void)setLevelBucks:(NSArray *)levelBucks
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:levelBucks forKey:KEY_LEVEL_BUCKS];
    [defaults synchronize];
}

+ (NSArray *)getLevelBucks
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:KEY_LEVEL_BUCKS];
}

+ (void)setLevelAmount:(NSArray *)levelAmount
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:levelAmount forKey:KEY_LEVEL_AMOUNT];
    [defaults synchronize];
}

+ (NSArray *)getLevelAmount
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:KEY_LEVEL_AMOUNT];
}

+ (NSDate *)getLastLogin
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:KEY_LAST_LOGIN];
}

+ (void)setLastLogin:(NSDate *)lastLogin
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:lastLogin forKey:KEY_LAST_LOGIN];
    [userDefaults synchronize];
}

+ (BOOL) showAlertForIncompleteProfile:(UserProfile *) profile {
    
    NSArray *arrayStrings = [[profile profilePercentage] componentsSeparatedByString:@" "];
    if(arrayStrings.count > 1) {
        NSString *strNum = arrayStrings[0];
        @try {
            int percent = [strNum intValue];
            if(percent == 100)
                return NO;
            return YES;
        }
        @catch(NSException *ex) {
            NSLog(@"Exception while parsing percentage: %@... %@", strNum, ex.reason);
        }
    }
    
    return NO;
}

#pragma mark - Private Methods

+ (void)saveInteger:(NSInteger)value forKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setInteger:value forKey:key];
    [userDefaults synchronize];
}

+ (void)saveBool:(BOOL)value forKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setBool:value forKey:key];
    [userDefaults synchronize];
}

#pragma mark - Remember Me
+ (void)saveLoginEmail:(NSString*)email{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:email forKey:KEY_LOGIN_EMAIL];
    [defaults synchronize];
}
+ (NSString*)getLoginEmail{
    return [[NSUserDefaults standardUserDefaults] objectForKey:KEY_LOGIN_EMAIL];
}

+ (void)saveLoginPassword:(NSString*)email{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:email forKey:KEY_LOGIN_PASSWORD];
    [defaults synchronize];
}
+ (NSString*)getLoginPassword{
    return [[NSUserDefaults standardUserDefaults] objectForKey:KEY_LOGIN_PASSWORD];
}

+ (void)clearSavedLoginCredentials{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults removeObjectForKey:KEY_LOGIN_EMAIL];
    [defaults removeObjectForKey:KEY_LOGIN_PASSWORD];
    [defaults synchronize];
}

+ (void)followPrivate:(NSString*)pageId {
    NSMutableArray *sam = [NSMutableArray arrayWithArray:[[UserDefaults getUserProfile] pending]];
    [sam addObject:pageId];
    UserProfile* samtwo = [UserDefaults getUserProfile];
    samtwo.pending = [NSArray arrayWithArray:sam];
    [UserDefaults setUserProfile:samtwo];
}

+ (void)followPublic:(NSString*)pageId {
    NSMutableArray *sam = [NSMutableArray arrayWithArray:[[UserDefaults getUserProfile] following]];
    [sam addObject:pageId];
    UserProfile* samtwo = [UserDefaults getUserProfile];
    samtwo.following = [NSArray arrayWithArray:sam];
    [UserDefaults setUserProfile:samtwo];
}

+ (void)unfollow:(NSString*)pageId {
    
    if ([[[UserDefaults getUserProfile] pending] containsObject:pageId]) {
        NSMutableArray *sam = [NSMutableArray arrayWithArray:[[UserDefaults getUserProfile] pending]];
        [sam removeObject:pageId];
        UserProfile* samtwo = [UserDefaults getUserProfile];
        samtwo.pending = [NSArray arrayWithArray:sam];
        [UserDefaults setUserProfile:samtwo];
    }
    
    if([[[UserDefaults getUserProfile] following] containsObject:pageId]) {
        NSMutableArray *sam = [NSMutableArray arrayWithArray:[[UserDefaults getUserProfile] following]];
        [sam removeObject:pageId];
        UserProfile* samtwo = [UserDefaults getUserProfile];
        samtwo.following = [NSArray arrayWithArray:sam];
        [UserDefaults setUserProfile:samtwo];
    }
    
}

@end
