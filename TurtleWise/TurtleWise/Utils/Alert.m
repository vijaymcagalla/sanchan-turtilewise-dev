//
//  Alert.m
//  Guardian
//
//  Created by mohsin on 10/22/14.
//  Copyright (c) 2014 10Pearls. All rights reserved.
//

#import "CustomIOSAlertView.h"
#import "BaseController.h"
#import <UIKit/UIKit.h>
#import "StringUtils.h"
#import "Constant.h"
#import "Alert.h"
#import "Font.h"

@implementation Alert

+(void)show:(NSString*)title andMessage:(NSString*)message{
    [Alert show:title andMessage:message andDelegate:nil];

}

+(void)show:(NSError *)error
{
    if ([StringUtils isEmptyOrNull:[error localizedDescription]])
    {
        [Alert show:ERROR_ALERT_TITLE andMessage:@"Unexpected error occured. Please try again." andDelegate:nil];
        
        return;
    }
    
    [Alert show:ERROR_ALERT_TITLE andMessage:[error localizedDescription] andDelegate:nil];
}

+(void)show:(NSString*)title andMessage:(NSString*)message andDelegate:(id<UIAlertViewDelegate>)theDelegate{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:theDelegate
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil, nil];
    [alert show];
}

+(void)show:(NSString*)title andMessage:(NSString *)message boldText:(NSString *)boldText
{
    [self show:title andMessage:message boldText:boldText onDismiss:nil];
}

+(void)show:(NSString *)title andMessage:(NSString *)message cancelButtonTitle:(NSString *)cancelButton otherButtonTitles:(NSString *)otherButtonTitles andDelegate:(id<UIAlertViewDelegate>)theDelegate
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:theDelegate
                                          cancelButtonTitle:cancelButton
                                          otherButtonTitles:otherButtonTitles, nil];
    [alert show];
}

+(void)show:(NSString *)title andMessage:(NSString *)message cancelButtonTitle:(NSString *)cancelButton otherButtonTitles:(NSString *)otherButtonTitles tag:(NSInteger) tag andDelegate:(id<UIAlertViewDelegate>)theDelegate
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:theDelegate
                                          cancelButtonTitle:cancelButton
                                          otherButtonTitles:otherButtonTitles, nil];
    alert.tag = tag;
    [alert show];
}

+(void)showInController:(BaseController *)controller withTitle:(NSString*)title message:(NSString *)message onDismiss:(AlertCompletionHandler)dismiss
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title
                                                                    message:message
                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                            {
                                if (dismiss)
                                {
                                    dismiss();
                                }
                                [alert dismissViewControllerAnimated:YES completion:^{
                                }];
                             
                            }];
    
    [alert addAction:ok];
    [controller presentViewController:alert animated:YES completion:nil];
}

+(void)show:(NSString*)title andMessage:(NSString *)message boldText:(NSString *)boldText onDismiss:(AlertCompletionHandler)onDismis
{
    message = [title stringByAppendingFormat:@"\n%@",message];
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:message attributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0], NSForegroundColorAttributeName:[UIColor blackColor]}];
    NSRange range = [message rangeOfString:boldText];
    [attStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0] range:range];
    
    range = [message rangeOfString:title];
    [attStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0] range:range];
    
    UILabel *messageLabel = [[UILabel alloc]init];
    [messageLabel setNumberOfLines:0];
    [messageLabel setAttributedText:attStr];
    [messageLabel setTextAlignment:NSTextAlignmentCenter];
    [messageLabel sizeToFit];
    [messageLabel setFrame:CGRectMake(0, 0, messageLabel.frame.size.width+80, messageLabel.frame.size.height+40)];

    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc]init];
    [alertView setContainerView:messageLabel];
    [alertView setButtonTitles:@[@"OK"]];
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(onDismis)
            onDismis();
        [alertView close];
    }];
    [alertView show];
}


@end